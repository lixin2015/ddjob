﻿using System;

namespace DdJob.Text
{
    //public enum VerifyCodeType : byte
    //{
    //    Register = 0,
    //    Login,
    //    ForgotPassword
    //}

    public class VerifyCode
    {
        public string Code { get; set; }
        public DateTime Expire { get; set; }
    }
    public class MobileVerifyCode : VerifyCode
    {
        public string Mobile { get; set; }

        public MobileVerifyCode(VerifyCode vCode)
        {
            base.Code = vCode.Code;
            base.Expire = vCode.Expire;
        }
    }

    public class EmailVerifyCode : VerifyCode
    {
        public string Email { get; set; }
        public EmailVerifyCode(VerifyCode vCode)
        {
            base.Code = vCode.Code;
            base.Expire = vCode.Expire;
        }
    }
    public class VerifyCodeGenerator
    {
        public static VerifyCode Create()
        {
            return Create(6);
        }

        public static VerifyCode Create(int length)
        {
            var rand = new Random();
            var i = rand.Next((int) Math.Pow(10, (length - 1)), (int) Math.Pow(10, length));
            return new VerifyCode()
            {
                Code = i.ToString().PadLeft(length, '0'),
                Expire = DateTime.Now.AddMinutes(5)
            };
        }

    }
}
