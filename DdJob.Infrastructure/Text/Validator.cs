﻿using System.Text.RegularExpressions;

namespace DdJob.Text
{
    public class Validator
    {
        public static bool IsChinaMobilePhone(string mobile)
        {
            return Regex.IsMatch(mobile, "^(13\\d|15[0-3,5-8]|18\\d|170|177|178)\\d{8}$");
        }
    }
}
