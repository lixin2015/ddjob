using System.Text.RegularExpressions;

namespace DdJob.Text
{
    /// <summary>
    /// This class contains several common regular expressions.
    /// </summary>
    public class RegexPatterns
    {
        public static readonly Regex RegSqlInjectionFilter =
            new Regex(@"\s?or\s*|\s?;\s?|\s?drop\s|\s?grant\s|^'|\s?--|\s?union\s|\s?delete\s|\s?truncate\s|" +
                      @"\s?sysobjects\s?|\s?xp_.*?|\s?syslogins\s?|\s?sysremote\s?|\s?sysusers\s?|\s?sysxlogins\s?|\s?sysdatabases\s?|\s?aspnet_.*?|\s?exec\s?",
                RegexOptions.IgnoreCase);

        /// <summary>
        /// Alphabetic regex.
        /// </summary>
        public const string Alpha = @"^[a-zA-Z]*$";


        /// <summary>
        /// Uppercase Alphabetic regex.
        /// </summary>
        public const string AlphaUpperCase = @"^[A-Z]*$";


        /// <summary>
        /// Lowercase Alphabetic regex.
        /// </summary>
        public const string AlphaLowerCase = @"^[a-z]*$";


        /// <summary>
        /// Alphanumeric regex.
        /// </summary>
        public const string AlphaNumeric = @"^[a-zA-Z0-9]*$";


        /// <summary>
        /// Alphanumeric and space regex.
        /// </summary>
        public const string AlphaNumericSpace = @"^[a-zA-Z0-9 ]*$";


        /// <summary>
        /// Alphanumeric and space and dash regex.
        /// </summary>
        public const string AlphaNumericSpaceDash = @"^[a-zA-Z0-9 \-]*$";


        /// <summary>
        /// Alphanumeric plus space, dash and underscore regex.
        /// </summary>
        public const string AlphaNumericSpaceDashUnderscore = @"^[a-zA-Z0-9 \-_]*$";


        /// <summary>
        /// Alphaumieric plus space, dash, period and underscore regex.
        /// </summary>
        public const string AlphaNumericSpaceDashUnderscorePeriod = @"^[a-zA-Z0-9\. \-_]*$";


        /// <summary>
        /// Numeric regex.
        /// </summary>
        public const string Numeric = @"^\-?[0-9]*\.?[0-9]*$";


        /// <summary>
        /// Numeric regex.
        /// </summary>
        public const string Integer = @"^\-?[0-9]*$";


        /// <summary>
        /// Ssn regex.
        /// </summary>
        public const string SocialSecurity = @"\d{3}[-]?\d{2}[-]?\d{4}";


        /// <summary>
        /// E-mail regex.
        /// </summary>
        public const string Email = @"^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$";


        /// <summary>
        /// Url regex.
        /// </summary>
        public const string Url =
            @"^^(ht|f)tp(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_=]*)?$";


        /// <summary>
        /// US zip code regex.
        /// </summary>
        public const string ZipCodeUS = @"\d{5}";


        /// <summary>
        /// US zip code with four digits regex.
        /// </summary>
        public const string ZipCodeUSWithFour = @"\d{5}[-]\d{4}";


        /// <summary>
        /// US zip code with optional four digits regex.
        /// </summary>
        public const string ZipCodeUSWithFourOptional = @"\d{5}([-]\d{4})?";


        /// <summary>
        /// US phone regex.
        /// </summary>
        public const string PhoneUS = @"\d{3}[-]?\d{3}[-]?\d{4}";

        ///<summary>
        /// General Format:(000)000-0000
        /// </summary>
        public const string PhoneGeneral = @"^\(\d+\)\d+\-\d+$";

        /// <summary>
        /// China PostCode: 000000
        /// </summary>
        public const string PostCodeCn = @"[1-9]\d{5}(?!\d)";

        public const string IDCard = "^\\d{17}[\\d|x|X]$";

        public const string MTP = "^\\d{6}$";

        public const string TelPhone = "^\\d{11}$";

        public const string TelAndHome = @"(^(\d{3}-)?\d{8})$|([1][3-8][0-9]{9})";

        public const string Password = "(?!^[a-z]+$|^[0-9]+$)^[a-zA-Z0-9]{6,}$";
    }
}