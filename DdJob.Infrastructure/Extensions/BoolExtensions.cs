﻿namespace DdJob
{
    public static class BoolExtensions
    {
        public static string ToYesNo(this bool value)
        {
            return value ? "Yes" : "No";
        }
        public static string ToYN(this bool value)
        {
            return value ? "Y" : "N";
        }
        public static string ToYEmpty(this bool value)
        {
            return value ? "Y" : "";
        }
        public static string ToYN(this bool? value)
        {
            return value.HasValue ? value.Value.ToYN() : "";
        }
        public static string ToSuccFail(this bool value)
        {
            return value ? "成功" : "失败";
        }
        public static string ToYesNoChn(this bool value)
        {
            return value ? "是" : "否";
        }
    }
}
