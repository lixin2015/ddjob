﻿using DdJob.Utilities._3DES;

namespace DdJob
{
    public static class _3DesExtensions
    {
        private static PhoneTriple3Des _phonePtriple3Des = new PhoneTriple3Des();
        private static Triple3Des _ptriple3Des = new Triple3Des();

        /// <summary>
        /// 手机号加密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string EncryptPhone(this string value)
        {
            if (value.Length != 11) return "";
            return _phonePtriple3Des.EncryptPhone(value);
        }

        /// <summary>
        /// 手机号解密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string DecryptPhone(this string value)
        {
            if (value == null) return "";
            if (value.Length != 24) return value;
            return _phonePtriple3Des.DecryptPhone(value);
        }

        /// <summary>
        /// 数据加密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Des3Encrypt(this string value)
        {
            if (string.IsNullOrEmpty(value)) return "";
            return _ptriple3Des.Des3Encrypt(value);
        }

        /// <summary>
        /// 数据解密
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string Des3Decrypt(this string value)
        {
            if (string.IsNullOrEmpty(value)) return "";
            return Triple3Des.Des3Decrypt(value);
        }
    }
}
