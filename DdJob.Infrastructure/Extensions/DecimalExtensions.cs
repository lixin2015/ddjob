﻿namespace DdJob
{
    public static class DecimalExtensions
    {
        public static string ToDecimalString(this decimal value, string prefix = "$", int decimaldigits = 2)
        {
            var format = "#0.";
            for (var i = 1; i <= decimaldigits; i++)
            {
                format += "0";
            }
            if (value > 0 || value < 0)
            {
                return prefix + value.ToString(format);
            }
            return prefix + "0";
        }

        public static string ToDynamicString(this decimal value, string prefix = "", int decimaldigits = 2)
        {
            var format = "#0.";
            for (var i = 1; i <= decimaldigits; i++)
            {
                format += "0";
            }
            if (value > 0 || value < 0)
            {
                var str = prefix + value.ToString(format);
                var arr = str.Split('.');
                if (arr.Length > 1 && arr[1] == "00")
                    return arr[0];
                return str;
            }
            return prefix + "0";
        }
    }
}
