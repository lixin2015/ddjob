﻿using System;
using System.Linq;
using NCommon.Data.EntityFramework;

namespace ZZB
{
    [Obsolete("Forbid to use this in this project.", true)]
    public static class EntityRepositoryExtensions
    {
        [Obsolete("Forbid to use this in this project.", true)]
        public static void Save<TEntity>(this EFRepository<TEntity> repository, TEntity entity) where TEntity : BaseEntity
        {
            if (entity.IsNew)
            {
                entity.CreatedDate = DateTime.Now;
                repository.Add(entity);
            }
            else
            {
                repository.Attach(entity);
            }
        }

        public static TEntity GetById<TEntity>(this EFRepository<TEntity> repository, long id) where TEntity : BaseEntity
        {
            return repository.FirstOrDefault(c => c.Id == id);
        }

        public static void Delete<TEntity>(this EFRepository<TEntity> repository, TEntity entity) where TEntity : BaseEntity
        {
            repository.Delete(entity);
        }

        public static void DeleteById<TEntity>(this EFRepository<TEntity> repository, long id) where TEntity : BaseEntity
        {
            var entity = repository.GetById(id);
            if (entity == null)
                return;
            repository.Delete(entity);
        }
    }
}
