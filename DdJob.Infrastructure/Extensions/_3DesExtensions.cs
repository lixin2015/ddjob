﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using DdJob.Utilities._3DES;

namespace DdJob
{
    public static class MD5Helper
    {
        /// <summary>
        /// 加密字符串
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetStringHash(string str)
        {
            List<string> ls = new List<string>();
            using (MD5 md5 = MD5.Create())
            {
                byte[] fromData = System.Text.Encoding.Unicode.GetBytes(str);
                byte[] targetData = md5.ComputeHash(fromData);
                string byte2String = null;

                for (int i = 0; i < targetData.Length; i++)
                {
                    byte2String += targetData[i].ToString("x");
                }

                return byte2String;
            }
        }

        /// <summary>
        /// 加密文件流
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFileHash(string fileName)
        {
            using (MD5 md5 = MD5.Create())
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    List<string> lis = new List<string>();
                    byte[] b1 = md5.ComputeHash(fs);
                    for (int i = 0; i < b1.Length; i++)
                    {
                        lis.Add(b1[i].ToString("x2"));
                    }
                    return string.Concat(lis);
                }
            }
        }

        /// <summary>
        /// 加密中文
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetChineseHash(string str)
        {
            List<string> ls = new List<string>();
            using (MD5 md5 = MD5.Create())
            {
                byte[] strbytes = Encoding.GetEncoding("gb2312").GetBytes(str);
                strbytes = md5.ComputeHash(strbytes);
                for (int i = 0; i < strbytes.Length; i++)
                {
                    ls.Add(strbytes[i].ToString("x2")); //将字节转换为字符串
                }
                // return  Encoding.UTF8.GetString(strbytes);
                return string.Concat(ls);

            }
        }

        ///DES加密    
        ///sKey 
        public static string GetStringHash(string str, string sKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByteArray = Encoding.Default.GetBytes(str);
            des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            StringBuilder ret = new StringBuilder();
            foreach (byte b in ms.ToArray())
            {
                ret.AppendFormat("{0:X2}", b);
            }
            ret.ToString();
            return ret.ToString();
        }
        ///DES解密    
        public static string DecryptStringHash(string pToDecrypt, string sKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            byte[] inputByteArray = new byte[pToDecrypt.Length / 2];
            for (int x = 0; x < pToDecrypt.Length / 2; x++)
            {
                int i = (Convert.ToInt32(pToDecrypt.Substring(x * 2, 2), 16));
                inputByteArray[x] = (byte)i;
            }

            des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);
            des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();
            new StringBuilder();

            return Encoding.Default.GetString(ms.ToArray());
        }
    }
}
