﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DdJob.Text;

namespace DdJob
{
    public static partial class StringExtensions
    {
        public static string EndWithBackslash(this string input)
        {
            if (input == null)
            {
                return "\\";
            }
            if (!input.EndsWith("\\"))
            {
                input += "\\";
            }
            return input;
        }

        public static string Default(this string input, string defaultStr = "-")
        {
            if (string.IsNullOrEmpty(input))
            {
                return defaultStr;
            }
            return input;
        }

        public static string ToPassword(this string input, int length = 8)
        {
            var result = string.Empty;
            return length <= 0 ? "" : result.PadRight(length, '*');
        }

        #region Type Converter

        public static T ParseTo<T>(this string str) where T : struct
        {
            return ParseTo<T>(str, default(T));
        }

        public static T ParseTo<T>(this string str, T defaultValue) where T : struct
        {
            var val = ParseToNullable<T>(str);
            if (val.HasValue)
                return val.Value;
            return defaultValue;
        }

        public static T? ParseToNullable<T>(this string str) where T : struct
        {
            Type t = typeof(T);
            if (t.IsEnum)
                return ToEnum<T>(str);
            return (T?)ParseTo(str, t.FullName);
        }

        private static object ParseTo(string str, string type)
        {
            switch (type)
            {
                case "System.Boolean":
                    return ToBoolean(str);
                case "System.SByte":
                    return ToSByte(str);
                case "System.Byte":
                    return ToByte(str);
                case "System.UInt16":
                    return ToUInt16(str);
                case "System.Int16":
                    return ToInt16(str);
                case "System.uInt32":
                    return ToUInt32(str);
                case "System.Int32":
                    return ToInt32(str);
                case "System.UInt64":
                    return ToUInt64(str);
                case "System.Int64":
                    return ToInt64(str);
                case "System.Single":
                    return ToSingle(str);
                case "System.Double":
                    return ToDouble(str);
                case "System.Decimal":
                    return ToDecimal(str);
                case "System.DateTime":
                    return ToDateTime(str);
                case "System.Guid":
                    return ToGuid(str);
                default:
                    throw new NotSupportedException(string.Format("The string of \"{0}\" can not be parsed to {1}", str, type));
            }
        }
        private static sbyte? ToSByte(string value)
        {
            sbyte ret;
            if (sbyte.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static byte? ToByte(string value)
        {
            byte ret;
            if (byte.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static ushort? ToUInt16(string value)
        {
            ushort ret;
            if (ushort.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static short? ToInt16(string value)
        {
            short ret;
            if (short.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static uint? ToUInt32(string value)
        {
            uint ret;
            if (uint.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static int? ToInt32(string value)
        {
            int ret;
            if (int.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static ulong? ToUInt64(string value)
        {
            ulong ret;
            if (ulong.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static long? ToInt64(string value)
        {
            long ret;
            if (long.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static float? ToSingle(string value)
        {
            float ret;
            if (float.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static double? ToDouble(string value)
        {
            double ret;
            if (double.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static decimal? ToDecimal(string value)
        {
            decimal ret;
            if (decimal.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static bool? ToBoolean(string value)
        {
            bool ret;
            if (bool.TryParse(value, out ret))
                return ret;
            return null;
        }
        private static T? ToEnum<T>(string str) where T : struct
        {
            T result;
            if (Enum.TryParse<T>(str, true, out result))
            {
                if (Enum.IsDefined(typeof(T), result))
                {
                    return result;
                }
            }
            return null;
        }
        private static Guid? ToGuid(string str)
        {
            Guid result;
            if (Guid.TryParse(str, out result))
                return result;
            return null;
        }
        private static DateTime? ToDateTime(string value)
        {
            DateTime ret;
            if (DateTime.TryParse(value, out ret))
                return ret;
            return null;
        }

        #endregion

        #region Validation

        public static bool IsEmail(this string input)
        {
            return Regex.IsMatch(input, RegexPatterns.Email);
        }

        /// <summary>
        /// 是否是手机号或家庭号码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsTelAndHome(this string input)
        {
            return Regex.IsMatch(input, RegexPatterns.TelAndHome);
        }
        

        public static bool IsBase64(this string input)
        {
            if (input == null)
                throw new ArgumentNullException();
            try
            {
                Convert.FromBase64String(input);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Others

        /// <summary>
        /// 将普通的字符串转化为Base64字符串。
        /// 将字符串转为字节数组时，使用的是UTF-16编码
        /// </summary>
        /// <param name="value">需要转化的字符串</param>
        /// <returns></returns>
        public static string ToBase64String(this string value)
        {
            if (value == null)
                throw new ArgumentNullException();

            var bytes = System.Text.Encoding.Unicode.GetBytes(value);
            return Convert.ToBase64String(bytes);
        }

        public static bool Eq(this string input, string toCompare, StringComparison comparison = StringComparison.OrdinalIgnoreCase)
        {
            return input.Equals(toCompare, comparison);
        }

        public static string ToWords(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }
            var chars = input.ToCharArray();
            var result = string.Empty;
            foreach (var c in chars)
            {
                var str = c.ToString();
                if (str == str.ToUpper())
                {
                    result += " ";
                }
                result += str;
            }
            return result.Trim();
        }

        #endregion

        /// <summary>
        /// Increases the randomly.
        /// </summary>
        /// <param name="str">The original string.</param>
        /// <param name="minLength">Length of the min.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="truncate">if set to <c>true</c> [truncate].</param>
        /// <returns>The string.</returns>
        public static string IncreaseRandomly(this string str, int minLength, int maxLength, bool truncate)
        {
            int num = new Random(minLength).Next(minLength, maxLength);
            return str.IncreaseTo(num, truncate);
        }

        /// <summary>
        /// Increases the original string to max length
        /// </summary>
        /// <param name="str">The original string.</param>
        /// <param name="maxLength">Length of the max.</param>
        /// <param name="truncate">if set to <c>true</c> [truncate].</param>
        /// <returns>The string.</returns>
        public static string IncreaseTo(this string str, int maxLength, bool truncate)
        {
            if (!string.IsNullOrEmpty(str))
            {
                if (str.Length == maxLength)
                {
                    return str;
                }
                if ((str.Length > maxLength) && truncate)
                {
                    return str.Truncate(maxLength);
                }
                string str2 = str;
                while (str.Length < maxLength)
                {
                    if ((str.Length + str2.Length) < maxLength)
                    {
                        str = str + str2;
                    }
                    else
                    {
                        str = str + str.Substring(0, maxLength - str.Length);
                    }
                }
            }
            return str;
        }

        /// <summary>
        /// Times the specified original string.
        /// </summary>
        /// <param name="str">The original string.</param>
        /// <param name="times">The times.</param>
        /// <returns>The string.</returns>
        public static string Times(this string str, int times)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            if (times <= 1)
            {
                return str;
            }
            string str2 = string.Empty;
            for (int i = 0; i < times; i++)
            {
                str2 = str2 + str;
            }
            return str2;
        }

        /// <summary>
        /// Truncates the specified string.
        /// </summary>
        /// <param name="txt">The original string.</param>
        /// <param name="maxChars">The max chars.</param>
        /// <returns>The truncated string.</returns>
        public static string Truncate(this string txt, int maxChars)
        {
            if (string.IsNullOrEmpty(txt))
            {
                return txt;
            }
            if (txt.Length <= maxChars)
            {
                return txt;
            }
            return txt.Substring(0, maxChars);
        }

        /// <summary>
        /// Truncates the with text.
        /// </summary>
        /// <param name="txt">The original string.</param>
        /// <param name="maxChars">The max chars.</param>
        /// <param name="suffix">The suffix.</param>
        /// <returns>The truncated string.</returns>
        public static string TruncateWithText(this string txt, int maxChars, string suffix)
        {
            if (string.IsNullOrEmpty(txt))
            {
                return txt;
            }
            if (txt.Length <= maxChars)
            {
                return txt;
            }
            return (txt.Substring(0, maxChars) + suffix);
        }

        /// <summary>
        /// Removes the non numeric chars
        /// </summary>
        /// <param name="s">The original string.</param>
        /// <returns>The string of non numeric char removed.</returns>
        public static string RemoveNonNumeric(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            var result = new char[s.Length];
            int resultIndex = 0;
            foreach (char c in s)
            {
                if (char.IsNumber(c))
                    result[resultIndex++] = c;
            }
            if (0 == resultIndex)
                s = string.Empty;
            else if (result.Length != resultIndex)
                s = new string(result, 0, resultIndex);

            return s;
        }

        /// <summary>
        /// Removes the line breaks, \r\n, \n, \r
        /// </summary>
        /// <param name="s">The original string .</param>
        /// <returns>The string without line breaks.</returns>
        public static string RemoveLineBreaks(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return s;
            }

            return s.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);
        }

        public static string ConvertCamelTextToWord(this string txt)
        {
            string result = string.Empty;
            char[] letters = txt.ToCharArray();
            foreach (char c in letters)
                if (c.ToString() != c.ToString().ToLower())
                    result += " " + c.ToString();
                else
                    result += c.ToString();
            return result;
        }

        public static bool IsInt(this string txt)
        {
            if (string.IsNullOrWhiteSpace(txt)) return false;
            return txt.All(str => str >= '0' && str <= '9');
        }

        #region Hex and Binary

        /// <summary>
        /// Determines whether the string contains valid hexadecimal characters only.
        /// </summary>
        /// <param name="txt">String to check.</param>
        /// <returns>True if the string contains valid hexadecimal characters.</returns>
        /// <remarks>An empty or null string is considered to <b>not</b> contain
        /// valid hexadecimal characters.</remarks>
        public static bool IsHex(this string txt)
        {
            return (!string.IsNullOrEmpty(txt) &&
                    (string.IsNullOrEmpty(txt.ReplaceChars("0123456789ABCDEFabcdef", "                      ").Trim())));
        }


        /// <summary>
        /// Determines whether the string contains valid binary characters only.
        /// </summary>
        /// <param name="txt">String to check.</param>
        /// <returns>True if the string contains valid binary characters.</returns>
        /// <remarks>An empty or null string is considered to <b>not</b> contain
        /// valid binary characters.</remarks>
        public static bool IsBinary(this string txt)
        {
            return (!string.IsNullOrEmpty(txt)) && (string.IsNullOrEmpty(txt.ReplaceChars("01", "  ").Trim()));
        }

        /// <summary>
        /// Returns the hexadecimal representation of a decimal number.
        /// </summary>
        /// <param name="txt">Hexadecimal string to convert to decimal.</param>
        /// <returns>Decimal representation of string.</returns>
        public static string DecimalToHex(this string txt)
        {
            return Convert.ToInt32(txt).ToHex();
        }


        /// <summary>
        /// Returns the binary representation of a binary number.
        /// </summary>
        /// <param name="txt">Decimal string to convert to binary.</param>
        /// <returns>Binary representation of string.</returns>
        public static string DecimalToBinary(this string txt)
        {
            return Convert.ToInt32(txt).ToBinary();
        }


        /// <summary>
        /// Returns the decimal representation of a hexadecimal number.
        /// </summary>
        /// <param name="txt">Hexadecimal string to convert to decimal.</param>
        /// <returns>Decimal representation of string.</returns>
        public static string HexToDecimal(this string txt)
        {
            return Convert.ToString(Convert.ToInt32(txt, 16));
        }


        /// <summary>
        /// Returns the binary representation of a hexadecimal number.
        /// </summary>
        /// <param name="txt">Binary string to convert to hexadecimal.</param>
        /// <returns>Hexadecimal representation of string.</returns>
        public static string HexToBinary(this string txt)
        {
            return Convert.ToString(Convert.ToInt32(txt, 16), 2);
        }


        /// <summary>
        /// Converts a hexadecimal string to a byte array representation.
        /// </summary>
        /// <param name="txt">Hexadecimal string to convert to byte array.</param>
        /// <returns>Byte array representation of the string.</returns>
        /// <remarks>The string is assumed to be of even size.</remarks>
        public static byte[] HexToByteArray(this string txt)
        {
            var b = new byte[txt.Length / 2];
            for (int i = 0; i < txt.Length; i += 2)
            {
                b[i / 2] = Convert.ToByte(txt.Substring(i, 2), 16);
            }
            return b;
        }


        /// <summary>
        /// Converts a byte array to a hexadecimal string representation.
        /// </summary>
        /// <param name="b">Byte array to convert to hexadecimal string.</param>
        /// <returns>String representation of byte array.</returns>
        public static string ByteArrayToHex(this byte[] b)
        {
            return BitConverter.ToString(b).Replace("-", "");
        }


        /// <summary>
        /// Returns the hexadecimal representation of a binary number.
        /// </summary>
        /// <param name="txt">Binary string to convert to hexadecimal.</param>
        /// <returns>Hexadecimal representation of string.</returns>
        public static string BinaryToHex(this string txt)
        {
            return Convert.ToString(Convert.ToInt32(txt, 2), 16);
        }


        /// <summary>
        /// Returns the decimal representation of a binary number.
        /// </summary>
        /// <param name="txt">Binary string to convert to decimal.</param>
        /// <returns>Decimal representation of string.</returns>
        public static string BinaryToDecimal(this string txt)
        {
            return Convert.ToString(Convert.ToInt32(txt, 2));
        }

        #endregion

        #region Replacement

        /// <summary>
        /// Replaces the characters in the originalChars string with the
        /// corresponding characters of the newChars string.
        /// </summary>
        /// <param name="txt">String to operate on.</param>
        /// <param name="originalChars">String with original characters.</param>
        /// <param name="newChars">String with replacement characters.</param>
        /// <example>For an original string equal to "123456654321" and originalChars="35" and
        /// newChars "AB", the result will be "12A4B66B4A21".</example>
        /// <returns>String with replaced characters.</returns>
        public static string ReplaceChars(this string txt, string originalChars, string newChars)
        {
            string returned = "";

            for (int i = 0; i < txt.Length; i++)
            {
                int pos = originalChars.IndexOf(txt.Substring(i, 1));

                if (-1 != pos)
                    returned += newChars.Substring(pos, 1);
                else
                    returned += txt.Substring(i, 1);
            }
            return returned;
        }

        #endregion

        public static List<long> ToIdList(this string str)
        {
            var result = new List<long>();
            if (string.IsNullOrEmpty(str)) return result;
            var ids = str.Split(new[] { ',', '，' }).Select(m => m.Trim()).ToList();
            ids.ForEach(m =>
            {
                long id;
                long.TryParse(m, out id);
                if (id > 0)
                {
                    result.Add(id);
                }
            });
            return result;
        }

        public static string ToStringX(this IList<string> list)
        {
            string re = string.Empty;
            if (list.Count == 0) return re;
            re = list[0];
            for (int i = 1; i < list.Count; i++)
            {
                re = string.Join(",", re, list[i]);
            }
            return re;
        }

        public static string ToNumberString(this string str)
        {
            var result = string.Empty;
            for (var i = 0; i < str.Length; i++)
            {
                if (char.IsNumber(str, i))
                {
                    result += str.Substring(i, 1);
                }
            }
            return result;
        }

        public static string LongToThreeDigit(this string str)
        {
            if (str.Length >= 3)
                return str.Substring(0, 3);
            if (str.Length == 2)
                return str + "0";
            if (str.Length == 1)
                return str + "00";
            return "000";
        }
    }
}
