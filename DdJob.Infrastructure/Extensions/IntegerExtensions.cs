using System;
using System.Collections.Generic;

namespace DdJob
{
    /// <summary>
    ///   Defines extension methods for the integer type
    /// </summary>
    public static class IntegerExtensions
    {
        /// <summary>
        ///   TimeSpan the specified number days.
        /// </summary>
        /// <param name = "num">The num.</param>
        /// <returns>The time spans</returns>
        public static TimeSpan Days(this int num)
        {
            return new TimeSpan(num, 0, 0, 0);
        }

        /// <summary>
        ///   Datetime for the specific number days ago.
        /// </summary>
        /// <param name = "num">The number of day.</param>
        /// <returns>The datetime for the specific number days ago.</returns>
        public static DateTime DaysAgo(this int num)
        {
            return DateTime.Now.AddDays(-num);
        }

        /// <summary>
        ///   Datetime for the specific number days from now.
        /// </summary>
        /// <param name = "num">The num.</param>
        /// <returns>Datetime for the specific number days from now.</returns>
        public static DateTime DaysFromNow(this int num)
        {
            return DateTime.Now.AddDays(num);
        }

        /// <summary>
        ///   Downtoes the specified end.
        /// </summary>
        /// <param name = "end">The end.</param>
        /// <param name = "start">The start.</param>
        /// <param name = "action">The action.</param>
        public static void Downto(this int end, int start, Action<int> action)
        {
            for (int i = end; i >= start; i--)
            {
                action(i);
            }
        }

        /// <summary>
        ///   The specified number hours.
        /// </summary>
        /// <param name = "num">The num.</param>
        /// <returns>The TimeSpan for the specified number hours.</returns>
        public static TimeSpan Hours(this int num)
        {
            return new TimeSpan(0, num, 0, 0);
        }

        /// <summary>
        ///   The datetime for the specified hours ago.
        /// </summary>
        /// <param name = "num">The number of hours.</param>
        /// <returns>The datetime for the specified hours ago.</returns>
        public static DateTime HoursAgo(this int num)
        {
            return DateTime.Now.AddHours(-num);
        }

        /// <summary>
        ///   The datetime for the specified hours from now.
        /// </summary>
        /// <param name = "num">The number of hours.</param>
        /// <returns>The datetime for the specified from now.</returns>
        public static DateTime HoursFromNow(this int num)
        {
            return DateTime.Now.AddHours(num);
        }

        /// <summary>
        ///   Determines whether the specified num is even.
        /// </summary>
        /// <param name = "num">The num.</param>
        /// <returns>
        ///   <c>true</c> if the specified num is even; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsEven(this int num)
        {
            return ((num % 2) == 0);
        }

        /// <summary>
        ///   Determines whether [is leap year] [the specified year].
        /// </summary>
        /// <param name = "year">The year.</param>
        /// <returns>
        ///   <c>true</c> if [is leap year] [the specified year]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLeapYear(this int year)
        {
            return (((year % 4) == 0) && (((year % 100) != 0) || ((year % 400) == 0)));
        }

        /// <summary>
        ///   Determines whether the specified num is odd.
        /// </summary>
        /// <param name = "num">The num.</param>
        /// <returns>
        ///   <c>true</c> if the specified num is odd; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsOdd(this int num)
        {
            return ((num % 2) != 0);
        }

        /// <summary>
        ///   Kiloes the bytes.
        /// </summary>
        /// <param name = "num">The number.</param>
        /// <returns>The number converted to kilo</returns>
        public static int KiloBytes(this int num)
        {
            return (num * 0x3e8);
        }

        /// <summary>
        ///   Megas the bytes.
        /// </summary>
        /// <param name = "num">The number.</param>
        /// <returns>The number converted to mergas</returns>
        public static int MegaBytes(this int num)
        {
            return (num * 0xf4240);
        }

        /// <summary>
        ///   Minuteses the specified number.
        /// </summary>
        /// <param name = "num">The number.</param>
        /// <returns>The TimeSpan for the specified number minutes.</returns>
        public static TimeSpan Minutes(this int num)
        {
            return new TimeSpan(0, 0, num, 0);
        }

        /// <summary>
        ///   Get the datetime for the specified number of minutes ago.
        /// </summary>
        /// <param name = "num">The number of minutes.</param>
        /// <returns>The datetime for the specified number of minutes ago.</returns>
        public static DateTime MinutesAgo(this int num)
        {
            return DateTime.Now.AddMinutes(-num);
        }

        /// <summary>
        ///   Get the datetime for the specified number of minutes from now.
        /// </summary>
        /// <param name = "num">The number of minutes.</param>
        /// <returns>The datetime for the specified number of minutes from now.</returns>
        public static DateTime MinutesFromNow(this int num)
        {
            return DateTime.Now.AddMinutes(num);
        }

        /// <summary>
        ///   Get the datetime for the specified number of months ago.
        /// </summary>
        /// <param name = "num">The number of months.</param>
        /// <returns>The datetime for the specified number of months ago</returns>
        public static DateTime MonthsAgo(this int num)
        {
            return DateTime.Now.AddMonths(-num);
        }

        /// <summary>
        ///   Get the datetime for the specified number of months from now.
        /// </summary>
        /// <param name = "num">The number of months.</param>
        /// <returns>The datetime for the specified number of months from now</returns>
        public static DateTime MonthsFromNow(this int num)
        {
            return DateTime.Now.AddMonths(num);
        }

        /// <summary>
        ///   Get the TimeSpan for the specified number of seconds.
        /// </summary>
        /// <param name = "num">The number of seconds.</param>
        /// <returns>The TimeSpan for the specified number of seconds</returns>
        public static TimeSpan Seconds(this int num)
        {
            return new TimeSpan(0, 0, 0, num);
        }

        /// <summary>
        ///   Get the specified number teras.
        /// </summary>
        /// <param name = "num">The number of teras.</param>
        /// <returns>The number of taras</returns>
        public static int TeraBytes(this int num)
        {
            return (num * 0x3b9aca00);
        }

        /// <summary>
        ///   Times the specified number.
        /// </summary>
        /// <param name = "num">The number.</param>
        /// <returns>The TimeSpan</returns>
        public static TimeSpan Time(this int num)
        {
            return num.Time(false);
        }

        /// <summary>
        ///   Times the specified number.
        /// </summary>
        /// <param name = "num">The number.</param>
        /// <param name = "convertSingleDigitsToHours">if set to <c>true</c> [convert single digits to hours].</param>
        /// <returns>The TimeSpan</returns>
        public static TimeSpan Time(this int num, bool convertSingleDigitsToHours)
        {
            if (convertSingleDigitsToHours && (num <= 0x18))
            {
                num *= 100;
            }
            int hours = num / 100;
            return new TimeSpan(hours, num % 100, 0);
        }

        /// <summary>
        ///   Timeses the specified NDX.
        /// </summary>
        /// <param name = "ndx">The NDX.</param>
        /// <param name = "action">The action.</param>
        public static void Times(this int ndx, Action<int> action)
        {
            for (int i = 0; i < ndx; i++)
            {
                action(i);
            }
        }


        /// <summary>
        /// Uptoes the specified start.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        /// <param name="action">The action.</param>
        public static void Upto(this int start, int end, Action<int> action)
        {
            for (int i = start; i <= end; i++)
            {
                action(i);
            }
        }

        /// <summary>
        ///   Get the datetime for the specified number of  Years ago.
        /// </summary>
        /// <param name = "num">The number of years.</param>
        /// <returns>The datetime for the specified number of  Years ago.</returns>
        public static DateTime YearsAgo(this int num)
        {
            return DateTime.Now.AddYears(-num);
        }

        /// <summary>
        ///   Get the datetime for the specified number of  Years from now.
        /// </summary>
        /// <param name = "num">The number of years.</param>
        /// <returns> The datetime for the specified number of  Years from now.</returns>
        public static DateTime YearsFromNow(this int num)
        {
            return DateTime.Now.AddYears(num);
        }

        /// <summary>
        ///   Gets the enum value.
        /// </summary>
        /// <typeparam name = "T">The type of enum</typeparam>
        /// <param name = "enumValue">The enum value.</param>
        /// <returns>The enum</returns>
        public static T GetEnum<T>(this int? enumValue) // where T : struct
        {
            return GetEnum<T>(enumValue.GetValueOrDefault());
        }

        /// <summary>
        ///   Gets the enum.
        /// </summary>
        /// <typeparam name = "T">The type of enum</typeparam>
        /// <param name = "enumValue">The enum value.</param>
        /// <returns>The enum</returns>
        public static T GetEnum<T>(this int enumValue) // where T : struct
        {
            return GetEnum<T>(enumValue.ToString());
        }

        /// <summary>
        ///   Gets the enum.
        /// </summary>
        /// <typeparam name = "T">The type of enum</typeparam>
        /// <param name = "enumValue">The enum value.</param>
        /// <returns>The enum</returns>
        public static T GetEnum<T>(this string enumValue) // where T : struct
        {
            T retval = default(T);
            Type t = typeof(T);
            try
            {
                if (!string.IsNullOrEmpty(enumValue))
                    retval = (T)Enum.Parse(t, enumValue);
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
            }
            return retval;
        }

        #region Hexadecimal and binary

        /// <summary>
        ///   Returns a hexadecimal string representation of the number.
        /// </summary>
        /// <param name = "number">The number</param>
        /// <returns>The string</returns>
        public static string ToHex(this int number)
        {
            return Convert.ToString(number, 16);
        }


        /// <summary>
        ///   Returns a binary string representation of the number.
        /// </summary>
        /// <param name = "number">The number</param>
        /// <returns>The string</returns>
        public static string ToBinary(this int number)
        {
            return Convert.ToString(number, 2);
        }

        #endregion

        public static bool HasElement(this List<long> list)
        {
            return list != null && list.Count > 0;
        }
        public static bool HasNoneZeroElement(this List<long> list)
        {
            return list != null && list.Count > 0 && list[0] != 0;
        }
    }
}