﻿namespace DdJob
{
    public static partial class StringExtensions
    {
        public static string ToHtml(this string txt)
        {
            return string.IsNullOrWhiteSpace(txt)
                       ? string.Empty
                       : txt.Replace("\n", "<br/>").Replace("\r\n", "<br/>").Replace(" ", "&nbsp;");
        }

        public static string ToText(this string txt)
        {
            return string.IsNullOrWhiteSpace(txt) ? string.Empty : txt.Replace("<br/>", "\r\n").Replace("&nbsp;", " ");
        }
    }
}
