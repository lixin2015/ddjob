﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;

namespace DdJob
{
    /// <summary>
    ///   Defines extension methods for object
    /// </summary>
    public static class ObjectExtensions
    {
        /// <summary>
        /// Takes an object and turns it into a dictionary. Each public property is
        /// added to the dictionary, with the name of the property being the dictionary key,
        /// and its value being the dictionary value.
        /// </summary>
        /// <param name="object">The @object.</param>
        /// <returns>The dictionary object.</returns>
        /// <remarks>
        /// Particularly useful for dealing with anonymous type declaration passed as objects
        /// to a method.
        /// </remarks>
        public static Dictionary<string, object> ToDictionary(this object @object)
        {
            var properties = TypeDescriptor.GetProperties(@object);
            var hash = new Dictionary<string, object>(properties.Count);
            foreach (PropertyDescriptor descriptor in properties)
            {
                hash.Add(descriptor.Name, descriptor.GetValue(@object));
            }
            return hash;
        }

        /// <summary>
        ///   Determines if the object is in the specific list.
        /// </summary>
        /// <param name = "object">The @object.</param>
        /// <param name = "list">The list.</param>
        /// <returns><c>true</c> if the object existed in the list; otherwise, <c>false</c>.</returns>
        public static bool In(this object @object, IEnumerable list)
        {
            return list.Cast<object>().Contains(@object);
        }

        /// <summary>
        /// Returns true if the object is marked with the specified attribute
        /// </summary>
        /// <param name="object">The @object.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <returns>
        ///   <c>true</c> if the object contains the specific attribute type; otherwise, <c>false</c>.
        /// </returns>
        public static bool DefinesAttribute(this object @object, string attributeType)
        {
            return @object.DefinesAttribute(Type.GetType(attributeType, true, true));
        }

        /// <summary>
        /// Returns true if the object is marked with the specified attribute
        /// </summary>
        /// <param name="object">The @object.</param>
        /// <param name="attributeType">Type of the attribute.</param>
        /// <returns>
        ///   <c>true</c> if the object contains the specific attribute type; otherwise, <c>false</c>.
        /// </returns>
        public static bool DefinesAttribute(this object @object, Type attributeType)
        {
            object[] definedAttributes = @object.GetType().GetCustomAttributes(attributeType, true);
            return (definedAttributes.Length > 0);
        }

        /// <summary>
        /// Gets the custom attribute.
        /// </summary>
        /// <typeparam name="T">The type of custom attribute.</typeparam>
        /// <param name="object">The @object.</param>
        /// <returns>
        ///   <c>true</c> if the object contains the specific attribute type; otherwise, <c>false</c>.
        /// </returns>
        public static IList<T> GetCustomAttribute<T>(this object @object) where T : Attribute
        {
            var retval = new List<T>();

            if (@object is ICustomAttributeProvider)
            {
                object[] attrs = (@object as ICustomAttributeProvider).GetCustomAttributes(typeof(T), true);
                attrs.Each(attr => retval.Add(attr as T));
            }
            else
            {
                object[] attrs = @object.GetType().GetCustomAttributes(typeof(T), true);
                attrs.Each(attr => retval.Add(attr as T));
            }

            return retval;
        }

        /// <summary>
        ///   Deeps the clone.
        /// </summary>
        /// <typeparam name = "T">The type of object.</typeparam>
        /// <param name = "a">A.</param>
        /// <returns>The cloned object.</returns>
        public static T DeepClone<T>(this T a)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }

        public static T Copy<T>(this T src, T des)
        {
            for (var i = 0; i < des.GetType().GetProperties().Length; i++)
            {
                var oOldObjectProperty = (PropertyInfo)src.GetType().GetProperties().GetValue(i);
                var oOldObjectValue = oOldObjectProperty.GetValue(src, null);

                var oNewObjectProperty = (PropertyInfo)des.GetType().GetProperties().GetValue(i);

                if (oOldObjectProperty.CanRead && oNewObjectProperty.CanWrite)
                {
                    oNewObjectProperty.SetValue(des, oOldObjectValue, null);
                }
            }
            return des;
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <returns>The converted value.</returns>
        public static object To(this object value, Type destinationType)
        {
            return To(value, destinationType, CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <param name="destinationType">The type to convert the value to.</param>
        /// <param name="culture">Culture</param>
        /// <returns>The converted value.</returns>
        public static object To(this object value, Type destinationType, CultureInfo culture)
        {
            if (value != null)
            {
                var sourceType = value.GetType();

                TypeConverter destinationConverter = GetCustomTypeConverter(destinationType);
                TypeConverter sourceConverter = GetCustomTypeConverter(sourceType);
                if (destinationConverter != null && destinationConverter.CanConvertFrom(value.GetType()))
                    return destinationConverter.ConvertFrom(null, culture, value);
                if (sourceConverter != null && sourceConverter.CanConvertTo(destinationType))
                    return sourceConverter.ConvertTo(null, culture, value, destinationType);
                if (destinationType.IsEnum && value is int)
                    return Enum.ToObject(destinationType, (int)value);
                if (!destinationType.IsInstanceOfType(value))
                    return Convert.ChangeType(value, destinationType, culture);
            }
            return value;
        }

        /// <summary>
        /// Converts a value to a destination type.
        /// </summary>
        /// <param name="value">The value to convert.</param>
        /// <typeparam name="T">The type to convert the value to.</typeparam>
        /// <returns>The converted value.</returns>
        public static T To<T>(this object value)
        {
            //return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            return (T)To(value, typeof(T));
        }


        public static TypeConverter GetCustomTypeConverter(Type type)
        {
            //we can't use the following code in order to register our custom type descriptors
            //TypeDescriptor.AddAttributes(typeof(List<int>), new TypeConverterAttribute(typeof(GenericListTypeConverter<int>)));
            //so we do it manually here

            if (type == typeof(List<int>))
                return new GenericListTypeConverter<int>();
            if (type == typeof(List<decimal>))
                return new GenericListTypeConverter<decimal>();
            if (type == typeof(List<string>))
                return new GenericListTypeConverter<string>();

            return TypeDescriptor.GetConverter(type);
        }
        /// <summary>
        /// 比较两个对象的所有属性是否相同.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="o1"></param>
        /// <param name="o2"></param>
        /// <returns></returns>
        public static bool EqualAllFields<T>(this T o1, T o2)
        {
            Type type = typeof(T);
            var fields = type.GetProperties();

            foreach (var field in fields)
            {
                var v1 = field.GetValue(o1);
                var v2 = field.GetValue(o2);
                if (v1 == null && v2 == null)
                {
                    continue;
                }
                else if (v1 != null && v2 != null && v1.Equals(v2))
                {
                    continue;
                }
                else
                {
                    return false;
                }

            }
            return true;
        }

        /// <summary>
        /// 将对象的所有属性复制到目标对象中
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <param name="target"></param>
        public static void CopyAllFieldsTo<T>(this T src, T target)
        {
            Type type = typeof(T);
            var fields = type.GetProperties();

            foreach (var field in fields)
            {
                var sValue = field.GetValue(src);
                field.SetValue(target, sValue);
            }
        }

        public static string GenerateUniqueNumber()
        {
            Random ran = new Random();
            string strDateTimeNumber = DateTime.Now.ToString("yyyyMMddHHmmssms");
            return strDateTimeNumber + ran.Next(100, 999);
        }
    }
}