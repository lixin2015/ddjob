﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DdJob
{
    public static class WhereInExtention
    {
        public static IQueryable<TEntity> WhereIn<TEntity, TValue>(this IQueryable<TEntity> query,
           Expression<Func<TEntity, TValue>> selector,
           IEnumerable<TValue> collection)
        {
            if (selector == null) throw new ArgumentNullException("selector");
            if (collection == null) throw new ArgumentNullException("collection");
            var values = collection as TValue[] ?? collection.ToArray();
            if (!values.Any())
                return query.Where(t => true);

            var p = selector.Parameters.Single();

            var equals = values.Select(value =>
               (Expression)Expression.Equal(selector.Body,
                    Expression.Constant(value, typeof(TValue))));

            var body = equals.Aggregate(Expression.Or);

            return query.Where(Expression.Lambda<Func<TEntity, bool>>(body, p));
        }

        //Optional - to allow static collection:
        public static IQueryable<TEntity> WhereIn<TEntity, TValue>(this IQueryable<TEntity> query,
            Expression<Func<TEntity, TValue>> selector,
            params TValue[] collection)
        {
            return WhereIn(query, selector, (IEnumerable<TValue>)collection);
        }


    }
}
