﻿using System;

namespace DdJob
{
    public static class DateTimeExtensions
    {
        public static DateTime ConvertToUtcTime(this DateTime dateTime)
        {
            return dateTime.ConvertToUtcTime(dateTime.Kind);
        }

        public static DateTime ConvertToUtcTime(this DateTime dateTime, DateTimeKind sourceDateTimeKind)
        {
            dateTime = DateTime.SpecifyKind(dateTime, sourceDateTimeKind);
            return TimeZoneInfo.ConvertTimeToUtc(dateTime);
        }

        public static DateTime ConvertToUtcTime(this DateTime dateTime, TimeZoneInfo sourceTimeZone)
        {
            if (sourceTimeZone.IsInvalidTime(dateTime))
            {
                //could not convert
                return dateTime;
            }
            return TimeZoneInfo.ConvertTimeToUtc(dateTime, sourceTimeZone);
        }

        public static DateTime ConvertToUtcWithNoValid(this DateTime dateTime, TimeZoneInfo sourceTimeZone)
        {
            return TimeZoneInfo.ConvertTime(dateTime, sourceTimeZone, TimeZoneInfo.Utc);
        }

        public static DateTime? ConvertToUtcTime(this DateTime? dateTime, TimeZoneInfo sourceTimeZone)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ConvertToUtcTime(sourceTimeZone);
            }
            return null;
        }

        public static DateTime? ConvertToUtcWithNoValid(this DateTime? dateTime, TimeZoneInfo sourceTimeZone)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ConvertToUtcWithNoValid(sourceTimeZone);
            }
            return null;
        }

        public static DateTime ConvertToUserTime(this DateTime dateTime, TimeZoneInfo sourceTimeZone, TimeZoneInfo destinationTimeZone)
        {
            return TimeZoneInfo.ConvertTime(dateTime, sourceTimeZone, destinationTimeZone);
        }

        public static DateTime? ConvertToUserTimeWithNoValid(this DateTime? dateTime, TimeZoneInfo destinationTimeZone)
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ConvertToUserTime(TimeZoneInfo.Utc, destinationTimeZone);
            }
            return null;
        }

        public static DateTime ConvertToUserTimeById(this DateTime dateTime, string zoneId)
        {
            var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(zoneId);
            return TimeZoneInfo.ConvertTime(dateTime, timeZoneInfo);
        }

        public static DateTime ConvertToUserTimeById(this DateTime dateTime, string fromZoneId, string toZoneId)
        {
            var from = TimeZoneInfo.FindSystemTimeZoneById(fromZoneId);
            var to = TimeZoneInfo.FindSystemTimeZoneById(toZoneId);
            return TimeZoneInfo.ConvertTime(dateTime, from, to);
        }

        public static DateTime ConvertUtcToPst(this DateTime dateTime)
        {
            var pstTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.Utc, pstTimeZoneInfo);
        }

        public static DateTime ConvertPstToUtc(this DateTime dateTime)
        {
            var pstTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            return TimeZoneInfo.ConvertTime(dateTime, pstTimeZoneInfo, TimeZoneInfo.Utc);
        }

        public static DateTime ConvertUtcToCst(this DateTime dateTime)
        {
            var pstTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.Utc, pstTimeZoneInfo);
        }

        public static DateTime ConvertCstToUtc(this DateTime dateTime)
        {
            var pstTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            return TimeZoneInfo.ConvertTime(dateTime, pstTimeZoneInfo, TimeZoneInfo.Utc);
        }
    }
}
