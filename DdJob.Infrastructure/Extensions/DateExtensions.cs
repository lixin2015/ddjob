﻿using System;
using System.Globalization;
using System.Web;

namespace DdJob
{
    public static class DateExtensions
    {
        public const string DateFormatString = "yyyy-MM-dd";
        public const string ReportDateFormatString = "yyyy/MM/dd";
        public const string ReportDateTimeFormatString = "yyyy/MM/dd HH:mm:ss";
        public const string DateTimeFormatString = "yyyy-MM-dd HH:mm";
        public const string TimeFormatString = "HH:mm";
        public const string DateFormat = "{0:yyyy-MM-dd}";
        public const string DateTimeFormat = "{0:yyyy-MM-dd HH:mm}";
        public const string DateTimeFullFormat = "yyyyMMddHHmmss";
        public const string TimeFormat = "{0:HH:mm}";

        public static int CompareDate(this DateTime? date, DateTime? dateTime)
        {
            if (date == null && dateTime == null)
                return 0;
            if (date == null)
                return -1;
            if (dateTime == null)
                return 1;

            return date.Value.Date.CompareTo(dateTime.Value.Date);
        }

        /// <summary>
        /// 格式: yyyy-MM-dd
        /// </summary>
        /// <param name="dateTime"></param>
        public static string ToDateFormat(this DateTime dateTime)
        {
            return dateTime.ToString(DateFormatString);
        }
        public static string ToDateFormat(this DateTime? dateTime)
        {
            return dateTime.HasValue ? dateTime.Value.ToString(DateFormatString) : "";
        }
        public static string ToReportDateFormat(this DateTime dateTime)
        {
            return dateTime.ToString(ReportDateFormatString);
        }

        public static string ToReportDateFormat(this DateTime? dateTime)
        {
            return dateTime.HasValue ? dateTime.Value.ToString(ReportDateFormatString) : "";
        }
        public static string ToReportDateCriteriasFormat(this DateTime? dateTime, string defaultResult)
        {
            return dateTime.HasValue ? dateTime.Value.ToString(ReportDateFormatString) : defaultResult;
        }

        public static string ToReportDateTimeFormat(this DateTime dateTime)
        {
            return dateTime.ToString(ReportDateTimeFormatString);
        }

        public static string ToReportDateTimeFormat(this DateTime? dateTime)
        {
            return dateTime.HasValue ? dateTime.Value.ToString(ReportDateTimeFormatString) : "";
        }

        public static string ToWxPushDateTimeFormat(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy年MM月dd日 HH:mm");
        }

        /// <summary>
        /// 格式: yyyy-MM-dd HH:mm
        /// </summary>
        /// <param name="dateTime"></param>
        public static string ToDateTimeFormat(this DateTime dateTime)
        {
            return dateTime.ToString(DateTimeFormatString);
        }

        /// <summary>
        /// 格式: HH:mm
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ToTimeFormat(this DateTime dateTime)
        {
            return dateTime.ToString(TimeFormatString);
        }

        public static string ToTimeFormat(this TimeSpan timeSpan)
        {
            return (DateTime.Now.Date + timeSpan).ToString(TimeFormatString);
        }

        ///// <summary>
        ///// 今天否为工作日
        ///// </summary>
        //public static bool TodayIsWeekday(this object obj)
        //{
        //    var week = DateTime.Now.DayOfWeek;
        //    return week != DayOfWeek.Saturday && week != DayOfWeek.Sunday;
        //}

        public static string ToLongDateTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("ddd MMM d hh:mm tt");
        }

        public static string ToZZBShortDateString(this DateTime dateTime)
        {
            return dateTime.ToString("MMM dd, yyyy");
        }

        public static string ToZZBShortDateStringWithSpace(this DateTime dateTime)
        {
            return dateTime.ToString("MMM dd yyyy");
        }

        public static string ToShortDateStringForDatepicker(this DateTime dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy");
        }

        public static string ToShortDateStringCustom(this DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMdd");
        }

        public static string ToDateTimeFullStringCustom(this DateTime dateTime)
        {
            return dateTime.ToString(DateTimeFullFormat);
        }

        public static string ToShortDateStringForExcel(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy/MM/dd");
        }

        public static string ToShortDateStringForDatepicker(this DateTime? dateTime, string emptyStr = "")
        {
            if (!dateTime.HasValue)
            {
                return emptyStr;
            }
            return dateTime.Value.ToString("MM/dd/yyyy");
        }

        public static string ToDateMonthString(this DateTime dateTime)
        {
            return dateTime.ToString("MMM dd");
        }

        public static string ToZZBLongTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("hh:mm tt");
        }

        public static string ToShortTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("HH:mm");
        }

        public static string ToShortDateAndTimeString(this DateTime dateTime)
        {
            return dateTime.ToString("MMM dd - hh:mm tt");
        }

        public static string ToYearMonthString(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy MM");
        }

        public static string ToWeekDayMonthString(this DateTime dateTime)
        {
            return dateTime.ToString("ddd MMM");
        }

        public static string ToZZBShortDateString(this DateTime? time, string defautEmptyStr = "")
        {
            if (time.HasValue)
            {
                return time.Value.ToZZBShortDateString();
            }
            return defautEmptyStr;
        }

        public static string ToZZBShortDateStringWithSpace(this DateTime? time, string defautEmptyStr = "")
        {
            if (time.HasValue)
            {
                return time.Value.ToZZBShortDateStringWithSpace();
            }
            return defautEmptyStr;
        }

        public static string ToZZBLongTimeString(this DateTime? dateTime, string defautEmptyStr = "")
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToZZBLongTimeString();
            }
            return defautEmptyStr;
        }

        public static string ToPSTString(this DateTime dateTime)
        {
            var time = System.TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime, "Pacific Standard Time");
            return time.ToString("MMM dd, yyyy HH:mm:ss") + " PST";
        }
        public static string ToPSTString(this DateTime? dateTime, string defautEmptyStr = "")
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToPSTString();
            }
            return defautEmptyStr;
        }
        public static HtmlString ToShortWeekAndDateString(this DateTime dateTime)
        {
            var dateStr = dateTime.ToString("ddd MMM dd <br/>yyyy");
            return new HtmlString(dateStr);
        }

        public static HtmlString ToShortDateStringWithBr(this DateTime dateTime)
        {
            var dateStr = dateTime.ToString("MMM dd <br/>yyyy");
            return new HtmlString(dateStr);
        }

        public static HtmlString ToShortDateStringWithBr(this DateTime? dateTime, string defautEmptyStr = "")
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToShortDateStringWithBr();
            }
            return new HtmlString(defautEmptyStr);
        }

        public static HtmlString ToShortWeekAndDateString(this DateTime? dateTime, string defaultEmptyStr = "")
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToShortWeekAndDateString();
            }
            return new HtmlString(defaultEmptyStr);
        }

        public static HtmlString ToDayMonthYearString(this DateTime dateTime)
        {
            return new HtmlString(dateTime.ToString("dd-MMM-yy"));
        }

        public static HtmlString ToDayMonthYearString(this DateTime? dateTime, string defaultEmptyStr = "")
        {
            if (dateTime.HasValue)
            {
                return dateTime.Value.ToDayMonthYearString();
            }
            return new HtmlString(defaultEmptyStr);
        }

        public static string GetTimeStampString(this object obj)
        {
            var ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds).ToString(CultureInfo.InvariantCulture);
        }

        public static long GetTimeStamp(this object obj)
        {
            var ts = DateTime.UtcNow - new DateTime(2013, 11, 07, 02, 13, 24, 100);
            return Convert.ToInt64(ts.TotalMilliseconds);
        }
    }
}
