using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using DdJob.Extensions;

namespace DdJob.Configuration
{
    ///<summary>
    ///  A Configuration Base, which provides access the App.Config / Web.Config settings.
    ///  Reading these settings and insert them into one Dictionary List
    ///</summary>
    ///<remarks>
    ///  It's better to create new sealed class to list all of settings of the App.Config or Web.Config
    ///</remarks>
    ///<example>
    ///  <code>
    ///    public partial class Config : AppConfigBase
    ///    {
    ///         private static readonly IDictionary&lt;string, string&gt; mProperties = new Dictionary&lt;string, string&gt;();
    ///
    ///         static Config()
    ///         {
    ///             Initialize();
    ///         }
    ///
    ///         private static void Initialize()
    ///         {
    ///             FillCollection("zzb.Common", mProperties);
    ///
    /// #if DEBUG
    ///             FillCollection("zzb.Debug", mProperties);
    /// #endif
    ///         }
    ///		       
    ///         public static string ProductName
    ///         {
    ///             get
    ///             {
    ///                 return mProperties.GetStringVal("ProductName", "defaultVal");
    ///             }
    ///         }
    ///    }
    ///  </code>
    ///</example>
    public class AppConfigBase
    {
        /// <summary>
        ///   Fills these settings into pProperties
        /// </summary>
        /// <param name = "pSection">The configuration section.</param>
        /// <param name = "pProperties">The p properties.</param>
        protected static void FillCollection(string pSection, IDictionary<string, string> pProperties)
        {
            try
            {
                object lObject = ConfigurationManager.GetSection(pSection);
                if (lObject == null)
                {
                }
                else
                {
                    var lNameValueCollection = lObject as NameValueCollection;
                    if (lNameValueCollection != null)
                    {
                        foreach (string lKey in lNameValueCollection)
                        {
                            pProperties[lKey] = lNameValueCollection[lKey];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(String.Format("error loading configsection {0}", pSection));
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        ///   Fills these settings into pProperties
        /// </summary>
        /// <param name = "pSection">The configuration section.</param>
        /// <param name = "pProperties">The p properties.</param>
        protected static void FillCollection(string pSection, IDictionary<string, object> pProperties)
        {
            try
            {
                object lObject = ConfigurationManager.GetSection(pSection);
                if (lObject != null)
                {
                    var lNameValueCollection = lObject as NameValueCollection;
                    if (lNameValueCollection != null)
                    {
                        foreach (string lKey in lNameValueCollection)
                        {
                            pProperties[lKey] = lNameValueCollection[lKey];
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(String.Format("error loading configsection {0}", pSection));
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
            }
        }
    }

    /// <summary>
    ///   Configuration Extensions, which helps to convert the App.Config/Web.Config's settings into strong Type instance.
    /// </summary>
    public static class ConfigurationExtensions
    {
        /// <summary>
        ///   Gets the setting and convert it to boolean type.
        /// </summary>
        /// <param name = "mProperties">The m properties.</param>
        /// <param name = "keyName">Name of the key.</param>
        /// <param name = "defaultValue">if set to <c>true</c> [default value].</param>
        /// <returns>The boolean value</returns>
        public static bool GetBoolenVal(this IDictionary<string, string> mProperties, string keyName,
                                        bool defaultValue = false)
        {
            var retval = defaultValue;
            string trueOrFalse = string.Empty;
            if (mProperties.ContainsKey(keyName))
                trueOrFalse = mProperties[keyName];

            if (!string.IsNullOrEmpty(trueOrFalse))
                retval = bool.Parse(trueOrFalse);

            return retval;
        }

        /// <summary>
        ///   Gets the setting and convert it to string type.
        /// </summary>
        /// <param name = "properties">The m properties.</param>
        /// <param name = "keyName">Name of the key.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <returns>The string value</returns>
        public static string GetStringVal(this IDictionary<string, string> properties, string keyName,
                                          string defaultValue = "")
        {
            return properties.ContainsKey(keyName) ? properties[keyName] : defaultValue;
        }

        /// <summary>
        ///   Gets the setting and convert it to Enum type.
        /// </summary>
        /// <typeparam name = "TEnum">The type of the enum.</typeparam>
        /// <param name = "properties">The m properties.</param>
        /// <param name = "keyName">Name of the key.</param>
        /// <param name = "defaultValue">The default value.</param>
        /// <returns>The Enum value</returns>
        public static TEnum GetEnumVal<TEnum>(this IDictionary<string, string> properties, string keyName,
                                              TEnum defaultValue = default(TEnum)) where TEnum : struct
        {
            var retval = defaultValue;
            string val = string.Empty;
            if (properties.ContainsKey(keyName))
                val = properties[keyName];

            if (!string.IsNullOrEmpty(val))
                retval = val.GetEnum<TEnum>();
            return retval;
        }

        /// <summary>
        /// Gets the int val.
        /// </summary>
        /// <param name="properties">The properties.</param>
        /// <param name="keyName">Name of the key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        public static int GetIntVal( this IDictionary<string, string> properties, string keyName, int defaultValue = 0)
        {
            int retval = 0;
            var val = "0";
            if (properties.ContainsKey(keyName))
                val = properties[keyName];

            if (!string.IsNullOrEmpty(val))
                int.TryParse(val, out retval);
            return retval;
        }
    }
}