using System;
using System.Security.Cryptography;
using System.Text;

namespace DdJob.Utilities.Cryptography
{
    /// <summary>
    /// Simple Cryptographic Services
    /// </summary>
    public class CryptographyUtils
    {
        /// <summary>
        /// Generates a cryptographic Hash Key for the provided text data.
        /// Basically a digital fingerprint
        /// </summary>
        /// <param name="dataToHash">text to hash</param>
        /// <param name="hashAlgorithm">e.g. new MD5CryptoServiceProvider();</param>
        /// <returns>Unique hash representing string</returns>
        public static string Encrypt(HashAlgorithm hashAlgorithm, String dataToHash)
        {
            String hexResult = "";
            var tabStringHex = new string[16];

            byte[] data = Encoding.ASCII.GetBytes(dataToHash);
            byte[] result = hashAlgorithm.ComputeHash(data);
            for (int i = 0; i < result.Length; i++)
            {
                tabStringHex[i] = (result[i]).ToString("x").PadLeft(2, '0'); 
                hexResult += tabStringHex[i];
            }
            return hexResult;
        }


        /// <summary>
        /// Generates a cryptographic Hash Key for the provided text data.
        /// Basically a digital fingerprint
        /// </summary>
        /// <param name="hashAlgorithm">e.g. new MD5CryptoServiceProvider();</param>
        /// <param name="hashedText">The hashed text.</param>
        /// <param name="unhashedText">The un hashed text.</param>
        /// <returns>Unique hash representing string</returns>
        public static bool IsHashMatch(HashAlgorithm hashAlgorithm, string hashedText, string unhashedText)
        {
            string hashedTextToCompare = Encrypt(hashAlgorithm, unhashedText);
            return (string.Compare(hashedText, hashedTextToCompare, false) == 0);
        }


        /// <summary>
        /// Encrypts text with Triple DES encryption using the supplied key
        /// </summary>
        /// <param name="algorithm">The algorithm.</param>
        /// <param name="plaintext">The text to encrypt</param>
        /// <param name="key">Key to use for encryption</param>
        /// <returns>The encrypted string represented as base 64 text</returns>
        public static string Encrypt(SymmetricAlgorithm algorithm, string plaintext, string key)
        {
            //TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
            var hashMD5 = new MD5CryptoServiceProvider();
            algorithm.Key = hashMD5.ComputeHash(Encoding.ASCII.GetBytes(key));
            algorithm.Mode = CipherMode.ECB;
            ICryptoTransform transformer = algorithm.CreateEncryptor();
            byte[] Buffer = Encoding.ASCII.GetBytes(plaintext);
            return Convert.ToBase64String(transformer.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }


        /// <summary>
        /// Decrypts supplied Triple DES encrypted text using the supplied key
        /// </summary>
        /// <param name="algorithm">The algorithm to use for decryption.</param>
        /// <param name="base64Text">Triple DES encrypted base64 text</param>
        /// <param name="key">Decryption Key</param>
        /// <returns>The decrypted string</returns>
        public static string Decrypt(SymmetricAlgorithm algorithm, string base64Text, string key)
        {
            var hashMD5 = new MD5CryptoServiceProvider();
            algorithm.Key = hashMD5.ComputeHash(Encoding.ASCII.GetBytes(key));
            algorithm.Mode = CipherMode.ECB;
            ICryptoTransform transformer = algorithm.CreateDecryptor();
            byte[] Buffer = Convert.FromBase64String(base64Text);
            return Encoding.ASCII.GetString(transformer.TransformFinalBlock(Buffer, 0, Buffer.Length));
        }


        /// <summary>
        /// Create new instance of symmetric algorithm using reflection by
        /// supplying the typename.
        /// </summary>
        /// <typeparam name="T">The type of fully qualified type name.</typeparam>
        /// <param name="fullyQualifiedTypeName">The fully qualified type name.</param>
        /// <returns>
        /// The fullyQualifiedTypeName instance
        /// </returns>
        public static T CreateAlgo<T>(string fullyQualifiedTypeName) where T : class
        {
            object algo = Activator.CreateInstance(Type.GetType(fullyQualifiedTypeName));
            return algo as T;
        }


        /// <summary>
        /// Create triple des symmetric algorithm.
        /// </summary>
        /// <returns>The TripleDESCryptoServiceProvider instance</returns>
        public static SymmetricAlgorithm CreateSymmAlgoTripleDes()
        {
            return new TripleDESCryptoServiceProvider();
        }


        /// <summary>
        /// Create MD5 hash algorithm.
        /// </summary>
        /// <returns>The MD5CryptoServiceProvider instance</returns>
        public static HashAlgorithm CreateHashAlgoMd5()
        {
            return new MD5CryptoServiceProvider();
        }
    }
}