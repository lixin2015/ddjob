﻿namespace DdJob.Utilities.Cryptography.DES
{
    /// <summary>
    /// Used to determine the length of a DES key.
    /// </summary>
    public enum DesKeyType
    {
        /// <summary>
        /// Single-length DES key (16 hexadecimal characters, 8 bytes).
        /// </summary>
        SingleLength,

        /// <summary>
        /// Double-length DES key (32 hexadecimal characters, 16 bytes).
        /// </summary>
        DoubleLength,

        /// <summary>
        /// Triple-length DES key (48 hexadecimal characters, 24 bytes).
        /// </summary>
        TripleLength
    };
}