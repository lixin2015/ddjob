﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace HFCoupon.Infrastructure.Cryptography
{
    public class PhoneTriple3Des
    {
        private static readonly char[] Constant = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private readonly SymmetricAlgorithm _mCsp = new TripleDESCryptoServiceProvider();

        public PhoneTriple3Des(string key = "")
        {
            if (string.IsNullOrEmpty(key)) key = Des3Key.PhoneKeyLong;
            _mCsp.Key = Convert.FromBase64String(key);
            _mCsp.IV = Convert.FromBase64String(Des3Key.Iv);
            _mCsp.Mode = CipherMode.ECB;
            _mCsp.Padding = PaddingMode.PKCS7;
        }

        #region 手机号加解密

        /// <summary>
        /// 手机号加密
        /// </summary>
        /// <param name="data">明文</param>
        /// <returns></returns>
        public string EncryptPhone(string data)
        {
            if (data == null) return "";
            //if (data.Length != 11) return data;

            var ct = _mCsp.CreateEncryptor(_mCsp.Key, _mCsp.IV);
            var byt = Encoding.UTF8.GetBytes(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Convert.ToBase64String(ms.ToArray());
        }

        /// <summary>
        /// 手机号密文解密
        /// </summary>
        /// <param name="data">密文</param>
        /// <returns></returns>
        public string DecryptPhone(string data)
        {
            if (data == null) return "";
            //if (data.Length == 11) return data;

            var ct = _mCsp.CreateDecryptor(_mCsp.Key, _mCsp.IV);
            var byt = Convert.FromBase64String(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Encoding.UTF8.GetString(ms.ToArray());
        }

        #endregion
    }
}
