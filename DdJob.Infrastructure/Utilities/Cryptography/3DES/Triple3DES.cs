﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using NPOI.SS.Formula.Functions;

namespace HFCoupon.Infrastructure.Cryptography
{
    public class Triple3Des
    {
        private static readonly char[] Constant = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private readonly SymmetricAlgorithm _mCsp = new TripleDESCryptoServiceProvider();
        public string Des3Encrypt(string data)
        {
            var key = Des3Key.Key;
            _mCsp.Key = Convert.FromBase64String(key);

            var ddd = new TripleDESCryptoServiceProvider();
            _mCsp.IV = ddd.IV;
            //指定加密的运算模式
            _mCsp.Mode = CipherMode.ECB;
            //获取或设置加密算法的填充模式
            _mCsp.Padding = PaddingMode.PKCS7;
            var ct = _mCsp.CreateEncryptor(_mCsp.Key, _mCsp.IV);
            var byt = Encoding.UTF8.GetBytes(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Convert.ToBase64String(ms.ToArray());
        }


        public static string Des3Decrypt(string data)
        {
            var key = data + Des3Key.Key;
            var mCsp = new TripleDESCryptoServiceProvider()
            {
                Key = Convert.FromBase64String(key),
                IV = Convert.FromBase64String(Des3Key.Iv),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            var ct = mCsp.CreateDecryptor(mCsp.Key, mCsp.IV);
            var byt = Convert.FromBase64String(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Encoding.UTF8.GetString(ms.ToArray());
        }

        public string Des3EncryptToVirtual(string data)
        {
            var key = data + Des3Key.VirtualKey;
            _mCsp.Key = Convert.FromBase64String(key);
            _mCsp.IV = Convert.FromBase64String(Des3Key.Iv);
            //指定加密的运算模式
            _mCsp.Mode = CipherMode.ECB;
            //获取或设置加密算法的填充模式
            _mCsp.Padding = PaddingMode.PKCS7;
            var ct = _mCsp.CreateEncryptor(_mCsp.Key, _mCsp.IV);
            var byt = Encoding.UTF8.GetBytes(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Convert.ToBase64String(ms.ToArray());
        }


        public static string Des3DecryptForVirtual(string data)
        {
            var mCsp = new TripleDESCryptoServiceProvider()
            {
                Key = Convert.FromBase64String(data + Des3Key.VirtualKey),
                IV = Convert.FromBase64String(Des3Key.Iv),
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };

            var ct = mCsp.CreateDecryptor(mCsp.Key, mCsp.IV);
            var byt = Convert.FromBase64String(data);
            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(byt, 0, byt.Length);
            cs.FlushFinalBlock();
            cs.Close();
            return Encoding.UTF8.GetString(ms.ToArray());
        }

        public static string GenerateRandomNumber(int length = 6)
        {
            var newRandom = new StringBuilder(10);
            var rd = new Random();
            for (var i = 0; i < length; i++)
            {
                newRandom.Append(Constant[rd.Next(10)]);
            }
            return newRandom.ToString();
        }

        //#region 手机号加解密

        //public string EncryptPhone(string data)
        //{
        //    if (data.Length != 11)
        //        return "";

        //    var key = data + Des3Key.PhoneKey;
        //    _mCsp.Key = Convert.FromBase64String(key);
        //    _mCsp.IV = Convert.FromBase64String(Des3Key.Iv);
        //    _mCsp.Mode = CipherMode.ECB;
        //    _mCsp.Padding = PaddingMode.PKCS7;
        //    var ct = _mCsp.CreateEncryptor(_mCsp.Key, _mCsp.IV);
        //    var byt = Encoding.UTF8.GetBytes(data);
        //    var ms = new MemoryStream();
        //    var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
        //    cs.Write(byt, 0, byt.Length);
        //    cs.FlushFinalBlock();
        //    cs.Close();
        //    return Convert.ToBase64String(ms.ToArray());
        //}

        //public static string DecryptPhone(string data)
        //{
        //    if (data.Length != 11)
        //        return "";

        //    var des = new TripleDESCryptoServiceProvider
        //    {
        //        Key = Encoding.ASCII.GetBytes(data + Des3Key.Key),
        //        Mode = CipherMode.CBC,
        //        Padding = PaddingMode.PKCS7
        //    };
        //    var desDecrypt = des.CreateDecryptor();

        //    var buffer = Convert.FromBase64String(data);
        //    return Encoding.ASCII.GetString(desDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
        //}

        //#endregion
    }
}
