using System.Security.Cryptography;
using System.Text;

namespace DdJob.Utilities.Cryptography
{
    ///<summary>
    ///  Cryptography service to encrypt and decrypt strings.
    ///  <example>
    ///    1. Encrypt using default provider. ( Symmetric TripleDes )
    ///    <code>
    ///      string plainText = "www.XXX.com";
    ///      string encrypted = Crypto.Encrypt(plainText);
    ///      string decrypted = Crypto.Decrypt(encrypted);
    ///
    ///      Console.WriteLine("====================================================");
    ///      Console.WriteLine("CRYPTOGRAPHY ");
    ///      Console.WriteLine("Encrypted : " + plainText + " to " + encrypted);
    ///      Console.WriteLine("Decrypted : " + encrypted + " to " + decrypted);
    ///      Console.WriteLine(Environment.NewLine);
    ///    </code>
    ///    2. Use non-static encryption provider.
    ///    <code>
    ///      ICrypto crypto = new CryptoHash("www.XXX.dk", new MD5CryptoServiceProvider());
    ///      string hashed = crypto.Encrypt("my baby - 2002 honda accord ex coupe");
    ///      Console.WriteLine(hashed);
    ///    </code>
    ///    3. Change the crypto provider on the static helper.
    ///    <code>
    ///      ICrypto crypto2 = new CryptoSym("new key", new TripleDESCryptoServiceProvider());
    ///      Crypto.Init(crypto2);
    ///      string encryptedWithNewKey = Crypto.Encrypt("www.XXX.dk");
    ///      Console.WriteLine(string.Format("Encrypted text : using old key - {0}, using new key - {1}", encrypted, encryptedWithNewKey));
    ///    </code>
    ///    4. Generate the check value of a 3DES key by encrypting 16 hexadecimal zeroes.
    ///    <code>
    ///      DESKey randomKey = new DESKey(DesKeyType.TripleLength);
    ///      string keyCheckValue = ComLib.Cryptography.DES.TripleDES.Encrypt(randomKey, "0000000000000000");
    ///      Console.WriteLine(string.Format("3DES key: {0} with check value {1}", randomKey.ToString(), keyCheckValue));
    ///    </code>
    ///  </example>
    ///</summary>
    public class Crypto
    {
        private static ICrypto _provider;


        /// <summary>
        ///   Create default instance of symmetric cryptographer.
        /// </summary>
        static Crypto()
        {
            _provider = new CryptoSym();
        }


        /// <summary>
        ///   Get reference to current encryption provider.
        /// </summary>
        public static ICrypto Provider
        {
            get { return _provider; }
        }

        /// <summary>
        ///   Initialize to new provider.
        /// </summary>
        /// <param name = "service">The encryption provider</param>
        public static void Init(ICrypto service)
        {
            _provider = service;
        }


        /// <summary>
        ///   Encrypts the plain text using an internal private key.
        /// </summary>
        /// <param name = "plaintext">The text to encrypt.</param>
        /// <returns>An encrypted string in base64 format.</returns>
        public static string Encrypt(string plaintext)
        {
            return _provider.Encrypt(plaintext);
        }


        /// <summary>
        ///   Decrypts the base64key using an internal private key.
        /// </summary>
        /// <param name = "base64Text">The encrypted string in base64 format.</param>
        /// <returns>The plain text string.</returns>
        public static string Decrypt(string base64Text)
        {
            return _provider.Decrypt(base64Text);
        }


        /// <summary>
        ///   Determine if the plain text and encrypted are ultimately the same.
        /// </summary>
        /// <param name = "encrypted">encrypted string</param>
        /// <param name = "plainText">plainText</param>
        /// <returns><c>true</c> if the plain text is match the encrypted string</returns>
        public static bool IsMatch(string encrypted, string plainText)
        {
            return _provider.IsMatch(encrypted, plainText);
        }


        /// <summary>
        ///   Calculate the md5 hash of the input text.
        /// </summary>
        /// <param name = "input">The input string</param>
        /// <returns>The MD5 code</returns>
        public static string ToMD5Hash(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // Now convert to hex.
            var sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}