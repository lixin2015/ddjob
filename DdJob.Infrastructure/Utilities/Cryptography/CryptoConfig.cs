namespace DdJob.Utilities.Cryptography
{
    /// <summary>
    /// Settings for the encryption.
    /// </summary>
    public class CryptoConfig
    {
        private readonly bool _encrypt = true;
        private readonly string _internalKey = "keyphrase";


        /// <summary>
        /// encryption option
        /// </summary>
        public CryptoConfig()
        {
        }


        /// <summary>
        /// encryption options
        /// </summary>
        /// <param name="encrypt">Whether or not encrypt</param>
        /// <param name="key">The encryption key.</param>
        public CryptoConfig(bool encrypt, string key)
        {
            _encrypt = encrypt;
            _internalKey = key;
        }


        /// <summary>
        /// Whether or not to encrypt;
        /// Primarily used for unit testing.
        /// Default is to encrypt.
        /// </summary>
        public bool Encrypt
        {
            get { return _encrypt; }
        }


        /// <summary>
        /// Key used to encrypt a word.
        /// </summary>
        public string InternalKey
        {
            get { return _internalKey; }
        }
    }
}