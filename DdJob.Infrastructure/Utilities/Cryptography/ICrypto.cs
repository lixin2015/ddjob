namespace DdJob.Utilities.Cryptography
{
    /// <summary>
    /// Cryptography interface to encrypt and decrypt strings.
    /// </summary>
    public interface ICrypto
    {
        /// <summary>
        /// Options for encryption.
        /// </summary>
        CryptoConfig Settings { get; }


        /// <summary>
        /// Encrypts a string.
        /// </summary>
        /// <param name="plaintext">The plain text.</param>
        /// <returns>The encrypted string</returns>
        string Encrypt(string plaintext);


        /// <summary>
        /// Decrypt the encrypted text.
        /// </summary>
        /// <param name="base64Text">The encrypted base64 text</param>
        /// <returns>The decrypt string </returns>
        string Decrypt(string base64Text);


        /// <summary>
        /// Determine if encrypted text can be matched to unencrypted text.
        /// </summary>
        /// <param name="encrypted">The encrypted text.</param>
        /// <param name="plainText">The plain text.</param>
        /// <returns><c>true</c> if the plain text is match the encrypted string</returns>
        bool IsMatch(string encrypted, string plainText);
    }
}