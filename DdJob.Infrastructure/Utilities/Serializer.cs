﻿using System;
using System.IO;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace DdJob.Utilities
{
    public static class Serializer
    {
        public static string ToJson(object entity)
        {
            return JsonConvert.SerializeObject(entity);
        }
        public static string ToJsonIgnoreLoopHandling(object entity)
        {
            return JsonConvert.SerializeObject(entity, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }      
        public static T FromJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static object FromJson(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }

        public static string ToXml<T>(T obj)
        {
            var serializer = new XmlSerializer(typeof (T));
            using (var stream = new MemoryStream())
            {
                serializer.Serialize(stream, obj);
                stream.Position = 0;
                var reader = new StreamReader(stream);
                var xml = reader.ReadToEnd();
                reader.Close();
                return xml;
            }
        }

        public static T FromXml<T>(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new MemoryStream())
            {
                var writer = new StreamWriter(stream);
                writer.Write(xml);
                writer.Flush();
                stream.Position = 0;
                T instance = (T)serializer.Deserialize(stream);
                writer.Close();
                return instance;
            }
        }

    }
}
