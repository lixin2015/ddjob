﻿using System;
using System.Collections.Generic;

namespace DdJob
{
    public class WxUserDto
    {
        public string OpenId { get; set; }
        public string NickName { get; set; }
        public string Sex { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string HeadimgUrl { get; set; }
        public List<string> Privilege { get; set; }
        public string UnionId { get; set; }
    }

    public class OAuthCode
    {
        public string Code { get; set; }

        public DateTime ExpiresDate { get; set; }

        public void SetBaseInfo(string code = "")
        {
            if (!string.IsNullOrEmpty(code))
            {
                Code = code;
            }
            ExpiresDate = DateTime.Now.AddMinutes(5);
        }
    }

    public class AccessToken
    {
        public string Token { get; set; }

        public DateTime ExpiresDate { get; set; }

        public string RefreshToken { get; set; }

        public void SetBaseInfo(string token = "", string refreshToken = "", int expire = 7200)
        {
            if (!string.IsNullOrEmpty(token))
            {
                Token = token;
            }
            if (!string.IsNullOrEmpty(refreshToken))
            {
                RefreshToken = RefreshToken;
            }
            ExpiresDate = DateTime.Now.AddSeconds(expire);
        }
    }

    public class JsTicket
    {
        public string Ticket { get; set; }

        public DateTime ExpiresDate { get; set; }

        public void SetBaseInfo(string ticket = "", int expire = 7200)
        {
            if (!string.IsNullOrEmpty(ticket))
            {
                Ticket = ticket;
            }
            ExpiresDate = DateTime.Now.AddSeconds(expire - 300);
        }
    }


    public class WeChatUserDto
    {
        public WxUserDto User = new WxUserDto();
        public OAuthCode OAuthCode = new OAuthCode();
        public AccessToken AccessToken = new AccessToken();
        public string OpenId { get; set; }
        public string UnionId { get; set; }
        public string Scope { get; set; }
        public string State { get; set; }
    }

    [Serializable]
    public class ThirdUserDto
    {
        public string OpenId { get; set; }
        public JsSdkConfig ConfigSignature { get; set; }
    }

    public class OAuthResponse
    {
        public string Code { get; set; }
        public string State { get; set; }

        public string access_token { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string openid { get; set; }
        public string scope { get; set; }
        public string unionid { get; set; }

        public WeChatUserDto SetAuthCode(WeChatUserDto user)
        {
            if (!string.IsNullOrEmpty(Code))
            {
                user.OAuthCode.SetBaseInfo(Code);
            }
            user.State = State;
            return user;
        }

        public WeChatUserDto SetAccessTokenOrRefreshToken(WeChatUserDto user)
        {
            user.AccessToken.SetBaseInfo(access_token, refresh_token, expires_in);
            user.OpenId = openid;
            user.Scope = scope;
            return user;
        }
    }

    public class TicketResponse
    {
        public string ticket { get; set; }
        public int expires_in { get; set; }
        public string errmsg { get; set; }
        public string errcode { get; set; }
    }

    public class TemplateResponse
    {
        public string template_id { get; set; }
        public string errmsg { get; set; }
        public string errcode { get; set; }
    }

    public class SendMsgResponse
    {
        public string msgid { get; set; }
        public string errmsg { get; set; }
        public string errcode { get; set; }
    }

    [Serializable]
    public class JsSdkConfig
    {
        public string Url { get; set; }
        public string AppId { get; set; }
        public string Timestamp { get; set; }
        public string NonceStr { get; set; }
        public string Signature { get; set; }
        public string JsApi { get; set; }

        public JsSdkConfig()
        {
            JsApi = "scanQRCode,startRecord,stopRecord,openLocation,getLocation,closeWindow";
        }
    }

    public class QRcodeResponse : ErrorMsg
    {
        public string ticket { get; set; }
        public string expire_seconds { get; set; }
        public string url { get; set; }
    }

    public class ErrorMsg
    {
        public string errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class BindingResponseDto
    {
        public string OpenId { get; set; }
        public string ReturnUrl { get; set; }
    }
}