﻿namespace DdJob
{
    public class WeChatUrls
    {
        public static string Host { get; set; }

        public static string RequestCodeUrl = "https://open.weixin.qq.com/connect/oauth2/authorize";

        public static string RequestTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token";

        public static string RequestRefreshTokenUrl = "https://api.weixin.qq.com/sns/oauth2/refresh_token";

        public static string RequestUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo";

        public static string RedirectCodeUrl { get { return Host + "/client/home/index"; } }

        public static string GetAccessTokenUrl = "https://api.weixin.qq.com/cgi-bin/token";

        public static string GetTicketUrl = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";

        public static string GetRcodeTicketUrl = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token={0}";

        public static string GetMsgTemplateUrl = "https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token={0}";

        public static string GetSendMsgUrl = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={0}";

        public static string GetQrCodeUrl = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={0}";
    }
}