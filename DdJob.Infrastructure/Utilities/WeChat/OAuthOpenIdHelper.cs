﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace DdJob
{
    public class OAuthOpenIdHelper
    {
        public static string GetHttp(string url, Dictionary<string, string> contexts, string urlEnd = "")
        {
            var queryString = contexts.Aggregate("?", (current, context) => current + (context.Key + "=" + context.Value + "&"));
            queryString = queryString.Substring(0, queryString.Length - 1);

            var newUrl = url + queryString + urlEnd;
            if (string.IsNullOrEmpty(newUrl)) return "";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(newUrl);

            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "GET";
            httpWebRequest.Timeout = 20000;

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            var responseContent = streamReader.ReadToEnd();

            httpWebResponse.Close();
            streamReader.Close();

            return responseContent;
        }

        public static string PostHttp(string url, string data, string urlEnd = "")
        {
            var newUrl = url + urlEnd;
            if (string.IsNullOrEmpty(newUrl)) return "";

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(newUrl);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            httpWebRequest.Timeout = 20000;

            var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            var streamReader = new StreamReader(httpWebResponse.GetResponseStream());
            var responseContent = streamReader.ReadToEnd();

            httpWebResponse.Close();
            streamReader.Close();

            return responseContent;
        }

        public static T PostResponse<T>(string url, string postData) where T : class,new()
        {
            if (url.StartsWith("https")) ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;

            var httpContent = new StringContent(postData);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var httpClient = new HttpClient();

            var response = httpClient.PostAsync(url, httpContent).Result;

            if (!response.IsSuccessStatusCode) return null;
            var t = response.Content.ReadAsStringAsync();
            var s = t.Result;

            var result = JsonConvert.DeserializeObject<T>(s);
            return result;
        }
    }
}