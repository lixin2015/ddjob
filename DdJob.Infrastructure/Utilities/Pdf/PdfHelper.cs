﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Web;
using DotLiquid;
using iTextSharp.text;
using NPOI.SS.UserModel;
using Winnovative.PdfCreator;
using Document = Winnovative.PdfCreator.Document;
using PageSize = Winnovative.PdfCreator.PageSize;

namespace ZZB.Pdf
{
    public class PdfHelper
    {
        public static void FillTemplate(string path, Stream outputStream, Dictionary<string, string> fields)
        {
            var pdfReader = new iTextSharp.text.pdf.PdfReader(path);
            var pdfStamper = new iTextSharp.text.pdf.PdfStamper(pdfReader, outputStream);
            var pdfFormFields = pdfStamper.AcroFields;

            foreach (var field in fields)
            {
                pdfFormFields.SetField(field.Key, field.Value);
            }

            pdfStamper.FormFlattening = true;
            pdfStamper.Close();
        }

        public static void WriteHtmlToPdf(string html, Stream outputStream)
        {
            LicensingManager.LicenseKey = "C1PA8pHWEdZAoqVImzQN3TSaT5TyF/HJz9LKAzUTDZu1mqsxk/p5t0nZL9SIhe8W";
            var document = new Document();

            document.CompressionLevel = CompressionLevel.NormalCompression;
            document.Margins = new Margins();
            document.Security.CanPrint = true;
            document.Security.UserPassword = "";

            document.DocumentInformation.Author = "";
            document.ViewerPreferences.HideToolbar = false;

            var page = document.Pages.AddNewPage(PageSize.A4, new Margins(25, 25, 25, 25), PageOrientation.Portrait);

            var smallfont = document.Fonts.Add(new Font(new FontFamily("Verdana"), 9, GraphicsUnit.Point));

            document.FooterTemplate = document.AddTemplate(document.Pages[0].ClientRectangle.Width, 60);

            document.FooterTemplate.AddElement(new TextElement(document.FooterTemplate.ClientRectangle.Width - 75, 40,
                 "Page &p; of &P;", smallfont));

            var htmlDoc = new HtmlToPdfElement(0, 0, -1, html, string.Empty, 700);

            htmlDoc.FitWidth = true;
            htmlDoc.EmbedFonts = true;
            htmlDoc.LiveUrlsEnabled = false;

            htmlDoc.ScriptsEnabled = false;
            htmlDoc.ActiveXEnabled = false;

            // add the HTML to PDF converter element to the page

            page.AddElement(htmlDoc);
            document.Save(outputStream);
        }

        public static void WriteHtmlToPdf(Uri uri, Stream outputStream)
        {
            var html = DownloadHtml(uri);
            WriteHtmlToPdf(html, outputStream);
        }

        private static string DownloadHtml(Uri uri)
        {
            var request = WebRequest.Create(uri) as HttpWebRequest;
            if (HttpContext.Current == null)
            {
                throw new Exception("HttpContext.Current is null");
            }
            var currentRequest = HttpContext.Current.Request;
            if (uri.Host.Equals(currentRequest.Url.Host, StringComparison.OrdinalIgnoreCase))
            {
                request.CookieContainer = new CookieContainer();
                foreach (string key in currentRequest.Cookies)
                {
                    var cookie = currentRequest.Cookies[key];
                    if (cookie == null)
                    {
                        continue;
                    }
                    var netCookie = new Cookie(cookie.Name, cookie.Value, cookie.Path, currentRequest.Url.Host);
                    request.CookieContainer.Add(netCookie);
                }
            }

            var response = request.GetResponse();
            var responseStream = response.GetResponseStream();
            if (responseStream == null)
            {
                throw new Exception("Error occurred while requesting the URL: " + uri);
            }
            var reader = new StreamReader(responseStream);
            var html = reader.ReadToEnd();
            reader.Dispose();
            responseStream.Dispose();

            return html;
        }
    }
}
