﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using ICSharpCode.SharpZipLib.Zip;
using NPOI.XSSF.UserModel;
using DdJob.Extensions;

namespace DdJob.Utilities
{
    public class DirectoryHelper
    {
        public static void CreateExcel(string fileName, string[] sheetNames)
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
            var workbook = new XSSFWorkbook();
            if (sheetNames != null)
            {
                foreach (var sheetName in sheetNames)
                {
                    workbook.CreateSheet(sheetName);
                }
            }
            else
            {
                workbook.CreateSheet();
            }
            var fs = File.Create(fileName);
            workbook.Write(fs);
            fs.Close();
        }

        public static string CleanAndCreateTempDirectory(string baseTempDirectory)
        {
            var today = DateTime.Now;
            var tempDirectory = baseTempDirectory + (baseTempDirectory.EndsWith("\\") ? "" : "\\") +
                                today.ToString("yyyyMMddHHmmss");
            if (Directory.Exists(baseTempDirectory))
            {
                var directories = Directory.GetDirectories(baseTempDirectory);
                if (directories.Any())
                {
                    var compareDate = today.AddDays(-1);
                    directories.ForEach(dir =>
                    {
                        var directoryInfo = new DirectoryInfo(dir);
                        if (directoryInfo.CreationTime <= compareDate)
                        {
                            directoryInfo.Delete(true);
                        }
                    });
                }
            }
            if (!Directory.Exists(tempDirectory))
            {
                Directory.CreateDirectory(tempDirectory);
            }
            return tempDirectory;
        }

        public static string CreateZip(string directory, string zipFileName, string[] fileNames)
        {
            var zipFilePath = directory + "\\" + zipFileName;
            using (var zip = ZipFile.Create(zipFilePath))
            {
                if (fileNames != null)
                {
                    var currentDirectory = Directory.GetCurrentDirectory();
                    Directory.SetCurrentDirectory(directory);
                    zip.BeginUpdate();
                    foreach (var fileName in fileNames)
                    {
                        zip.Add(fileName);
                    }
                    zip.CommitUpdate();
                    Directory.SetCurrentDirectory(currentDirectory);
                }
            }
            return zipFilePath;
        }
    }
}
