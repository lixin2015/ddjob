﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DdJob.Utilities
{
    /// <summary>
    ///   Provides helper methods for Files operations.
    /// </summary>
    public class FileHelper
    {
        /// <summary>
        /// Moves files by search pattern
        /// </summary>
        /// <param name="pSourcePath">The source path.</param>
        /// <param name="pSearchPattern">The search pattern.</param>
        /// <param name="pTargetPath">The target path.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static bool FilesMove(string pSourcePath, string pSearchPattern, string pTargetPath)
        {
            // search files
            string[] lFiles = GetFilesFromLocal(pSourcePath, pSearchPattern, SearchOption.TopDirectoryOnly);
            // move files
            return FilesMove(pSourcePath, lFiles, pTargetPath);
        }

        /// <summary>
        /// Move files by files' name
        /// </summary>
        /// <param name="pSourcePath">The source path.</param>
        /// <param name="pFileNames">The file names.</param>
        /// <param name="pTargetPath">The target path.</param>
        /// <returns>
        /// If the operations is success return true,otherwise return false
        /// </returns>
        public static bool FilesMove(string pSourcePath, string[] pFileNames, string pTargetPath)
        {
            bool lIsAllFilesMove = true;
            try
            {
                if (pFileNames != null && pFileNames.Length > 0)
                {
                    foreach (string t in pFileNames)
                    {
                        if (!FileMove(pSourcePath, t, pTargetPath))
                        {
                            lIsAllFilesMove = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return lIsAllFilesMove;
        }

        /// <summary>
        /// move a single file by file name
        /// </summary>
        /// <param name="pSourcePath">The source path.</param>
        /// <param name="pFileName">Name of the p file.</param>
        /// <param name="pTargetPath">The target path.</param>
        /// <returns>
        /// If the operations is success return true,otherwise return false
        /// </returns>
        public static bool FileMove(string pSourcePath, string pFileName, string pTargetPath)
        {
            try
            {
                if (!Directory.Exists(pTargetPath))
                {
                    Directory.CreateDirectory(pTargetPath);
                }
                if (File.Exists(pSourcePath + "\\" + pFileName))
                {
                    FileCopy(pSourcePath, pFileName, pTargetPath);
                    File.Delete(pSourcePath + "\\" + pFileName);

                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// copy files by search pattern
        /// </summary>
        /// <param name="pSourcePath">The source path.</param>
        /// <param name="pSearchPattern">The search pattern.</param>
        /// <param name="pTargetPath">The target path.</param>
        /// <returns>
        /// If the operations is success return true,otherwise return false
        /// </returns>
        public static bool FilesCopy(string pSourcePath, string pSearchPattern, string pTargetPath)
        {
            // search files
            string[] lFiles = GetFilesFromLocal(pSourcePath, pSearchPattern, SearchOption.TopDirectoryOnly);
            // copy files
            return FilesCopy(pSourcePath, lFiles, pTargetPath);
        }

        /// <summary>
        /// Copies these files.
        /// </summary>
        /// <param name="pSourcePath">The source path.</param>
        /// <param name="pFilesName">Name of the p files.</param>
        /// <param name="pTargetPath">The target path.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static bool FilesCopy(string pSourcePath, string[] pFilesName, string pTargetPath)
        {
            bool lIsAllFilesCopy = true;
            try
            {
                if (pFilesName != null && pFilesName.Length > 0)
                {
                    for (int i = 0; i < pFilesName.Length; i++)
                    {
                        if (!FileCopy(pSourcePath, Path.GetFileName(pFilesName[i]), pTargetPath))
                        {
                            lIsAllFilesCopy = false;
                        }
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }

            return lIsAllFilesCopy;
        }

        /// <summary>
        /// copy a single file by file name
        /// </summary>
        /// <param name="pSourcePath">The source path.</param>
        /// <param name="pFileName">Name of the p file.</param>
        /// <param name="pTargetPath">The target path.</param>
        /// <returns>
        /// If the operations is success return true,otherwise return false
        /// </returns>
        public static bool FileCopy(string pSourcePath, string pFileName, string pTargetPath)
        {
            try
            {
                if (!Directory.Exists(pTargetPath))
                {
                    Directory.CreateDirectory(pTargetPath);
                }

                if (File.Exists(pSourcePath + "\\" + pFileName))
                {
                    File.Copy(pSourcePath + "\\" + pFileName, pTargetPath + "\\" + pFileName, true);

                    // set the file's attribute as normal
                    File.SetAttributes(pTargetPath + "\\" + pFileName, FileAttributes.Normal);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        ///   Gets the files from local.
        /// </summary>
        /// <param name = "pSourcePath">The source path.</param>
        /// <param name = "pSearchPattern">The search pattern.</param>
        /// <param name = "pSearchOption">The p search option.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static string[] GetFilesFromLocal(string pSourcePath, string pSearchPattern, SearchOption pSearchOption)
        {
            string[] lFiles = null;
            try
            {
                if (Directory.Exists(pSourcePath))
                {
                    lFiles = Directory.GetFiles(pSourcePath, pSearchPattern, pSearchOption);
                }
                return lFiles;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///   Gets the directories.
        /// </summary>
        /// <param name = "pSourcePath">The source path.</param>
        /// <param name = "pSearchPattern">The search pattern.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static List<string> GetDirectories(string pSourcePath, string pSearchPattern)
        {
            var lLsitDirectories = new List<string>();
            try
            {
                if (Directory.Exists(pSourcePath))
                {
                    string[] lFiles = Directory.GetFiles(pSourcePath, pSearchPattern, SearchOption.AllDirectories);
                    foreach (string t in lFiles)
                    {
                        string lFilePath = Path.GetDirectoryName(t);
                        if (lLsitDirectories.IndexOf(lFilePath) < 0)
                        {
                            lLsitDirectories.Add(lFilePath);
                        }
                    }
                }
                return lLsitDirectories;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///   Gets the binary value of file.
        /// </summary>
        /// <param name = "pFileFullName">The path of  file.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static Byte[] GetBinaryValueOfFile(string pFileFullName)
        {
            try
            {
                if (File.Exists(pFileFullName))
                {
                    Byte[] lFileBytes = File.ReadAllBytes(pFileFullName);
                    return lFileBytes;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        ///   Delete the specific file.
        /// </summary>
        /// <param name = "pFileFullName">The path of  file.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static bool Fieldelete(string pFileFullName)
        {
            try
            {
                if (File.Exists(pFileFullName))
                {
                    File.Delete(pFileFullName);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        ///   Deletes files.
        /// </summary>
        /// <param name = "pFilesName">Name of the p files.</param>
        /// <param name = "pSourcePath">The source path.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static bool FilesDelete(string[] pFilesName, string pSourcePath)
        {
            bool lDeleteAll = true;
            if (pFilesName != null && pFilesName.Length > 0)
            {
                foreach (string t in pFilesName)
                {
                    if (!Fieldelete(pSourcePath + "\\" + t))
                    {
                        lDeleteAll = false;
                    }
                }
            }
            return lDeleteAll;
        }

        /// <summary>
        ///   Delete files 
        /// </summary>
        /// <param name = "pFilesFullName">Full name of the p files.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static bool FilesDelete(string[] pFilesFullName)
        {
            bool lDeleteAll = true;
            if (pFilesFullName != null && pFilesFullName.Length > 0)
            {
                foreach (string t in pFilesFullName)
                {
                    if (!Fieldelete(t))
                    {
                        lDeleteAll = false;
                    }
                }
            }
            return lDeleteAll;
        }

        /// <summary>
        ///   Delete files according the search patterns
        /// </summary>
        /// <param name = "pTempPath">The p temp path.</param>
        /// <param name = "pSearchPattern">The search pattern.</param>
        /// <returns>If the operations is success return true,otherwise return false</returns>
        public static bool FilesDelete(string pTempPath, string pSearchPattern)
        {
            return FilesDelete(Directory.GetFiles(pTempPath, pSearchPattern, SearchOption.TopDirectoryOnly));
        }

        /// <summary>
        ///   Writes the string content into one file.
        /// </summary>
        /// <param name = "path">The file path.</param>
        /// <param name = "content">The content.</param>
        public static void WriteFile(string path, string content)
        {
            Byte[] data = Encoding.UTF8.GetBytes(content);
            WriteFile(path, data);
        }

        public static void WriteFile(string path, byte[] content)
        {
            using (var fs = new FileStream(path, FileMode.Create))
            {
                fs.Write(content, 0, content.Length);
                fs.Flush();
            }
        }

        /// <summary>
        ///   Reads the file and return the content
        /// </summary>
        /// <param name = "file">The file path</param>
        /// <returns>The file's content string</returns>
        public static string ReadFile(string file)
        {
            string retval;
            using (var fs = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                var sr = new StreamReader(fs);
                sr.BaseStream.Seek(0, SeekOrigin.Begin);
                retval = sr.ReadToEnd();
            }
            return retval;
        }

        public static string GenerateRandomFileName(string fileName)
        {
            var timestamp = DateTime.Now.ToString("yyyyMMdd_HHmmss");
            var randomNum = Guid.NewGuid().GetHashCode();
            var prefix = Path.GetFileNameWithoutExtension(fileName);
            var suffix = Path.GetExtension(fileName);
            return string.Format("{0}_{1}_{2}{3}", prefix, timestamp, randomNum, suffix);
        }
    }
}