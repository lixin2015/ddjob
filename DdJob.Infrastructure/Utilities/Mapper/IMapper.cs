﻿namespace DdJob.Utilities.Mapper
{
    public interface IMapper<in TSource, out TDestination>
    {
        TDestination Map(TSource source);
    }
}