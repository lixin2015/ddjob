﻿using DdJob.Utilities.Mapper;

namespace DdJob.Utilities.Mapper
{
    public class ObjectMapper<TSource, TDestination> : IMapper<TSource, TDestination>
    {
        #region IMapper<TSource,TDestination> Members

        public TDestination Map(TSource source)
        {
            return AutoMapper.Mapper.Map<TDestination>(source);
        }

        #endregion
    }
}