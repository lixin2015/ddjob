﻿using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;

namespace DdJob.Utilities.Excel
{
    public interface IExcelExporter
    {
        IWorkbook Workbook { get; set; }

        ExcelExporter Export<T>(ExcelSheetDto sheetDto, List<T> items, bool createSerialNumber,
            Action<ExcelColumnFactory<T>> columns, List<ExcelGroupHeader> list = null);

        void Write(Stream stream);
    }
}
