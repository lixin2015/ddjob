﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ZZB;
using NPOI.SS.UserModel;

namespace ZZB.Excel
{
    public abstract class ExcelImporter<T> where T : class
    {
        protected abstract int ColumnCount { get; }

        protected abstract string TableName { get; }

        private Dictionary<string, string> _parttimeUserDic;

        public Dictionary<string, string> ParttimeUserDic
        {
            get { return _parttimeUserDic; }
        }

        //protected IList<T> UpdateData = new List<T>();

        protected IList<T> InsertData = new List<T>();

        protected readonly EFRepository<T> _repository;

        private bool _hasError;

        public bool HasError
        {
            get { return _hasError; }
        }

        protected string SharedUserFilePath
        {
            get { return Environment.CurrentDirectory + @"\Import\Data\员工信息\SharedUsers.xlsx"; }
        }

        protected ExcelImporter()
        {
            _parttimeUserDic = new Dictionary<string, string>();
            _repository = IoC.GetEFRepository<T>();
        }

        public virtual void Import(ISheet sheet, ICellStyle style)
        {
            var rowNum = sheet.PhysicalNumberOfRows;
            for (var i = 1; i < rowNum; i++)
            {
                var row = sheet.GetRow(i);
                if (row.PhysicalNumberOfCells != this.ColumnCount && row.LastCellNum != this.ColumnCount)
                {
                    row.MarkErrorWithCellStyle(this.ColumnCount, style);
                    _hasError = true;
                    continue;
                }
                var ignoreThisRow = false;
                T entity = null;

                try
                {
                    entity = GetFromRow(row, style, out ignoreThisRow);
                }
                catch (Exception)
                {
                    row.MarkErrorWithCellStyle(this.ColumnCount, style);
                    _hasError = true;
                    Console.WriteLine(DateTime.Now + " 发生异常。行号：" + i);
                    continue;
                }
                if (!ignoreThisRow)
                {
                    if (entity != null)
                    {
                        //if (!entity.IsNew)//IsExisted(entity))
                        //{
                        //    entity.UpdatedDate = DateTime.Now;
                        //    entity.UpdatedBy = null;
                        //    UpdateData.Add(entity);
                        //}
                        //else
                        //{
                        //entity.CreatedDate = DateTime.Now;
                        InsertData.Add(entity);
                        //}
                    }
                    else
                    {
                        _hasError = true;
                    }
                }
            }

            if (InsertData.Count > 0 && !_hasError)
            {
                BeforeImport();
                var dt = InsertData.ToDataTable();
            
                BulkCopy(dt, TableName);
                AfterImport();
                InsertData = new List<T>();
            }

            //Console.WriteLine(DateTime.Now + " start to bulk update data.");
            //UpdateToDb();
            //Console.WriteLine(DateTime.Now + " bulk update data finished.");
        }

        protected void BulkCopy(DataTable dt, string tableName)
        {
            Console.WriteLine(DateTime.Now + " 开始批量插入数据. 表名：" + tableName);
            var connStr = ConfigurationManager.ConnectionStrings["ZZBDataContext"].ConnectionString;
            using (var bulkCopy = new SqlBulkCopy(connStr) {DestinationTableName = tableName, NotifyAfter = 10000})
            {
                bulkCopy.BulkCopyTimeout = 600;
                bulkCopy.SqlRowsCopied += bulkCopy_SqlRowsCopied;
                foreach (DataColumn dc in dt.Columns)
                {
                    bulkCopy.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                }
                bulkCopy.WriteToServer(dt);
            }
            Console.WriteLine(DateTime.Now + " 批量插入数据成功. 表名：" + tableName);
            //var bulkCopy = new SqlBulkCopy(connStr) { DestinationTableName = tableName, NotifyAfter = 10000 };

        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(e.RowsCopied + " record inserted into " + TableName);
            Log.Info(e.RowsCopied + " record inserted into " + TableName);
        }

        protected abstract T GetFromRow(IRow row, ICellStyle style, out bool ignoreThisRow);

        protected void GetSharedUsers()
        {
            if (File.Exists(SharedUserFilePath))
            {
                var excel = WorkbookFactory.Create(SharedUserFilePath);
                var sheet = excel.GetSheetAt(0);
                var rowNum = sheet.PhysicalNumberOfRows;
                for (var i = 0; i < rowNum; i++)
                {
                    var row = sheet.GetRow(i);
                    _parttimeUserDic.Add(row.GetString(0), row.GetString(1));
                }
            }
        }

        protected virtual void BeforeImport()
        {
        }

        protected virtual void AfterImport()
        {
        }

        //protected abstract bool IsExisted(T entity);

        //protected abstract void UpdateToDb();

    }
}