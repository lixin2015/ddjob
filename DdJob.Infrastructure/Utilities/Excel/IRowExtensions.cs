﻿using System;
using System.Collections.Generic;
using System.Text;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace DdJob.Utilities.Excel
{
    public static class IRowExtensions
    {

        public static string ReplaceHtmlTag(this IRow row, string html, int length = 0)
        {
            string strText = System.Text.RegularExpressions.Regex.Replace(html, "<[^>]+>", "");
            //strText = System.Text.RegularExpressions.Regex.Replace(strText, "&[^;]+;", "");

            if (length > 0 && strText.Length > length)
                return strText.Substring(0, length);

            return strText;
        }

        public static string GetSplitEnterString(this IRow row, string str)
        {
            var sb = new StringBuilder();

            var scopeStrings = str.Split(new string[] { "\\n\\n" }, StringSplitOptions.None);
            foreach (var scopeString in scopeStrings)
            {
                sb.AppendLine(scopeString);
            }
            return sb.ToString();
        }


        public static string GetSplitString(this IRow row, string str)
        {
            var sb = new StringBuilder();

            var scopeStrings = str.Split(new string[] { "\\r\\n" }, StringSplitOptions.None);
            foreach (var scopeString in scopeStrings)
            {
                sb.AppendLine(scopeString);
            }
            return sb.ToString();
        }
        public static string GetString(this IRow row, int index)
        {
            var cell = row.GetCell(index);
            if (cell == null) return "";
            if (cell.CellType == CellType.Numeric)
                return cell.NumericCellValue.ToString();
            return cell.StringCellValue.Trim();
        }

        public static double GetNumber(this IRow row, int index)
        {
            var cell = row.GetCell(index);
            if (cell == null) return -1;
            return cell.CellType == CellType.Numeric ? cell.NumericCellValue : 0;
        }

        public static string GetRequiredString(this IRow row, int index)
        {
            var val = GetString(row, index);
            if (!string.IsNullOrEmpty(val))
            {
                return val;
            }
            //MarkError(row.GetCell(index));
            return null;
        }

        public static DateTime? GetNullableDate(this IRow row, int index)
        {
            try
            {
                var cell = row.GetCell(index);
                DateTime? date = null;
                try
                {
                    date = cell.DateCellValue;
                }
                catch (Exception)
                {
                    date = DateTime.Parse(cell.StringCellValue);
                }
                return date > DateTime.MinValue ? (DateTime?)date : null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DateTime? GetDate(this IRow row, int index)
        {
            try
            {
                var cell = row.GetCell(index);
                if (cell == null) return null;
                DateTime? date = null;
                try
                {
                    date = cell.DateCellValue;
                }
                catch (Exception)
                {
                    date = DateTime.Parse(cell.StringCellValue);
                }
                return date;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool GetBool(this IRow row, int index)
        {
            var cell = row.GetCell(index);
            if (cell.CellType == CellType.Boolean)
            {
                return cell.BooleanCellValue;
            }

            if (cell.CellType == CellType.Numeric)
            {
                return cell.NumericCellValue == 1;
            }

            var flag = row.GetCell(index).StringCellValue.Trim().ToLower();
            if (flag == "y" || flag == "是" || flag == "1")
                return true;
            //if (flag == "n")
            //    return false;
            return false;
        }

        public static void MarkError(this IRow row)
        {
            foreach (var cell in row.Cells)
            {
                cell.MarkError();
            }
        }

        public static void MarkError(this ICell cell)
        {
            cell.CellStyle.FillBackgroundColor = HSSFColor.Red.Index;
        }

        public static void MarkErrorWithCellStyle(this IRow row, int coumnCount, ICellStyle style)
        {
            var cellCount = row.LastCellNum;
            if (coumnCount <= cellCount) return;
            for (var i = cellCount; i < coumnCount; i++)
            {
                (row.GetCell(i) ?? row.CreateCell(i)).MarkErrorWithCellStyle(style);
            }
        }

        public static void MarkErrorWithCellStyle(this IRow row, ICellStyle style)
        {
            foreach (var cell in row.Cells)
            {
                cell.MarkErrorWithCellStyle(style);
            }
        }

        public static void MarkErrorWithCellStyle(this ICell cell, ICellStyle style)
        {
            style.FillForegroundColor = HSSFColor.Red.Index;
            style.FillPattern = FillPattern.SolidForeground;
            cell.CellStyle = style;
        }

        public static void MarkErrorWithCellStyle(this IRow row, List<int> indexs, ICellStyle style)
        {
            foreach (var index in indexs)
            {
                (row.GetCell(index) ?? row.CreateCell(index)).MarkErrorWithCellStyle(style);
            }
        }

        public static bool? GetCheckBool(this string str)
        {
            if (str == "Y" || str == "y")
            {
                return true;
            }
            if (str == "N" || str == "n")
            {
                return false;
            }
            return null;
        }
    }
}
