﻿using System;
using System.Collections.Generic;

namespace DdJob.Utilities.Excel
{
    public class ExcelColumnFactory<T>
    {
        public List<ExcelColumnBuilder<T>> Columns = new List<ExcelColumnBuilder<T>>();

        public int ColumnCount
        {
            get { return Columns.Count; }
        }

        public ExcelColumnBuilder<T> Bound(Func<T, object> property)
        {
            var builder = new ExcelColumnBuilder<T>(property);
            Columns.Add(builder);
            return builder;
        }
    }
}
