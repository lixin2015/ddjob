﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using NPOI.SS.UserModel;

namespace DdJob.Utilities.Excel
{
    public abstract class ExcelImporter<T> where T : class
    {
        /// <summary>
        /// 列的数量
        /// </summary>
        protected abstract int ColumnCount { get; }
        /// <summary>
        /// 需要插入的表的名字
        /// </summary>
        protected abstract string TableName { get; }

        public IList<T> InsertData = new List<T>();

        private bool _hasError;

        public bool HasError
        {
            get { return _hasError; }
        }
        public virtual void Import(ISheet sheet, ICellStyle style, bool UseBulkCopy = false)
        {
            var rowNum = sheet.PhysicalNumberOfRows;
            for (var i = 1; i < rowNum; i++)
            {
                var row = sheet.GetRow(i);
                if (row.PhysicalNumberOfCells != this.ColumnCount && row.LastCellNum != this.ColumnCount)
                {
                    row.MarkErrorWithCellStyle(this.ColumnCount, style);
                    _hasError = true;
                    continue;
                }
                var ignoreThisRow = false;
                T entity = null;

                try
                {
                    entity = GetFromRow(row, style, out ignoreThisRow);
                }
                catch (Exception)
                {
                    row.MarkErrorWithCellStyle(this.ColumnCount, style);
                    _hasError = true;
                    Console.WriteLine(DateTime.Now + " 发生异常。行号：" + i);
                    continue;
                }
                if (!ignoreThisRow)
                {
                    if (entity != null)
                    {
                        InsertData.Add(entity);
                    }
                    else
                    {
                        _hasError = true;
                    }
                }
            }

            if (InsertData.Count > 0 && !_hasError)
            {
                var dt = InsertData.ToDataTable();
                if (UseBulkCopy)
                {
                    BulkCopy(dt, TableName);
                }
            }

            //Console.WriteLine(DateTime.Now + " start to bulk update data.");
            //UpdateToDb();
            //Console.WriteLine(DateTime.Now + " bulk update data finished.");
        }

        protected void BulkCopy(DataTable dt, string tableName)
        {
            Console.WriteLine(DateTime.Now + " 开始批量插入数据. 表名：" + tableName);
            var connStr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (var bulkCopy = new SqlBulkCopy(connStr) { DestinationTableName = tableName, NotifyAfter = 10000 })
            {
                bulkCopy.BulkCopyTimeout = 60;
                bulkCopy.SqlRowsCopied += bulkCopy_SqlRowsCopied;
                foreach (DataColumn dc in dt.Columns)
                {
                    bulkCopy.ColumnMappings.Add(dc.ColumnName, dc.ColumnName);
                }
                bulkCopy.WriteToServer(dt);
            }
            Console.WriteLine(DateTime.Now + " 批量插入数据成功. 表名：" + tableName);
            //var bulkCopy = new SqlBulkCopy(connStr) { DestinationTableName = tableName, NotifyAfter = 10000 };

        }

        private void bulkCopy_SqlRowsCopied(object sender, SqlRowsCopiedEventArgs e)
        {
            Console.WriteLine(e.RowsCopied + " record inserted into " + TableName);
        }

        protected abstract T GetFromRow(IRow row, ICellStyle style, out bool ignoreThisRow);

    }
}
