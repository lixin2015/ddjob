﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace DdJob.Utilities.Excel
{
    public static class ListExtensions
    {
        public static DataTable ToDataTable<T>(this IList<T> items)
        {
            var types = new List<Type>()
            {
                typeof (bool),
                typeof (int),
                typeof (double),
                typeof (decimal),
                typeof (float),
                typeof (long)
            };
            var dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            var props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                var type = prop.PropertyType;
                //if (!prop.CanWrite || type.BaseType == typeof(BaseEntity)
                //    || (type.IsGenericType 
                //    && type.GetGenericTypeDefinition() == typeof(ICollection<>)
                //    && type.GetGenericArguments()[0].BaseType == typeof(BaseEntity)))
                if (!prop.CanWrite || (type != typeof(string) && !type.IsValueType))
                {
                    continue;
                }

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    type = type.GetGenericArguments()[0];
                }

                dataTable.Columns.Add(prop.Name, type);
            }

            foreach (T item in items)
            {
                if (item != null)
                {
                    var dr = dataTable.NewRow();

                    foreach (var p in props)
                    {
                        var type = p.PropertyType;
                        //if (!p.CanWrite || type.BaseType == typeof(BaseEntity)
                        //        || (type.IsGenericType 
                        //        && type.GetGenericTypeDefinition() == typeof(ICollection<>)
                        //        && type.GetGenericArguments()[0].BaseType == typeof(BaseEntity)))
                        if (!p.CanWrite || (type != typeof(string) && !type.IsValueType))
                            continue;

                        //If struct type
                        if (types.IndexOf(p.PropertyType) >= 0)
                        {
                            dr[p.Name] = p.GetValue(item);
                        }
                        else
                        {
                            dr[p.Name] = p.GetValue(item) ?? DBNull.Value;
                        }
                    }

                    dataTable.Rows.Add(dr);
                }
            }
            return dataTable;
        }
    }
}
