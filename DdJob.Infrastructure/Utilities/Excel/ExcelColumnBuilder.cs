﻿using System;

namespace DdJob.Utilities.Excel
{
    public class ExcelColumnBuilder<T>
    {
        public const int ExcelWidthRate = 32;

        private readonly Func<T, object> _property;
        public Func<T, object> Property
        {
            get { return _property; }
        }

        private int _columnWidth;
        public int ColumnWidth
        {
            get { return _columnWidth; }
        }

        private Func<int, int> _widthFunction;
        public Func<int, int> WidthFunction
        {
            get { return _widthFunction; }
        }

        private string _columnTitle;
        public string ColumnTitle
        {
            get { return _columnTitle; }
        }

        private string _columnSummaryInfo;

        private bool _isListSummaryInfo;
        public string ColumnSummaryInfo
        {
            get { return _columnSummaryInfo; }
        }

        public bool IsListSummaryInfo
        {
            get { return _isListSummaryInfo; }
        }

        private Func<T, string[]> _cellLines;
        public Func<T, string[]> CellLines
        {
            get { return _cellLines; }
        }

        public ExcelColumnBuilder(Func<T, object> property)
        {
            _property = property;
        }

        /// <summary>
        /// Set width of the column in px.
        /// </summary>
        /// <param name="width">in px</param>
        /// <returns></returns>
        public ExcelColumnBuilder<T> Width(int width)
        {
            _columnWidth = width * ExcelWidthRate;
            return this;
        }

        public ExcelColumnBuilder<T> Width(Func<int, int> widthFunction)
        {
            _widthFunction = widthFunction;
            return this;
        }

        public ExcelColumnBuilder<T> Title(string title)
        {
            _columnTitle = title;
            return this;
        }
        public ExcelColumnBuilder<T> SummaryInfo(string summaryInfo)
        {
            _columnSummaryInfo = summaryInfo;
            return this;
        }

        public ExcelColumnBuilder<T> ShowListSummaryInfo(bool isListSummaryInfo)
        {
            _isListSummaryInfo = isListSummaryInfo;
            return this;
        }

        public ExcelColumnBuilder<T> Lines(Func<T, string[]> cellValues)
        {
            _cellLines = cellValues;
            return this;
        }
    }
}
