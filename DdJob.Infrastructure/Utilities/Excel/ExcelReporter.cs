﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;

namespace DdJob.Utilities.Excel
{
    public class ExcelReporter
    {
        public IWorkbook Workbook { get; set; }
        public ISheet CurrentSheet { get; private set; }
        public int BodyLineHeight { get; set; }

        public ExcelReporter()
        {
            this.Workbook = new HSSFWorkbook();
            this.BodyLineHeight = 300;
            this.Initialize();
        }


        public void Export<T>(string sheetName, IEnumerable<T> items, Action<ExcelColumnFactory<T>> columns)
            where T : class
        {
            var sheet = String.IsNullOrEmpty(sheetName)
                            ? this.Workbook.CreateSheet()
                            : this.Workbook.CreateSheet(sheetName);
            var factory = new ExcelColumnFactory<T>();
            columns(factory);
            this.CurrentSheet = sheet;
            this.GenerateHeader(factory, sheet);
            this.GenerateItems(items, factory, sheet);
            this.GenerateSummaryInfo(factory, sheet, items.Count() + 1);

            var columnIndex = 0;
            foreach (var column in factory.Columns)
            {
                sheet.AutoSizeColumn(columnIndex);
                var width = column.ColumnWidth;
                if (width <= 0)
                {
                    if (column.WidthFunction != null)
                    {
                        var currentWidth = sheet.GetColumnWidth(columnIndex);
                        const int rate = ExcelColumnBuilder<T>.ExcelWidthRate;
                        width = column.WidthFunction(currentWidth / rate) * rate;
                    }
                }
                if (width > 0)
                {
                    sheet.SetColumnWidth(columnIndex, width);
                }
                else
                {
                    sheet.SetColumnWidth(columnIndex, sheet.GetColumnWidth(columnIndex) + 500);
                }
                columnIndex++;
            }
        }

        public void Write(Stream stream)
        {
            this.Workbook.Write(stream); 
        }

        #region private methods

        private void Initialize()
        {
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();

            si.Author = "ZZB";
            si.CreateDateTime = DateTime.Now;
            si.LastSaveDateTime = DateTime.Now;
            si.ApplicationName = "ZZB";
            si.Keywords = "ZZB";
            dsi.Company = "ZZB";

            var hssfWorkbook = this.Workbook as HSSFWorkbook;
            if (hssfWorkbook != null)
            {
                hssfWorkbook.SummaryInformation = si;
                hssfWorkbook.DocumentSummaryInformation = dsi;
            }
        }

        private void GenerateHeader<T>(ExcelColumnFactory<T> factory, ISheet sheet, int startRowIndex = 0)
            where T : class
        {
            var headRow = sheet.CreateRow(startRowIndex);
            var columnIndex = 0;
            var headerStyle = this.Workbook.CreateCellStyle();
            headerStyle.FillForegroundColor = HSSFColor.LightCornflowerBlue.Index;
            headerStyle.FillPattern = FillPattern.SolidForeground;
            var font = this.Workbook.CreateFont();
            font.Boldweight = 2330;
            headerStyle.SetFont(font);
            headerStyle.VerticalAlignment = VerticalAlignment.Center;
            headRow.Height = (short)(this.BodyLineHeight + 150);
            foreach (var column in factory.Columns)
            {
                var columnCell = headRow.CreateCell(columnIndex);
                columnCell.SetCellValue(column.ColumnTitle);
                columnCell.CellStyle = headerStyle;
                columnIndex++;
            }
            headRow.CreateCell(columnIndex);
        }

        private void GenerateSummaryInfo<T>(ExcelColumnFactory<T> factory, ISheet sheet, int startRowIndex)
            where T : class
        {
            if (!factory.Columns.Any(m => !string.IsNullOrEmpty(m.ColumnSummaryInfo)))
            {
                return;
            }
            var summaryRow = sheet.CreateRow(startRowIndex);
            var columnIndex = 0;
            var summaryStyle = this.Workbook.CreateCellStyle();
            summaryStyle.WrapText = true;
            summaryStyle.FillBackgroundColor = HSSFColor.Black.Index;
            summaryStyle.FillPattern = FillPattern.SolidForeground;
            var font = this.Workbook.CreateFont();
            font.Boldweight = 2330;
            font.Color = HSSFColor.White.Index;
            summaryStyle.SetFont(font);
            summaryStyle.VerticalAlignment = VerticalAlignment.Top;
            if (!factory.Columns.Any(m => !string.IsNullOrEmpty(m.ColumnSummaryInfo) && m.ColumnSummaryInfo.Contains("\n")))
            {
                summaryRow.Height = (short)(this.BodyLineHeight + 150);
            }
            foreach (var column in factory.Columns)
            {
                var columnCell = summaryRow.CreateCell(columnIndex);
                columnCell.SetCellValue(column.ColumnSummaryInfo);
                columnCell.CellStyle = summaryStyle;
                columnIndex++;
            }
            summaryRow.CreateCell(columnIndex);
        }

        private void GenerateItems<T>(IEnumerable<T> dataSource, ExcelColumnFactory<T> factory, ISheet sheet,
                                      Int32 startRowIndex = 1)
            where T : class
        {
            if (dataSource == null || !dataSource.Any())
            {
                return;
            }

            var bodyStyle = this.Workbook.CreateCellStyle();
            bodyStyle.WrapText = true;
            bodyStyle.VerticalAlignment = VerticalAlignment.Top;

            foreach (var item in dataSource)
            {
                IRow row = sheet.CreateRow(startRowIndex);

                Int32 columnIndex = 0;
                startRowIndex++;
                var maxLines = 1;
                foreach (var column in factory.Columns)
                {
                    ICell cell = row.CreateCell(columnIndex);
                    cell.CellStyle = bodyStyle;
                    var cellValue = string.Empty;
                    if (column.CellLines != null)
                    {
                        var lines = column.CellLines(item);
                        maxLines = Math.Max(lines.Length, maxLines);
                        cellValue = string.Join("\n", lines);
                    }
                    else if (column.Property != null)
                    {
                        var propertyValue = column.Property(item);
                        if (propertyValue != null)
                        {
                            cellValue = propertyValue.ToString();
                        }
                    }
                    cell.SetCellValue(cellValue);
                    columnIndex++;
                }
                row.Height = (short)(this.BodyLineHeight * maxLines + 100);
            }
        }

        #endregion
    }
}
