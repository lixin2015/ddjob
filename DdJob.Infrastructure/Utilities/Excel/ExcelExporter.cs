﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NPOI.HPSF;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

namespace DdJob.Utilities.Excel
{
    public class ExcelExporter : IDisposable
    {
        public IWorkbook Workbook { get; set; }
        public int BodyLineHeight { get; set; }
        public ICellStyle HeaderStyle { get; set; }

        public ExcelExporter()
        {
            Workbook = new XSSFWorkbook();
            BodyLineHeight = 300;
            Initialize();
        }

        public ExcelExporter ExcelExport<T>(ExcelSheetDto sheetDto, List<T> items, bool createSerialNumber,
            Action<ExcelColumnFactory<T>> columns, List<ExcelGroupHeader> list = null)
        {
            var summaryList = new List<decimal>();
            var startIndex = 0;

            var sheet = String.IsNullOrEmpty(sheetDto.SheetName)
                ? Workbook.CreateSheet()
                : Workbook.CreateSheet(sheetDto.SheetName);

            var factory = new ExcelColumnFactory<T>();
            columns(factory);
            //CurrentSheet = sheet;
            if (!string.IsNullOrWhiteSpace(sheetDto.Title))
            {
                GenerateTitle(sheetDto.Title, sheet, ref startIndex);
                CreateBlankRow(ref startIndex);
            }
            if (sheetDto.Criterias != null && sheetDto.Criterias.Count > 0)
            {
                GenerateCriterias(sheetDto.Criterias, sheet, ref startIndex);
                CreateBlankRow(ref startIndex);
            }
            var maxColumnCount = 0;
            if (sheetDto.SheetHeader == null)
            {
                GenerateHeader(factory, sheet, ref startIndex, createSerialNumber, ref maxColumnCount);
            }
            else
            {
                GenerateGroupHeader(sheetDto.SheetHeader, sheet, ref startIndex, createSerialNumber, list,
                    ref maxColumnCount);
            }
            GenerateItems(items.ToArray(), factory, sheet, ref startIndex, createSerialNumber, summaryList);

            GenerateSummaryInfo(factory, sheet, startIndex, summaryList);

            const int defaultColumnWidth = 120 * 256 / 8;
            if (maxColumnCount != factory.Columns.Count)
            {
                for (var i = 0; i < maxColumnCount; i++)
                {
                    sheet.SetColumnWidth(i, defaultColumnWidth);
                }
            }
            else
            {
                var columnIndex = 0;
                const int rate = ExcelColumnBuilder<T>.ExcelWidthRate;
                foreach (var column in factory.Columns)
                {
                    //sheet.AutoSizeColumn(columnIndex);
                    var width = column.ColumnWidth;
                    if (width <= 0)
                    {
                        sheet.SetColumnWidth(columnIndex, defaultColumnWidth);
                    }
                    else
                    {
                        sheet.SetColumnWidth(columnIndex, (width / rate) * 256 / 8);
                    }
                    columnIndex++;
                }
            }
            items = null;
            list = null;
            return this;
        }

        public void Write(Stream stream)
        {
            Workbook.Write(stream);
        }

        private void Initialize()
        {
            var dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            var si = PropertySetFactory.CreateSummaryInformation();

            si.Author = "TD";
            si.CreateDateTime = DateTime.Now;
            si.LastSaveDateTime = DateTime.Now;
            si.ApplicationName = "TD";
            si.Keywords = "TD";
            dsi.Company = "TD";

            var hssfWorkbook = Workbook as HSSFWorkbook;
            if (hssfWorkbook != null)
            {
                hssfWorkbook.SummaryInformation = si;
                hssfWorkbook.DocumentSummaryInformation = dsi;
            }
            HeaderStyle = CreateAndGetHeaderStyle();
        }

        private void GenerateHeader<T>(ExcelColumnFactory<T> factory, ISheet sheet, ref int startRowIndex,
            bool createSerialNumber, ref int maxColumnCount)
        {
            var headRow = sheet.CreateRow(startRowIndex);
            startRowIndex++;
            var columnIndex = 0;

            var headerStyle = HeaderStyle;
            if (createSerialNumber)
            {
                var cell = headRow.CreateCell(columnIndex);
                cell.SetCellValue("序号");
                cell.CellStyle = headerStyle;
                columnIndex++;
                maxColumnCount++;
            }
            foreach (var column in factory.Columns)
            {
                var columnCell = headRow.CreateCell(columnIndex);
                columnCell.SetCellValue(column.ColumnTitle);
                columnCell.CellStyle = headerStyle;
                columnIndex++;
                maxColumnCount++;
            }
        }

        private void GenerateGroupHeader(SheetHeader header, ISheet sheet, ref int startRowIndex,
            bool createSerialNumber, List<ExcelGroupHeader> list, ref int maxColumnCount)
        {
            var firstRow = sheet.CreateRow(startRowIndex);
            var columnIndex = 0;
            var headerStyle = HeaderStyle;

            if (createSerialNumber)
            {
                var cell = firstRow.CreateCell(columnIndex);
                cell.SetCellValue("序号");
                cell.CellStyle = headerStyle;
                columnIndex++;
                maxColumnCount++;
            }
            if (list == null)
            {
                list = ExcelGroupHeaderToGrid(header.GroupHeader);
            }
            maxColumnCount += list.Count(m => m.ChildHeader == null || m.ChildHeader.Count == 0);
            var rowMergeCount = list == null || list.Count <= 0 ? 0 : list.Max(c => c.PositionY);
            foreach (var column in header.ColumnHeader)
            {
                var columnCell = firstRow.CreateCell(columnIndex);

                columnCell.SetCellValue(column);
                AddMergedRegionAndSetBorder(sheet, startRowIndex, startRowIndex + rowMergeCount, columnIndex,
                    columnIndex);
                columnCell.CellStyle = headerStyle;

                columnIndex++;
                maxColumnCount++;
            }

            if (list == null || list.Count <= 0)
            {
                startRowIndex++;
            }
            else
            {
                var addRowCount = 0;
                GenerateGroupHeaderWithChildren(list, sheet, ref startRowIndex, columnIndex, out addRowCount);
                startRowIndex += addRowCount;
            }
        }

        private void GenerateGroupHeaderWithChildren(IReadOnlyCollection<ExcelGroupHeader> header, ISheet sheet, ref int startRowIndex,
            int startColumnIndex, out int addRowCount)
        {
            var headerStyle = HeaderStyle;
            var maxRowSpan = 1;
            var draw = sheet.CreateDrawingPatriarch();
            foreach (var head in header)
            {
                var row = sheet.GetRow(startRowIndex + head.PositionY) ??
                          sheet.CreateRow(startRowIndex + head.PositionY);
                row.Height = Math.Max((short)(BodyLineHeight + 150), row.Height);
                var cell = row.GetCell(startColumnIndex + head.PositionX) ??
                           row.CreateCell(startColumnIndex + head.PositionX);
                if (head.HeaderName.Contains("\\n"))
                {
                    var lines = head.HeaderName.Split(new[] { "\\n" }, StringSplitOptions.RemoveEmptyEntries);
                    cell.SetCellValue(string.Join("\n", lines));
                    row.HeightInPoints = 2 * sheet.DefaultRowHeight / 20;
                }
                else
                {
                    cell.SetCellValue(head.HeaderName);
                }
                if (!string.IsNullOrEmpty(head.Comment))
                {
                    //var comment = draw.CreateCellComment(new XSSFClientAnchor(0, 0, 0, 0, 1, 2, 4, 4));
                    //comment.String = new XSSFRichTextString(head.Comment);
                    var comment = draw.CreateCellComment(new HSSFClientAnchor(0, 0, 0, 0, 1, 2, 4, 4));
                    comment.String = new HSSFRichTextString(head.Comment);
                    cell.CellComment = comment;
                }

                AddMergedRegionAndSetBorder(sheet, startRowIndex + head.PositionY,
                    startRowIndex + head.PositionY + head.RowSpan - 1,
                    startColumnIndex + head.PositionX,
                    startColumnIndex + head.PositionX + head.ColumnSpan - 1);
                maxRowSpan = Math.Max(maxRowSpan, head.RowSpan);
                cell.CellStyle = headerStyle;
            }

            if (header.Count <= 0)
            {
                addRowCount = 1;
            }
            else
            {
                var maxPositionYCell = header.Where(c => c.PositionX == header.Max(m => m.PositionY));
                if (maxPositionYCell.Any())
                {
                    var maxRowSpanCell = maxPositionYCell.First(c => c.RowSpan == maxPositionYCell.Max(q => q.RowSpan));
                    addRowCount = maxRowSpanCell.PositionY + maxRowSpanCell.RowSpan;
                }
                else
                {
                    addRowCount = 2;
                }
            }
        }

        private void GenerateSummaryInfo<T>(ExcelColumnFactory<T> factory, ISheet sheet, int startRowIndex,
            List<decimal> summaaryList)
        {
            if (factory.Columns.All(m => string.IsNullOrEmpty(m.ColumnSummaryInfo) && !m.IsListSummaryInfo))
            {
                return;
            }
            var summaryRow = sheet.CreateRow(startRowIndex);
            var columnIndex = 0;
            var summaryStyle = CreateAndGetSummaryStyle();

            if (
                !factory.Columns.Any(
                    m => !string.IsNullOrEmpty(m.ColumnSummaryInfo) && m.ColumnSummaryInfo.Contains("\n")))
            {
                summaryRow.Height = (short)(BodyLineHeight + 150);
            }
            foreach (var column in factory.Columns)
            {
                if (!column.IsListSummaryInfo)
                {
                    var columnCell = summaryRow.CreateCell(columnIndex);
                    columnCell.SetCellValue(column.ColumnSummaryInfo);
                    columnCell.CellStyle = summaryStyle;
                    columnIndex++;
                }
                else
                {
                    foreach (var v in summaaryList)
                    {
                        var columnCell = summaryRow.CreateCell(columnIndex);
                        columnCell.SetCellValue(v.ToString());
                        columnCell.CellStyle = summaryStyle;
                        columnIndex++;
                    }
                }
            }
        }

        private void GenerateItems<T>(IEnumerable<T> dataSource, ExcelColumnFactory<T> factory, ISheet sheet,
            ref int startRowIndex, bool createSerialNumber, IList<decimal> summaryList)
        {
            if (dataSource == null || !dataSource.Any())
            {
                return;
            }

            var bodyStyle = CreateAndGetItemStyle();
            var dateStyle = CreateAndGetItemDateStyle();
            var index = 1;
            foreach (var item in dataSource)
            {
                var row = sheet.CreateRow(startRowIndex);
                var columnIndex = 0;
                #region
                if (createSerialNumber)
                {
                    var cell = row.CreateCell(columnIndex);
                    cell.CellStyle = bodyStyle;
                    cell.SetCellValue(index);
                    columnIndex++;
                }
                #endregion
                index++;
                startRowIndex++;
                var maxLines = 1;
                int dataIndex = 0;
                foreach (var column in factory.Columns)
                {
                    var cell = row.CreateCell(columnIndex);
                    cell.CellStyle = bodyStyle;
                    var cellValue = string.Empty;
                    if (column.CellLines != null)
                    {
                        var lines = column.CellLines(item);
                        maxLines = Math.Max(lines.Length, maxLines);
                        cellValue = string.Join("\n", lines);
                        cell.SetCellValue(string.IsNullOrEmpty(cellValue) ? " " : cellValue);
                    }
                    else if (column.Property != null)
                    {
                        var propertyValue = column.Property(item);
                        if (propertyValue != null && !(propertyValue is IList))
                        {
                            if (propertyValue is DateTime)
                            {
                                var date = propertyValue as DateTime?;
                                cell.CellStyle = dateStyle;
                                cell.SetCellValue(date.Value.Date);
                                columnIndex++;
                            }
                            else
                            {
                                cell.CellStyle = bodyStyle;
                                cellValue = propertyValue.ToString();
                                columnIndex++;
                                cell.SetCellValue(string.IsNullOrEmpty(cellValue) ? " " : cellValue);
                            }
                        }
                        else if (propertyValue != null && (propertyValue is IList))
                        {
                            var list = propertyValue as IList;
                            if (list.Count <= 0)
                            {
                                cell.SetCellValue(" ");
                                cell.CellStyle = null;
                                columnIndex++;
                                continue;
                            }
                            foreach (var v in list)
                            {
                                ICell c = row.CreateCell(columnIndex);
                                c.CellStyle = bodyStyle;
                                if (v == null)
                                {
                                    c.SetCellValue(" ");
                                    c.CellStyle = null;
                                    columnIndex++;
                                    continue;
                                }
                                if (v is DateTime)
                                {
                                    var date = v as DateTime?;
                                    c.CellStyle = dateStyle;
                                    c.SetCellValue(date.Value.Date);
                                }
                                else
                                {
                                    var str = v.ToString();
                                    decimal d;
                                    decimal.TryParse(str, out d);
                                    if (summaryList.Count <= dataIndex)
                                    {
                                        summaryList.Add(d);
                                    }
                                    else
                                    {
                                        summaryList[dataIndex] += d;
                                    }
                                    dataIndex++;
                                    c.SetCellValue(string.IsNullOrEmpty(str) ? " " : str);
                                }
                                columnIndex++;
                            }
                        }
                        else
                        {
                            cell.SetCellValue(" ");
                            columnIndex++;
                        }
                    }
                }
                row.Height = (short)(this.BodyLineHeight * maxLines + 100);
            }
        }

        private void GenerateCriterias(IEnumerable<string> criterias, ISheet sheet, ref int startRowIndex)
        {
            foreach (var criteria in criterias)
            {
                var criteriasdRow = sheet.CreateRow(startRowIndex);
                var headerStyle = CreateAndGetCriteriasStyle();
                var columnCell = criteriasdRow.CreateCell(0);
                columnCell.CellStyle = headerStyle;
                columnCell.SetCellValue(criteria);
                startRowIndex++;
            }
        }

        private void GenerateTitle(string title, ISheet sheet, ref int startRowIndex)
        {
            var titleRow = sheet.CreateRow(startRowIndex);
            var headerStyle = CreateAndGetTitleStyle();
            var columnCell = titleRow.CreateCell(0);
            columnCell.CellStyle = headerStyle;
            columnCell.SetCellValue(title);
            startRowIndex++;
        }

        public void GenerateTitle(int columnCount, string title,
            ISheet sheet, ref int startRowIndex,
            ICellStyle cellStype, int rowSpan, int colSpan)
        {
            if (string.IsNullOrWhiteSpace(title))
            {
                return;
            }

            var titleRow = sheet.CreateRow(startRowIndex);
            var headerStyle = HeaderStyle;
            var columnCell = titleRow.CreateCell(0);
            columnCell.CellStyle = headerStyle;
            columnCell.SetCellValue(title);
            AddMergedRegionAndSetBorder(sheet, startRowIndex, startRowIndex, 0, columnCount);
            startRowIndex++;
        }

        public List<ExcelGroupHeader> ExcelGroupHeaderToGrid(List<ExcelGroupHeader> headers)
        {
            var result = new List<ExcelGroupHeader>();
            var width = 0;
            foreach (var header in headers)
            {
                GetChildren(header, 0, ref width, result);
            }
            return result;
        }

        private void GetChildren(ExcelGroupHeader header, int deep, ref int width, List<ExcelGroupHeader> result)
        {
            header.PositionY = deep;
            while (true)
            {
                if (header.ChildHeader == null && !header.IsHandle)
                {
                    header.ColumnSpan = 1;
                    header.IsHandle = true;
                    header.PositionX = width;
                    width++;
                    result.Add(header);
                    break;
                }
                var h = header.ChildHeader.FirstOrDefault(c => c != null && !c.IsHandle);
                if (h == null)
                {
                    header.ColumnSpan = header.ChildHeader.Sum(c => c.ColumnSpan);
                    header.IsHandle = true;
                    if (header.ChildHeader != null && header.ChildHeader.Count > 0)
                    {
                        header.PositionX = header.ChildHeader.Min(c => c.PositionX);
                    }
                    else
                    {
                        header.PositionX = width;
                        width++;
                    }
                    result.Add(header);
                    break;
                }
                GetChildren(h, deep + 1, ref width, result);
            }
        }

        public ICellStyle CreateAndGetTitleStyle()
        {
            var style = Workbook.CreateCellStyle();

            var font = Workbook.CreateFont();

            font.FontName = "Arial";
            style.WrapText = false;
            font.FontHeight = 16;
            font.Underline = FontUnderlineType.Single;
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.Alignment = HorizontalAlignment.Left;
            style.VerticalAlignment = VerticalAlignment.Center;
            style.SetFont(font);
            return style;
        }

        public ICellStyle CreateAndGetCriteriasStyle()
        {
            var style = Workbook.CreateCellStyle();
            var font = Workbook.CreateFont();
            font.FontName = "Arial";
            style.WrapText = false;
            font.FontHeight = 10;
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.Alignment = HorizontalAlignment.Left;
            style.VerticalAlignment = VerticalAlignment.Center;
            style.SetFont(font);
            return style;
        }

        public ICellStyle CreateAndGetHeaderStyle()
        {
            var style = Workbook.CreateCellStyle();

            var font = Workbook.CreateFont();
            font.FontName = "Arial";
            style.FillForegroundColor = HSSFColor.LightCornflowerBlue.Index;
            style.FillPattern = FillPattern.SolidForeground;

            font.FontHeight = 10;
            font.Boldweight = (short)FontBoldWeight.Bold;
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;

            //style.BorderTop = BorderStyle.Thin;
            //style.TopBorderColor = HSSFColor.Black.Index;

            //style.BorderRight = BorderStyle.Thin;
            //style.RightBorderColor = HSSFColor.Black.Index;

            //style.BorderBottom = BorderStyle.Thin;
            //style.BottomBorderColor = HSSFColor.Black.Index;

            //style.BorderLeft = BorderStyle.Thin;
            //style.LeftBorderColor = HSSFColor.Black.Index;

            style.SetFont(font);
            style.WrapText = true;
            return style;
        }

        public ICellStyle CreateAndGetItemStyle()
        {
            var style = Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Left;
            var font = Workbook.CreateFont();

            font.FontName = "Calibri";
            font.FontHeight = 11;

            style.Alignment = HorizontalAlignment.Left;
            style.VerticalAlignment = VerticalAlignment.Center;

            //style.BorderTop = BorderStyle.Thin;
            //style.TopBorderColor = HSSFColor.Black.Index;

            //style.BorderRight = BorderStyle.Thin;
            //style.RightBorderColor = HSSFColor.Black.Index;

            //style.BorderBottom = BorderStyle.Thin;
            //style.BottomBorderColor = HSSFColor.Black.Index;

            //style.BorderLeft = BorderStyle.Thin;
            //style.LeftBorderColor = HSSFColor.Black.Index;

            style.WrapText = false;
            style.SetFont(font);

            return style;
        }

        public ICellStyle CreateAndGetItemDateStyle()
        {
            var style = Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Left;
            var font = Workbook.CreateFont();

            font.FontName = "Calibri";
            font.FontHeight = 11;

            style.Alignment = HorizontalAlignment.Left;
            style.VerticalAlignment = VerticalAlignment.Center;

            //style.BorderTop = BorderStyle.Thin;
            //style.TopBorderColor = HSSFColor.Black.Index;

            //style.BorderRight = BorderStyle.Thin;
            //style.RightBorderColor = HSSFColor.Black.Index;

            //style.BorderBottom = BorderStyle.Thin;
            //style.BottomBorderColor = HSSFColor.Black.Index;
            //style.DataFormat = Workbook.CreateDataFormat().GetFormat("yyyy/MM/dd");

            //style.BorderLeft = BorderStyle.Thin;
            //style.LeftBorderColor = HSSFColor.Black.Index;

            style.WrapText = false;
            style.SetFont(font);
            return style;
        }

        public ICellStyle CreateAndGetSummaryStyle()
        {
            var style = Workbook.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Left;
            var font = Workbook.CreateFont();

            font.FontName = "Calibri";
            font.FontHeight = 11;

            style.Alignment = HorizontalAlignment.Left;
            style.VerticalAlignment = VerticalAlignment.Center;


            //style.BorderTop = BorderStyle.Thin;
            //style.TopBorderColor = HSSFColor.Black.Index;

            //style.BorderRight = BorderStyle.Thin;
            //style.RightBorderColor = HSSFColor.Black.Index;

            //style.BorderBottom = BorderStyle.Thin;
            //style.BottomBorderColor = HSSFColor.Black.Index;

            //style.BorderLeft = BorderStyle.Thin;
            //style.LeftBorderColor = HSSFColor.Black.Index;

            style.WrapText = false;
            style.SetFont(font);
            return style;
        }

        public void CreateBlankRow(ref int rowIndex)
        {
            rowIndex++;
        }

        public void AddMergedRegionAndSetBorder(ISheet sheet, int startRow, int endRow, int startCol, int endCol)
        {
            const BorderStyle borderStyle = BorderStyle.Thin;
            const short borderColor = HSSFColor.Black.Index;

            var region = new CellRangeAddress(startRow, endRow, startCol, endCol);
            sheet.AddMergedRegion(region);
            for (var i = region.FirstRow; i <= region.LastRow; i++)
            {
                var row = sheet.GetRow(i) ?? sheet.CreateRow(i);
                for (var j = region.FirstColumn; j <= region.LastColumn; j++)
                {
                    var cell = row.GetCell(j);
                    if (cell == null)
                    {
                        cell = row.CreateCell(j);
                        cell.SetCellValue("");
                    }
                    cell.CellStyle = HeaderStyle;
                }
            }
        }

        public void Dispose()
        {
            Workbook = null;
            GC.Collect();
        }
    }

    public class ExcelDto
    {
        public string FileName { get; set; }

        public List<ExcelSheetDto> Sheets { get; set; }
    }

    public class ExcelSheetDto
    {
        public SheetHeader SheetHeader { set; get; }

        public string SheetName { get; set; }

        public string Title { get; set; }

        public List<string> Criterias { get; set; }
    }

    public class SheetHeader
    {
        public List<string> ColumnHeader { set; get; }
        public List<ExcelGroupHeader> GroupHeader { set; get; }

        public SheetHeader()
        {
            ColumnHeader = new List<string>();
            GroupHeader = new List<ExcelGroupHeader>();
        }
    }

    public class ExcelGroupHeader
    {
        public int PositionX { get; set; }
        public int PositionY { set; get; }
        public int RowSpan { set; get; }

        public int ColumnSpan { set; get; }

        public string Comment { get; set; }

        public bool IsHandle { set; get; }

        public ExcelGroupHeader()
        {
            RowSpan = 1;
            ColumnSpan = 1;
            IsHandle = false;
        }
        public string HeaderName { set; get; }
        public List<ExcelGroupHeader> ChildHeader { set; get; }
    }
}