﻿using DotLiquid;
using DdJob.Utilities.Template;

namespace DdJob.Utilities.Template
{
    public class TemplateHelper
    {
        static TemplateHelper()
        {
        }

        public static string FillTemplate(string templateStr, EmailDrop parameterObj)
        {
            DotLiquid.Template.NamingConvention = new DotLiquid.NamingConventions.CSharpNamingConvention();

            var template = DotLiquid.Template.Parse(templateStr);  // Parses and compiles the template

            var result = template.Render(new RenderParameters
            {
                RethrowErrors = true,
                LocalVariables = Hash.FromAnonymousObject(new { Learner = parameterObj.Learner, Course = parameterObj.Apc, Order = parameterObj.Order, Instructor = parameterObj.Instructor, System = parameterObj.System })
            });

            return result;
        }
    }
}
