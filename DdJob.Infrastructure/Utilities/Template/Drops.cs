﻿using System;
using DotLiquid;

namespace DdJob.Utilities.Template
{
    public class EmailDrop : Drop
    {
        public LearnerDrop Learner { get; private set; }
        public ApcCourceDrop Apc { get; private set; }
        public OrderDrop Order { get; private set; }
        public InstructorDrop Instructor { get; private set; }
        public SystemInfoDrop System { get; private set; }

        public EmailDrop(LearnerDrop learner = null, ApcCourceDrop apc = null, OrderDrop order = null, InstructorDrop instructor = null)
        {
            Apc = apc;
            Learner = learner;
            Order = order;
            Instructor = instructor;
            System = new SystemInfoDrop();
        }

        #region calass for email

        public class LearnerDrop : Drop
        {
            #region Learner contact information

            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string MiddleName { get; set; }
            public string FullName
            {
                get
                {
                    return string.IsNullOrWhiteSpace(MiddleName) ? string.Format("{0} {1}", FirstName, LastName)
                        : string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
                }
            }
            public string PreferredName { get; set; }
            public string EmailAddress { get; set; }

            #endregion

            #region Learner brokerage information

            public BrokerageDrop Brokerage { get; set; }

            #endregion

            #region APC Learner Course Progress And APC Compliance Information

            public MailingAddressDrop MailingAddress { get; set; }
            public ApcDrop Apc { get; set; }
            public PreviousApcDrop[] PreviousApcArray { get; set; }

            #endregion
        }

        public class ApcCourceDrop : Drop
        {
            public ApcCourceLearnerDrop[] LearnersArray { get; set; }
            public string CourseCode { get; set; }
            public string CourseType { get; set; }
            public string Region { get; set; }
            public CourseComponentDrop[] ComponentArray { get; set; }
        }

        public class OrderDrop : Drop
        {
            public long Id { get; set; }
            public decimal Subtotal { get; set; }
            public decimal Tax1 { get; set; }
            public decimal Tax2 { get; set; }
            public decimal Total { get; set; }
            public DateTime Date { get; set; }
            public string Type { get; set; }
            public string Status { get; set; }

            public OrderItemDrop[] ItemArray { get; set; }
            public string PaymentStatus { get; set; }
            public PaymentDrop[] PaymentArray { get; set; }
        }

        public class InstructorDrop : Drop
        {
            public string MemberBoard { get; set; }
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Designation { get; set; }
            public string Email { get; set; }
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string City { get; set; }
            public string PostalCode { get; set; }
            public string PrimaryPhone { get; set; }
        }

        public class SystemInfoDrop : Drop
        {
            public DateTime DateTime { get { return DateTime.Now; } }
        }

        #endregion

        #region Learner common sub class
        public class MailingAddressDrop
        {
            public string Line1 { get; set; }
            public string Line2 { get; set; }
            /// <summary>
            /// Mailing address Line 1 + " " + Mailing address line 2
            /// </summary>
            public string Line1And2Break
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(Line1) && !string.IsNullOrWhiteSpace(Line2))
                    {
                        return Line1 + " " + Line2;
                    }
                    if (string.IsNullOrWhiteSpace(Line2))
                    {
                        return Line1;
                    }
                    return string.IsNullOrWhiteSpace(Line1) ? Line2 : "";
                }
            }
            public string City { get; set; }
            public string Prov { get; set; }
            public string PostalCode { get; set; }
        }

        public class ApcDrop
        {
            public DateTime CompletionDate { get; set; }
            public string CurrentComponentName { get; set; }
            public string CurrentSessionName { get; set; }
            public DateTime[] NoShowDateArray { get; set; }
            public DateTime Components1And2CutoffDate { get; set; }
            public DateTime Components3And4CutoffDate { get; set; }
            public DateTime RegDate { get; set; }
            public string CourseCode { get; set; }
            public string CourseType { get; set; }
            public string Region { get; set; }
            public CourseComponentDrop[] ComponentArray { get; set; }
        }

        public class CourseComponentDrop
        {
            /// <summary>
            /// Formate: dd/mm/yyyy
            /// </summary>
            public DateTime[] DateArray { get; set; }
            public SessionDrop[] SessionArray { get; set; }
        }

        public class PreviousApcDrop
        {
            public string Code { get; set; }
            public string RemovalReason { get; set; }
            public string RemovalReasonDetail { get; set; }
        }

        public class SessionDrop
        {
            public DateTime Date { get; set; }
            public string Instructions { get; set; }
            public string WebInstructions { get; set; }
            public VenueDrop Venue { get; set; }
        }

        public class VenueDrop
        {
            public string Notes { get; set; }
            public string Name { get; set; }
            public string Room { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string City { get; set; }
            public string PostalCode { get; set; }
        }

        public class BrokerageDrop
        {
            /// <summary>
            /// Brokerage Name
            /// </summary>
            public string Name { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string AddressLine1And2
            {
                get
                {
                    if (!string.IsNullOrWhiteSpace(AddressLine1) && !string.IsNullOrWhiteSpace(AddressLine2))
                    {
                        return AddressLine1 + " " + AddressLine2;
                    }
                    if (string.IsNullOrWhiteSpace(AddressLine2))
                    {
                        return AddressLine1;
                    }
                    return string.IsNullOrWhiteSpace(AddressLine1) ? AddressLine2 : "";
                }
            }
            public string Phonebreak { get; set; }
        }
        #endregion

        #region APC Course common sub class

        public class ApcCourceLearnerDrop
        {
            public string LastFirstName { get; set; }
            public string FullName { get; set; }
            public string City { get; set; }
        }

        #endregion

        #region Learner order common sub class

        public class OrderItemDrop
        {
            public string Item { get; set; }
            public decimal Cost { get; set; }
            public decimal Subtotal { get; set; }
            public decimal Tax1 { get; set; }
            public decimal Tax2 { get; set; }
            public decimal Total { get; set; }
            public string Description { get; set; }
            public int Quantity { get; set; }
        }

        public class PaymentDrop
        {
            public string CcType { get; set; }
            public string CcLast4 { get; set; }
            public DateTime Date { get; set; }
            public string ConfirmationCode { get; set; }
        }

        #endregion
    }
}
