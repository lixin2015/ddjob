using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Threading;
using DdJob.Extensions;

namespace DdJob.Utilities
{
    /// <summary>
    ///   Wrapper a data source object for Enum Type, and provides
    ///   the way to display the Enum Title from resource file in Culture.
    /// </summary>
    /// <typeparam name = "TEnumType">
    ///   The Enum type.
    /// </typeparam>
    /// <exception cref = "NotSupportedException" />
    /// <example>
    ///   <code>
    ///     this.comboBox1.DataSource = new EnumDataSource&lt;MyEnum&gt;();
    ///     this.comboBox1.DisplayMember = "DisplayValue";
    ///     this.comboBox1.ValueMember = "Value";
    ///   </code>
    /// </example>
    public class EnumDataSource<TEnumType> : List<EnumDataSource<TEnumType>.EnumAdapter>
    {
        /// <summary>
        ///   Constructor a new <see cref = "EnumDataSource{EnumType}" />
        /// </summary>
        /// <exception cref = "NotSupportedException" />
        public EnumDataSource()
        {
            if (!typeof(TEnumType).IsEnum)
            {
                throw new NotSupportedException("Can not support type: " + typeof(TEnumType).FullName);
                // It's better use resource version as below.
                // throw new NotSupportedException(SR.GetString("TYPE_NOT_SUPPORT",  typeof(EnumType).FullName));
            }

            // Use Enum helper enumerator list all enum values and add to current context.
            foreach (TEnumType value in Enum.GetValues(typeof(TEnumType)))
            {
                Add(new EnumAdapter(value));
            }
        }

        #region Nested type: EnumAdapter

        /// <summary>
        ///   Enum value adapter, used to get values from each Cultures.
        /// </summary>
        public sealed class EnumAdapter : IConvertible
        {
            /// <summary>
            ///   Storage the actual Enum value.
            /// </summary>
            private readonly TEnumType _value;

            /// <summary>
            ///   Constructor an <see cref = "EnumAdapter" />.
            /// </summary>
            /// <param name = "value">The enum value.</param>
            public EnumAdapter(TEnumType value)
            {
                if (!Enum.IsDefined(typeof(TEnumType), value))
                {
                    throw new ArgumentException(
                        string.Format("{0} is not defined in {1}", value, typeof(TEnumType).Name), "value");
                    // It's better use resource version as below.
                    // throw new ArgumentException(SR.GetString("ENUM_NOT_DEFINED_FMT_KEY", value, typeof(EnumType).Name), "value");
                }
                _value = value;
            }

            /// <summary>
            ///   Gets the actual enum value.
            /// </summary>
            public TEnumType Value
            {
                get { return _value; }
            }

            /// <summary>
            ///   Gets the display value for enum value by search local resource with current UI culture 
            ///   and special key which is concated from Enum type name and Enum value name.
            /// </summary>
            /// <remarks>
            ///   This would get correct display value by accessing location resource with current UI Culture.
            /// </remarks>
            public string DisplayValue
            {
                get
                {
                    //string retval = SR.GetString(string.Format("{0}_{1}", typeof (TEnumType).Name, _value));
                    //if (string.IsNullOrEmpty(retval)) retval = ConvertText(_value.ToString());
                    //return retval;
                    return _value.ToString().ToWords();
                }
            }
            public string DescriptionAttributeInfo
            {
                get
                {
                    DescriptionAttribute attr = Attribute.GetCustomAttribute(_value.GetType().GetField(_value.ToString()), typeof(DescriptionAttribute), false) as DescriptionAttribute;
                    if (attr != null)
                    {
                        return attr.Description;
                    }
                    return "";
                }
            }

            #region IConvertible Members

            /// <summary>
            ///   Returns the <see cref = "T:System.TypeCode" /> for this instance.
            /// </summary>
            /// <returns>
            ///   The enumerated constant that is the <see cref = "T:System.TypeCode" /> of the class or value type that implements this interface.
            /// </returns>
            public TypeCode GetTypeCode()
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent Boolean value using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   A Boolean value equivalent to the value of this instance.
            /// </returns>
            public bool ToBoolean(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 8-bit unsigned integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 8-bit unsigned integer equivalent to the value of this instance.
            /// </returns>
            public byte ToByte(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent Unicode character using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   A Unicode character equivalent to the value of this instance.
            /// </returns>
            public char ToChar(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent <see cref = "T:System.DateTime" /> using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   A <see cref = "T:System.DateTime" /> instance equivalent to the value of this instance.
            /// </returns>
            public DateTime ToDateTime(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent <see cref = "T:System.Decimal" /> number using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   A <see cref = "T:System.Decimal" /> number equivalent to the value of this instance.
            /// </returns>
            public decimal ToDecimal(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent double-precision floating-point number using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   A double-precision floating-point number equivalent to the value of this instance.
            /// </returns>
            public double ToDouble(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 16-bit signed integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 16-bit signed integer equivalent to the value of this instance.
            /// </returns>
            public short ToInt16(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 32-bit signed integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 32-bit signed integer equivalent to the value of this instance.
            /// </returns>
            public int ToInt32(IFormatProvider provider)
            {
                return Convert.ToInt32(_value);
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 64-bit signed integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 64-bit signed integer equivalent to the value of this instance.
            /// </returns>
            public long ToInt64(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 8-bit signed integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 8-bit signed integer equivalent to the value of this instance.
            /// </returns>
            public sbyte ToSByte(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent single-precision floating-point number using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   A single-precision floating-point number equivalent to the value of this instance.
            /// </returns>
            public float ToSingle(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Returns a <see cref = "System.String" /> that represents this instance.
            /// </summary>
            /// <param name = "provider">The provider.</param>
            /// <returns>
            ///   A <see cref = "System.String" /> that represents this instance.
            /// </returns>
            public string ToString(IFormatProvider provider)
            {
                return DisplayValue;
            }

            /// <summary>
            ///   Converts the value of this instance to an <see cref = "T:System.Object" /> of the specified <see cref = "T:System.Type" /> that has an equivalent value, using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "conversionType">The <see cref = "T:System.Type" /> to which the value of this instance is converted.</param>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An <see cref = "T:System.Object" /> instance of type <paramref name = "conversionType" /> whose value is equivalent to the value of this instance.
            /// </returns>
            public object ToType(Type conversionType, IFormatProvider provider)
            {
                return _value;
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 16-bit unsigned integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 16-bit unsigned integer equivalent to the value of this instance.
            /// </returns>
            public ushort ToUInt16(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 32-bit unsigned integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 32-bit unsigned integer equivalent to the value of this instance.
            /// </returns>
            public uint ToUInt32(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            /// <summary>
            ///   Converts the value of this instance to an equivalent 64-bit unsigned integer using the specified culture-specific formatting information.
            /// </summary>
            /// <param name = "provider">An <see cref = "T:System.IFormatProvider" /> interface implementation that supplies culture-specific formatting information.</param>
            /// <returns>
            ///   An 64-bit unsigned integer equivalent to the value of this instance.
            /// </returns>
            public ulong ToUInt64(IFormatProvider provider)
            {
                throw new NotImplementedException();
            }

            #endregion

            /// <summary>
            ///   Returns a <see cref = "System.String" /> that represents this instance.
            /// </summary>
            /// <returns>
            ///   A <see cref = "System.String" /> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return DisplayValue;
            }
        }

        #endregion
    }

    /// <summary>
    ///   Defined the extension for the EnumDataSource
    /// </summary>
    public static class EnumDataSourceExtension
    {
        /// <summary>
        ///   Gets the display value.
        /// </summary>
        /// <typeparam name = "TEnum">The type of the enum.</typeparam>
        /// <param name = "enumValue">The enum value.</param>
        /// <returns>The display value from resource file</returns>
        public static string GetDisplayValue<TEnum>(this TEnum enumValue) where TEnum : struct
        {
            //string retval = SR.GetString(string.Format("{0}_{1}", typeof(TEnum).Name, TEnum.ToString()));
            //if (string.IsNullOrEmpty(retval)) retval = enumValue.ToString().ToWords();
            //return retval;
            //return enumValue.ToString().ToWords();
            return EnumHelper.GetEnumDescription<TEnum>(enumValue);
        }
    }

    public sealed class SR
    {
        private static readonly Dictionary<string, ResourceManager> _srCollection =
            new Dictionary<string, ResourceManager>();

        /// <summary>
        /// Gets the culture.
        /// </summary>
        private static CultureInfo Culture
        {
            //get { return null; }
            get { return Thread.CurrentThread.CurrentCulture; }
        }

        /// <summary>
        /// Attaches the resource manager.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        /// <param name="resourceManager">The resource manager.</param>
        public static void AttachResourceManager(string resourceName, ResourceManager resourceManager)
        {
            if (!_srCollection.ContainsKey(resourceName))
                _srCollection.Add(resourceName, resourceManager);
        }

        /// <summary>
        /// Detaches the resource manager.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        /// <param name="resourceManager">The resource manager.</param>
        public static void DetachResourceManager(string resourceName, ResourceManager resourceManager)
        {
            if (_srCollection.ContainsKey(resourceName))
                _srCollection.Remove(resourceName);
        }

        /// <summary>
        /// Gets the resource manager.
        /// </summary>
        /// <param name="resourceName">Name of the resource.</param>
        /// <returns></returns>
        public static ResourceManager GetResourceManager(string resourceName = null)
        {
            if (string.IsNullOrEmpty(resourceName))
                return _srCollection.Values.FirstOrDefault();

            if (_srCollection.ContainsKey(resourceName))
                return _srCollection[resourceName];
            return null;
        }

        /// <summary>
        ///   Gets an object from resources by special key, which provided by <paramref name = "name" />.
        /// </summary>
        /// <param name = "name">Resource accessed key</param>
        /// <param name="resourceName"> </param>
        /// <returns>return stored object in resource. if resource not found, return <paramref name = "name" /> as object.</returns>
        public static object GetObject(string name, string resourceName = "")
        {
            object retval = name;
            try
            {
                if (!string.IsNullOrEmpty(resourceName) && _srCollection.ContainsKey(resourceName))
                {
                    ResourceManager sr = _srCollection[resourceName];
                    return sr.GetObject(name, Culture);
                }
                foreach (var rsItem in _srCollection)
                {
                    retval = rsItem.Value.GetObject(name, Culture);
                    if (retval != null) break;
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
            }
            return retval;
        }

        /// <summary>
        ///   Gets a string from resources by special key, which provided by <paramref name = "name" />.
        /// </summary>
        /// <param name = "name">Resource accessed key</param>
        /// <param name="resourceName"> </param>
        /// <returns>return stored string in resource. If resource not found, return <paramref name = "name" /> as result.</returns>
        public static string GetString(string name, string resourceName = "")
        {
            string retval = name;
            try
            {
                if (!string.IsNullOrEmpty(resourceName) && _srCollection.ContainsKey(resourceName))
                {
                    ResourceManager sr = _srCollection[resourceName];
                    return sr.GetString(name, Culture);
                }
                foreach (var rsItem in _srCollection)
                {
                    string text = rsItem.Value.GetString(name, Culture);
                    if (text != null)
                    {
                        retval = text;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
            }
            return retval;
        }

        /// <summary>
        ///   Gets a formatted string from resources by special key, which provided by <paramref name = "name" /> and optional parameters.
        /// </summary>
        /// <param name = "name">Resource accessed key</param>
        /// <param name = "args">format arguments.</param>
        /// <returns>return stored string in resource. If resource not found, use <paramref name = "name" /> as formator, return the formatted string.</returns>
        public static string GetString(string name, params object[] args)
        {
            string format = name;
            try
            {
                format = GetString(name, Culture);
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message);
                Trace.WriteLine(ex.StackTrace);
            }

            if ((args == null) || (args.Length <= 0))
            {
                return format;
            }

            // It's better cut long argument for formatting.
            for (int i = 0; i < args.Length; i++)
            {
                var arg = args[i] as string;
                if ((arg != null) && (arg.Length > 0x400))
                {
                    args[i] = arg.Substring(0, 0x3fd) + "...";
                }
            }
            return string.Format(CultureInfo.CurrentCulture, format, args);
        }
    }
}