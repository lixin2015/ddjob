﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DdJob.Utilities
{
    public static class SqlExcuter
    {
        public static string ConnectionString = "server=.;database=MySchool(你的数据库名称);uid=sa(登录数据库时的名称);pwd=123(密码可为空)";
        public static void Excute(string sql)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            try
            {

                //创建SqlCommand的实例     
                SqlCommand cmd = conn.CreateCommand();
                //sql语句     
                cmd.CommandText = sql;
                //打开连接     
                conn.Open();
                //执行sql     
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                if (conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }

        }
    }
}
