﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace DdJob.Web.Extensions
{
    public class LowerCamelJsonResult: JsonResult
    {
        public new object Data { get; set; }

        public LowerCamelJsonResult()
        {         
        }

        public LowerCamelJsonResult(object data)
        {
            Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }
            //json对象命名小驼峰式转换
            var json = JsonConvert.SerializeObject(
                this.Data,
                Formatting.Indented,
                new JsonSerializerSettings { ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver() }
                );

            context.HttpContext.Response.Write(json);
        }
    }
}