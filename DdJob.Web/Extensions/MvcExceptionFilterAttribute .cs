﻿using System.Web.Mvc;
using Abp.UI;
using Abp.Web.Models;
using DdJob.Web.Models;

namespace DdJob.Web.Extensions
{
    public class MvcExceptionFilterAttribute : IExceptionFilter
    {
        public void OnException(ExceptionContext filterContext)
        {
            var invokeResult = new InvokeResult<object>();
            invokeResult.Error = new ErrorInfo();
            if (filterContext.Exception is UserFriendlyException)
            {
                invokeResult.Error.Message = filterContext.Exception.Message;
            }
            else
            {
                invokeResult.Error.Message = filterContext.Exception.Message + (filterContext.Exception.StackTrace ?? "");
                //记录日志
            }
            var jsonResult = new LowerCamelJsonResult
            {
                Data = invokeResult,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            filterContext.ExceptionHandled = true;
            filterContext.Result = jsonResult;
        }
    }
}
