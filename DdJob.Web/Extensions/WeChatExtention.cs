﻿namespace DdJob.Web.Extensions
{
    public class WeChatExtention
    {
        public static string GetUserOpenId(OAuthResponse response)
        {
            if (string.IsNullOrEmpty(response.Code))
                return "";

            var wxUser = new WeChatUserDto();
            wxUser = response.SetAuthCode(wxUser);
            wxUser.GetAccessToken();

            return wxUser.OpenId;
        }
    }
}