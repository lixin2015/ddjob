﻿using DdJob.Utilities;
using System;
using System.Collections.Generic;
using System.Web;

namespace DdJob.Web.Extensions
{
    public static class OAuth2
    {
        /// <summary>
        /// 得到微信用户基本信息
        /// </summary>
        /// <param name="user"></param>
        public static void GetWxUserInfo(this WeChatUserDto user)
        {
            //if (user.AccessToken.ExpiresDate <= DateTime.Now)
            //{
            //    RefreshToken(user);
            //}

            var contexts = new Dictionary<string, string>
            {
                {"access_token", user.AccessToken.Token},
                {"openid", user.OpenId},
                {"lang", "zh_CN"}
            };
            var result = OAuthOpenIdHelper.GetHttp(WeChatUrls.RequestUserInfoUrl, contexts);
            user.User = Serializer.FromJson<WxUserDto>(result);
        }

        //public static void GetAuthCode(this WeChatUser user)
        //{
        //    var redirectUrl = HttpUtility.UrlEncode(WeChatUrls.RedirectCodeUrl);
        //    var contexts = new Dictionary<string, string>
        //    {
        //        {"appid", WeChatSettings.AppId},
        //        {"redirect_uri", redirectUrl},
        //        {"response_type", "code"},
        //        {"scope", "snsapi_base"},
        //        {"state", "lms1234"}
        //    };
        //    var result = OAuthOpenIdHelper.GetHttp(WeChatUrls.RequestCodeUrl, contexts, "#wechat_redirect");
        //    var resultJson = Serializer.FromJson<OAuthResponse>(result);
        //    resultJson.SetAuthCode(user);
        //}

        /// <summary>
        /// 得到AccessToken
        /// </summary>
        /// <param name="user"></param>
        public static void GetAccessToken(this WeChatUserDto user)
        {
            var contexts = new Dictionary<string, string>
            {
                {"appid", ""},
                {"secret", ""},
                {"code", user.OAuthCode.Code},
                {"grant_type", "authorization_code"}
            };
            var result = OAuthOpenIdHelper.GetHttp(WeChatUrls.RequestTokenUrl, contexts);
            var resultJson = Serializer.FromJson<OAuthResponse>(result);
            resultJson.SetAccessTokenOrRefreshToken(user);
        }

        public static void SetHost()
        {
            if (!string.IsNullOrEmpty(WeChatUrls.Host)) return;

            var request = HttpContext.Current.Request;
            var originalStr = request.Url.OriginalString.Split('/');
            if (originalStr.Length > 0)
                WeChatUrls.Host = originalStr[0] + "//" + request.Url.Host;
            else
                WeChatUrls.Host = "http://" + request.Url.Host;
        }
    }
}