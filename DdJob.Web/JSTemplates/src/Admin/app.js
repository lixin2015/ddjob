﻿import Vue from 'vue'
import Element from 'element-ui'
import 'Common/theme/element-ui-theme.css'

import 'share.css'
import './app.css'

import { } from 'common'
import { } from 'common/filter'

import ddjob from 'ddjob'

ddjob.init()
window.ddjob = ddjob

Vue.use(Element)

import login from './Account/login'
import leftMenu from './Layout/leftMenu'
// import leftBar from './User/leftBar'
import userList from './User/userList'

new Vue({
    el: '#root-container',
    components: {
        login,
        leftMenu,
        // leftBar
        userList
    }
})