﻿import Vue from 'vue'

import vueSelect from './vueSelect'
import dotdotdot from './dotdotdot'
import videoLink from './videoLink'
import loading from './loading'
import slide from './slide'
import slick from './slick'
import pager from './pager'
import vueDatepicker from './vueDatepicker'
import starRadio from './starRadio'

const iview = {
    vueSelect,
    dotdotdot,
    videoLink,
    loading,
    slide,
    slick,
    pager,
    vueDatepicker,
    starRadio
}

Object.keys(iview).forEach((key) => {
    Vue.component(key, iview[key])
})

export default {}