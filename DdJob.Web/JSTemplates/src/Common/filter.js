import Vue from 'vue'
import accounting from 'accounting'

Vue.filter('currency', function (val) {
    return accounting.formatMoney(val)
})

Vue.filter('currency2', function (val) {
    return accounting.formatMoney(val, {precision: 2})
})

Vue.filter('lower', function (val) {
    return val.toLowerCase()
})

Vue.filter('number', function (val) {
    return accounting.formatNumber(val)
})

export default {}