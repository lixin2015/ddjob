var scriptsLoaded = false
var clientLoaded = false

window.$.getScript('https://apis.google.com/js/client.js', function () {
    (function checkIfLoaded () {
        if (window.gapi.client) {
            scriptsLoaded = true
            window.gapi.client.setApiKey(window.$('#hidGoogleApiKey').val())
            window.gapi.client.load('urlshortener', 'v1', function () {
                clientLoaded = true
            })
        } else window.setTimeout(checkIfLoaded, 10)
    })()
})

var urlShortener = function (options) {
    var settings = {}
    // var data = {}
    window.$.extend(settings, {}, options)
    var checkScriptsAndClientLoaded = function () {
        if (scriptsLoaded && clientLoaded) {
            if (settings.longUrl !== undefined) {
                longToShort(settings)
            } else if (settings.shortUrl !== undefined) {
                shortUrlInfo(settings)
            }
        } else {
            window.setTimeout(checkScriptsAndClientLoaded, 10)
        }
    }
    checkScriptsAndClientLoaded()

    function longToShort (s) {
        var data = {
            'longUrl': s.longUrl
        }
        var request = window.gapi.client.urlshortener.url.insert({
            'resource': data
        })
        request.execute(function (response) {
            if (response.id != null) {
                if (s.success) {
                    s.success.call(this, response.id)
                }
            } else {
                if (s.error) {
                    s.error.call(this, response.error)
                }
            }
        })
    }

    function shortUrlInfo (s) {
        var data = {
            'shortUrl': s.shortUrl,
            'projection': s.projection
        }
        var request = window.gapi.client.urlshortener.url.get(data)
        request.execute(function (response) {
            if (response.longUrl != null) {
                if (s.success) {
                    if (s.projection === undefined) s.success.call(this, response.longUrl)
                    else s.success.call(this, response)
                }
            } else {
                if (s.error) {
                    s.error.call(this, response.error)
                }
            }
        })
    }
}

export default urlShortener