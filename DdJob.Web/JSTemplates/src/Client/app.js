﻿import Vue from 'vue'
import Element from 'element-ui'
import 'Common/theme/element-ui-theme.css'

import 'share.css'

// import './app.css'

import { } from 'common'
import { } from 'common/filter'

import ddjob from 'ddjob'

ddjob.init()
window.ddjob = ddjob

Vue.use(Element)

import home from './JSHome/home'
import example from './JSHome/example'

new Vue({
    el: '#root-container',
    components: {
        home,
        example
    }
})