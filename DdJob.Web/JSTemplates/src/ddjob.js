﻿import accounting from 'accounting'

var $ = window.jQuery
var ddjob = {}
ddjob.scrollbarWidth = (function () {
    var inner = document.createElement('div')
    inner.style.width = '100%'
    inner.style.height = '200px'

    var outer = document.createElement('div')
    var outerStyle = outer.style

    outerStyle.position = 'absolute'
    outerStyle.top = 0
    outerStyle.left = 0
    outerStyle.pointerEvents = 'none'
    outerStyle.visibility = 'hidden'
    outerStyle.width = '200px'
    outerStyle.height = '150px'
    outerStyle.overflow = 'hidden'

    outer.appendChild(inner)

    document.body.appendChild(outer)

    var widthContained = inner.offsetWidth
    outer.style.overflow = 'scroll'
    var widthScroll = inner.offsetWidth

    if (widthContained === widthScroll) {
        widthScroll = outer.clientWidth
    }

    document.body.removeChild(outer)
    return widthContained - widthScroll
}())

ddjob.pageSelfAdaptation = function () {
    // self adapt the page content height
    var minHeight = $(window).height()
    // $('#root-container .fillcontain').css('min-height', minHeight + 'px')
    $('#root-container .fillcontain').each(function () {
        $(this).css('min-height', minHeight + 'px')
    })
}

ddjob.init = function () {
    // set min-height
    ddjob.pageSelfAdaptation()

    window.onresize = function () {
        ddjob.pageSelfAdaptation()
    }

    accounting.settings = {
        currency: {
            symbol: '$',
            format: '%s%v',
            decimal: '.',
            thousand: ',',
            precision: 0
        },
        number: {
            precision: 0,
            thousand: ',',
            decimal: '.'
        }
    }
}

ddjob.clearCookies = function () {
    $.toursManager.removeCookie()
    $.toursManager.clearFromChristmas()
    $.toursManager.setShowToursOnce(false)
    return true
}

// alert message on mid screen
ddjob.showMessage = function (msg) {
    var $html = '<div class="modal-content modal-backdrop-custom" style="width:96%;max-width:456px;" >' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + msg + '</div>'
    $('body').append($html)
    $('.close').click(function () { $('.modal-backdrop-custom').remove() })

    setTimeout(function () {
        $('.modal-backdrop-custom').fadeOut(200, function () {
            $('.modal-backdrop-custom').remove()
        })
    }, 5000)
}

ddjob.session = (function () {
    var temp = window.ddjobSession
    window.ddjobSession = undefined
    $('#ddjob-session').remove()
    return temp
}())

ddjob.getQueryString = function (name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)')
    var r = window.location.search.toLowerCase().substr(1).match(reg)
    if (r != null) return unescape(r[2])
    return null
}
ddjob.setQueryString = function (url, name, value) {
    if (url.indexOf('?') > 0) { return ddjob.format('{0}&{1}={2}', url, name, value) } else { return ddjob.format('{0}?{1}={2}', url, name, value) }
}
ddjob.format = function () {
    if (arguments.length === 0) { return null }
    var str = arguments[0]
    for (var i = 1; i < arguments.length; i++) {
        var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm')
        str = str.replace(re, arguments[i])
    }
    return str
}
ddjob.fridenlyErroMsg = "Something's not quite right... please try again soon or <a href='/contact-us'>let us know</a> what happened."

// get form json data
ddjob.getJsonData = function (wrapperSelector) {
    var manipulationRcheckableType = /^(?:checkbox|radio)$/i
    var rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i
    var rsubmittable = /^(?:input|select|textarea|keygen)/i
    var $wrapper = $(wrapperSelector)
    var result = {}
    $wrapper.find('*').filter(function () {
        var type = this.type
        if (type === 'hidden' && this.name) {
            if ($wrapper.find(":checkbox[name='" + this.name + "']").size()) {
                return false
            }
        }
        return this.name && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !manipulationRcheckableType.test(type))
    }).each(function (i, elem) {
        var $field = $(elem)
        if (elem.type === 'checkbox') {
            result[elem.name] = $field.val() === 'true' ? elem.checked : $field.val()
        } else {
            var val = $field.val()
            if (val != null) {
                result[elem.name] = $field.val()
            }
        }
    })
    return result
}

// process post back result
ddjob.processAjaxResult = function (result) {
    var showMsg = function (ajaxResult, callback) {
        if (ajaxResult.Message) {
            if (ajaxResult.Success) {
                ddjob.showSuccess(ajaxResult.Message) // 2. success
            } else {
                if (!ajaxResult.Success && ajaxResult.Type === 0) {
                    ddjob.showInfo(ajaxResult.Message) // 2. info
                } else {
                    if (!ajaxResult.Success && ajaxResult.Type !== 0) {
                        ddjob.showError(ajaxResult.Message) // 3. error
                    }
                }
            }
            if (callback) {
                callback && callback()
            }
        }
    }
    if (result) {
        // 1. Navigation
        if (result.Url && !result.Message) {
            window.location.href = result.Url
        } else if (!result.Url && result.Message) {
            // 2. Show message
            showMsg(result)
        } else if (result.Url && result.Message) {
            // 3. Show message and navigation
            showMsg(result, function () {
                window.location.href = result.Url
            })
        }
    }
    return result && result.Success
}

// ajax post request
ddjob.ajaxPostMvc = function (options) {
    globalAjaxQueue.add()

    var defaultOption = {
        dataType: 'JSON',
        type: 'POST',
        cache: false,
        closeLoading: true,
        showBusy: true,
        error: function () {
            if (options.sender) {
                options.sender.loading = false
            }
        }
    }
    options = $.extend({}, defaultOption, options)
    options.url = ddjob.setQueryString(options.url, 'aliaspath', encodeURIComponent(ddjob.session.nodeAliasPath))
    options.beforeSend = function (request) {
        // add content type by barry
        request.setRequestHeader('Content-Type', 'application/json')

        if (options.sender) {
            options.sender.loading = true
        } else if (options.showBusy) {
            ddjob.loading()
        }
    }
    var success = options.success
    if ($.isFunction(success)) {
        options.success = function (result) {
            if (options.sender && result && ((result.Success && options.closeLoading) || !result.Success)) {
                options.sender.loading = false
            }
            success(result)
        }
    } else {
        options.success = function (result) {
            if (options.sender && result && ((result.Success && options.closeLoading) || !result.Success)) {
                options.sender.loading = false
            }
            ddjob.processAjaxResult(result)
        }
    }
    var complete = options.complete
    options.complete = function () {
        globalAjaxQueue.remove()
        // ddjob.colseloading()
        if ($.isFunction(complete)) {
            complete()
        }
    }
    var jsonData = options.data || {}
    options.data = JSON.stringify(jsonData)
    $.ajax(options)
}

// ajax get request
ddjob.ajaxGetMvc = function (url, successCallback, errorCallback) {
    $.ajax({
        type: 'GET',
        url: url,
        data: { aliaspath: ddjob.session.nodeAliasPath },
        success: function (result) {
            if (successCallback !== undefined && successCallback != null) {
                successCallback(result)
            }
        },
        error: function (err) {
            if (errorCallback !== undefined && errorCallback != null) {
                errorCallback(err)
            } else {
                console.log(err)
                ddjob.showError(ddjob.fridenlyErroMsg)
            }
        },
        beforeSend: function () {
            ddjob.loading()
        },
        complete: function () {
            ddjob.closeloading()
        }
    })
}

// loding
ddjob.loading = function () {
    $('#ajax-busy').show()
}

// close loading
ddjob.closeloading = function () {
    $('#ajax-busy').hide()
}

// show success message
ddjob.showSuccess = function (info) {
    ddjob.showMessage(info, 'success')
}

// show error message
ddjob.showError = function (info) {
    ddjob.showMessage(info, 'error')
}

// show info message
ddjob.showInfo = function (info) {
    ddjob.showMessage(info, 'info')
}

ddjob.showMessageWithBtn = function (info, type) {
    var str = 'info'
    if (type === 'success' || type === 'error') {
        str = type
    }
    var $html = '<div class="modal-content modal-backdrop-custom ' + str + '" style="width:96%;max-width:456px;" >' +
        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + info + '</div>'
    $('body').append($html)
    $('.close').click(function () { $('.modal-backdrop-custom').remove() })
}

ddjob.showMessage = function (info, type) {
    var $msg
    if (type === 'success') {
        $msg = $(successHtml(info))
    } else {
        if (type === 'error') {
            $msg = $(errorHtml(info))
        } else {
            $msg = $(infoHtml(info))
        }
    }
    $('body').append($msg)
    $msg.fadeIn(200)
    setTimeout(function () {
        $('.modal-backdrop-custom').fadeOut(400, function () {
            $('.modal-backdrop-custom').remove()
        })
    }, 3000)
}

var successHtml = function (msg) {
    return '<div id="toast" class="modal-content modal-backdrop-custom success">' + msg + '</div>'
}

var errorHtml = function (msg) {
    return '<div id="toast" class="modal-content modal-backdrop-custom error">' + msg + '</div>'
}

var infoHtml = function (msg) {
    return '<div id="toast" class="modal-content modal-backdrop-custom info">' + msg + '</div>'
}

ddjob.clearInfo = function () {
    ddjob.showMessage()
}

ddjob.setCookie = function (name, value, expiredays) {
    if (!expiredays) {
        document.cookie = name + '=' + escape(value) + ';path=/'
    } else {
        var exdate = new Date()
        exdate.setDate(new Date().getDate() + expiredays)
        document.cookie = name + '=' + escape(value) + ';expires=' + exdate.toString() + ';path=/'
    }
}

ddjob.getCookie = function (name) {
    if (document.cookie.length === 0) {
        return ''
    }

    var start = document.cookie.indexOf(name + '=')
    if (start >= 0) {
        start = start + name.length + 1
        var end = document.cookie.indexOf(';', start)
        if (end === -1) {
            end = document.cookie.length
        }
        return unescape(document.cookie.substring(start, end))
    }
    return ''
}

ddjob.removeCookie = function (name) {
    ddjob.setCookie(name, null, -1)
}

ddjob.errorcallback = function (error) {
    if (error) {
        ddjob.showError(error)
    }
}

ddjob.setGTM = function (tag) {
    if (typeof window.dataLayer !== 'undefined') {
        window.dataLayer.push(tag)
    }
}

ddjob.updateCartItemsCount = function (showMessage) {
    if (window.updateCartItemsCount) window.updateCartItemsCount(showMessage)
}

ddjob.scrollTo = function (hash) {
    setTimeout(function () {
        if (!hash) return
        var $el = window.$(hash)
        if ($el) {
            console.log(hash)
            $('html,body').animate({ scrollTop: $el.offset().top }, 1000)
        }
    }, 100)
}

ddjob.initScrollAnchor = function () {
    var anchors = window.$('.scroll-anchor')
    anchors.each(function () {
        var href = $(this).attr('href')
        if (href) {
            $(this).data('href', href)
            $(this).removeAttr('href')
        }
    })
    anchors.unbind('click')
    anchors.click(function (e) {
        e.preventDefault()
        var $this = $(this)
        ddjob.scrollTo($this.data('href'))
    })
}

ddjob.animateShow = function () {
    $('.anim').each(function (i) {
        $(this).css('transition-delay', (i * 0.05) + 's')
        $(this).addClass('anim-done')
    })
}
var ajaxLoadHanders = []
ddjob.ajaxLoaded = function (func) {
    ajaxLoadHanders.push(func)
}
ddjob.ajaxLoaded(function () {
    setTimeout(function () {
        ddjob.scrollTo(window.location.hash)
        ddjob.animateShow()
        ddjob.initScrollAnchor()
    }, 200)
})

ddjob.ajaxQueue = function (callback) {
    return {
        queue: [],
        add: function () {
            if (typeof this.queue === 'undefined') {
                this.queue = []
            }
            var index = this.queue.length
            var id = Math.floor(Math.random() * 1000000)
            this.queue[index] = id
        },
        remove: function () {
            var index = this.queue.length - 1
            this.queue.splice(index, 1)
            var size = this.queue.length
            if (size === 0) {
                if (typeof callback === 'function') {
                    callback()
                }
            }
        }
    }
}
var globalAjaxQueue = ddjob.ajaxQueue(function () {
    ddjob.closeloading()
    ajaxLoadHanders.forEach(function (o) {
        if (typeof o === 'function') {
            o()
        }
    })
    ajaxLoadHanders = []
})

ddjob.incrementScore = function (sec) {
    window.$('.incrementscore').each(function () {
        var self = window.$(this)
        var score = self.data('score')
        setTimeout(function () {
            increment(self, 0, score)
        }, sec)
    })
    function increment (elem, StartVal, finalVal) {
        var currVal = parseInt(StartVal, 10)
        var percent = elem.data('percent')
        if (currVal < finalVal) {
            currVal++
            elem.html(currVal + (percent ? '%' : ''))

            setTimeout(function () {
                increment(elem, currVal, finalVal)
            }, 2000 / (finalVal > 0 ? finalVal : 1))
        } else {
            elem.html(currVal + (percent ? '%' : ''))
        }
    }
}

ddjob.showLoginAlert = function () {
    ddjob.showMessageWithBtn('<p>You need to <a href="/home?ReturnURL=/" style="color:#13a89e">login</a> or <a href="/registration" style="color:#13a89e">register for free</a> to complete a report</p>')
}

ddjob.setCustomTableTitle = function (table, columns) {
    window.$(table).find('.ivu-table-body tbody tr').each(function (index, row) {
        window.$(row).find('td').each(function (index, col) {
            window.$(col).attr('data-title', columns[index].title)
        })
    })
}
export default ddjob