﻿$(function () {
    //grid
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/role/GetRolesByPage"
                }
            },
            pageSize: 20
        }
    });

    //grid search
    $(".q-condition .btn-search").click(function () {
        DdJob.refresh();
    });

    var _$modal = $('#CreateModal');
    var _$baseform = _$modal.find('form.itemBaseForm');

    _$baseform.validate();

    _$baseform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$baseform.valid()) {
            return;
        }
        var model = _$baseform.serializeFormToObject(); //serializeFormToObject is defined in main.js          
        abp.ui.setBusy(_$modal);
        var data = JSON.stringify(model);
        $.ajax({
            type: "post",
            url: "/api/role/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    abp.notify.success("基本信息保存成功", "通知");
                    abp.ui.clearBusy(_$modal);
                    if (model.Id && model.Id > 0)
                        _$modal.modal('hide');
                    $("input[name=Id]").val(result.data);
                    $("form .btns-group").removeClass("hide");
                    _$modal.find('a#tab2').trigger("click");
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });

    var _$permissionform = _$modal.find('form.itemPermissionForm');
    _$permissionform.validate();

    _$permissionform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();
        var id = _$permissionform.find("input[name=Id]").val();
        if (!id) {
            abp.notify.error("请先保存基本信息", "通知");
            return;
        }
        var permissionStr = "";
        $(".treeview-permissions input[type=checkbox]:checked").each(function () {
            permissionStr = permissionStr + $(this).val() + "|";
        });
        abp.ui.setBusy(_$modal);
        var data = JSON.stringify({ id: id, permissions: permissionStr });
        $.ajax({
            type: "post",
            url: "/api/role/UpdatePermissions",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    abp.notify.success("保存成功", "通知");
                    _$modal.modal('hide');
                    abp.ui.clearBusy(_$modal);
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    var _allPermissions = null;
    function getAllPermissions() {
        if (!_allPermissions) {
            abp.ui.setBusy(_$modal);
            $.ajax({
                type: "get",
                url: "/api/role/GetAllPermissions",
                contentType: "application/json",
                data: null,
                success: function (result) {
                    if (result.success) {
                        _allPermissions = result.data;
                        var str = buildPermission(_allPermissions, 1);
                        $(".modal .treeview-permissions").html(str);
                        $(".treeview-permissions input[type=checkbox][name=1]").prop("checked", true);
                    }
                    abp.ui.clearBusy(_$modal);
                },
                error: function (err) {
                    abp.ui.clearBusy(_$modal);
                }
            });
        } else {
            var str = buildPermission(_allPermissions, 1);
            $(".modal .treeview-permissions").html(str);
            $(".treeview-permissions input[type=checkbox][name=1]").prop("checked", true);
        }
    };

    function getAllGrantedPermissions(uid) {
        abp.ui.setBusy(_$modal);
        $.ajax({
            type: "get",
            url: "/api/role/GetGrantedPermissions?id=" + uid,
            contentType: "application/json",
            data: null,
            success: function (result) {
                if (result.success) {
                    $(".treeview-permissions input[type=checkbox]").each(function () {
                        var perArry = result.data.split('|');
                        if (result.data && perArry.indexOf($(this).val()) >= 0) {
                            $(this).prop("checked", true);
                        } else {
                            $(this).prop("checked", false);
                        }
                    });
                    $(".treeview-permissions input[type=checkbox][name=1]").prop("checked", true);
                }
                abp.ui.clearBusy(_$modal);
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
            }
        });
    };

    var idIndex = 1;
    buildPermission = function (input, level) {
        var idname = "chk-" + level +"-"+ idIndex;
        var str = '<label><input type="checkbox" class="chkbox" id="' + idname + '" name="' + level + '" value="' + input.name + '" > ' + '<label for="' + idname + '"></label>'+ input.displayName+'</label>';
        if (input.children && input.children.length > 0) {
            str += '<ul>';
            var childs = "";
            for (var i = 0; i < input.children.length; i++) {
                str += '<li>' + buildPermission(input.children[i], level + 1) + '</li>';
            }
            str += "</ul>";
        }
        idIndex++;
        return str;
    };

    $(".treeview-permissions").on("click", "input[type=checkbox]", function () {
        var $this = $(this);
        var level = $this.attr("name");
        $this.parent().parent().find("input[type=checkbox]").each(function () {
            $(this).prop("checked", $this.is(":checked"));
        });
        var parents = $this.parent().parent().parent();
        var all = parents.find("input[type=checkbox][name=" + level + "]");
        var notCheckedLength = all.not("input:checked").length;
        var checkedLength = all.length - notCheckedLength;
        if (checkedLength > 0) {
            $this.parent().parent().parent().prev().find("input[type=checkbox]").prop("checked", true);
        }
        else
            $this.parent().parent().parent().prev().find("input[type=checkbox]").prop("checked", false);

        $(".treeview-permissions input[type=checkbox][name=1]").prop("checked", true);
    });

    $(".page-content").on("click", ".btn-showmodal", function () {
        var uid = $(this).data("uid");
        var _$modal = $('#CreateModal');
        _$modal.modal('show');

        getAllPermissions();

        abp.ui.setBusy(_$modal);
        var eleName = $("input[name=Name]");
        var eleDisplayName = $("input[name=DisplayName]");
        $("input[name=Id]").val(uid);
        _$modal.find('a#tab1').trigger("click");
        if (uid && uid > 0) {
            $("#CreateModal .modal-title").html("编辑");
            $.ajax({
                type: "get",
                url: "/api/role/getbyid?id=" + uid,
                contentType: "application/json",
                data: null,
                success: function (result) {
                    if (result.success) {
                        eleName.val(result.data.name).attr("disabled", result.data.isStatic);
                        eleDisplayName.val(result.data.displayName).attr("disabled", result.data.isStatic);
                        //if (result.data.isStatic)
                        //$(".modal button[type=submit]").attr("disabled", result.data.isStatic);//todo
                    }
                    abp.ui.clearBusy(_$modal);

                    getAllGrantedPermissions(uid);
                },
                error: function (err) {
                    abp.ui.clearBusy(_$modal);
                }
            });
        } else {
            $("#CreateModal .modal-title").html("新建");
            eleName.val("").attr("disabled", false);
            eleDisplayName.val("").attr("disabled", false);
            $("form .btns-group").addClass("hide");
            abp.ui.clearBusy(_$modal);
        }
    });

    $(".page-content").on("click", ".btn-delete", function () {
        var uid = $(this).data("uid");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteUser();
                }
            });

        function deleteUser() {
            var data = { id: uid };
            abp.ui.setBusy($('body'),
                $.ajax({
                    type: "post",
                    url: "/api/role/SingleDelete",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.success) {
                            abp.notify.success("删除成功", "通知");
                            //location.reload(true);
                            DdJob.refresh();
                        } else {
                            abp.notify.error("删除失败", "通知");
                        }
                    },
                    error: function (err) {
                        abp.notify.error(err.responseJSON.error.message, "通知");
                    }
                }));
        };
    });
});