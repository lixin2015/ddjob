﻿

$(function () {
    //grid
    getAllRoles();
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/user/GetUsersByPage"
                }
            },
            pageSize: 20
        }
    });
    //grid search
    $(".q-condition .btn-search").click(function () {
        var grid = DdJob.kd.grid($(".q-result .q-grid"));
        if (grid) {
            grid.dataSource.page(1);
        }
    });
    var _$modal = $('#CreateUserModal');
    var _$baseform = _$modal.find('form.itemBaseForm');
    _$baseform.validate();
    _$baseform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();
        if (!_$baseform.valid()) {
            return;
        }
        var model = _$baseform.serializeFormToObject();
        if ($("input[name=Id]").val() == 0) {
            model.UserType = $("#UserType").val();
        } else {
            model.UserType = $("#UserTypeHidden").val();
        }
        if (model.userType == 1 && $("input[name=Id]").val() == 0) {
            abp.notify.error("请勿添加商城用户", "通知");
            return false;
        }
        var data = JSON.stringify(model);
        $.ajax({
            type: "post",
            url: "/api/user/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                if (result.success) {
                    abp.notify.success("基本信息保存成功", "通知");
                    abp.ui.clearBusy();
                    if (model.Id && model.Id > 0)
                        _$modal.modal('hide');
                    $("input[name=Id]").val(result.data);
                    $("form .btns-group").removeClass("hide");
                    _$modal.find('a#tab2').trigger("click");
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy();
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });
    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });
    var _$rolesform = _$modal.find('form.itemRolesForm');
    _$rolesform.validate();
    _$rolesform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();
        var id = _$rolesform.find("input[name=Id]").val();
        if (!id) {
            abp.notify.error("请先保存基本信息", "通知");
            return;
        }
        var roleStr = "";
        $(".treeview-roles input[type=checkbox]:checked").each(function () {
            roleStr = roleStr + $(this).val() + "|";
        });
        abp.ui.setBusy(_$modal);
        var data = JSON.stringify({ userid: id, roleNames: roleStr });
        $.ajax({
            type: "post",
            url: "/api/user/UpdateUserRoles",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    abp.notify.success("保存成功", "通知");
                    _$modal.modal('hide');
                    abp.ui.clearBusy(_$modal);
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    var _allRoles = null;
    function getAllRoles() {
        if (!_allRoles) {
            $.ajax({
                type: "get",
                url: "/api/role/GetAll",
                contentType: "application/json",
                data: null,
                beforeSend: function () { abp.ui.setBusy(); },
                success: function (result) {
                    abp.ui.clearBusy();
                    if (result.success) {
                        if (result.data.items && result.data.items.length > 0) {
                            _allRoles = result.data.items;
                            var str = "<ul>";
                            for (var i = 0; i < _allRoles.length; i++) {
                                var idname = "chk" + i;
                                str += '<li><label class="chk-wrap"><input type="checkbox" id="' + idname + '" class="chkbox roleck" value="' + _allRoles[i].name + '" > <label for="' + idname + '"></label> ' + _allRoles[i].displayName + " </label></li>";
                            }
                            str += "</ul>";
                            $(".modal .treeview-roles").html(str);
                        }
                    }
                },
                error: function (err) {
                    abp.ui.clearBusy();
                }
            });
        } else {
            $(".treeview-roles input[type=checkbox]").prop("checked", false);
        }
    };

    function getAllGrantedRoles(id) {
        $.ajax({
            type: "get",
            url: "/api/user/GetUserRoles?id=" + id,
            contentType: "application/json",
            data: null,
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                abp.ui.setBusy();
                if (result.success) {
                    if (result.data) {
                        $(".treeview-roles input[type=checkbox]").each(function () {
                            if (result.data.indexOf($(this).val()) >= 0) {
                                $(this).prop("checked", true);
                            } else {
                                $(this).prop("checked", false);
                            }
                        });
                    }
                }
                abp.ui.clearBusy();
            },
            error: function (err) {
                abp.ui.setBusy();
                abp.ui.clearBusy();
            }
        });
    };

    $(".page-content").on("click", ".btn-showmodal", function () {
        var id = $(this).data("id");
        var customer = $("#Customer").val();
        var customerDes = $("#Customer").data("des");
        if (id == 0) {
            $("#UserType option[value=" + customer + "]").remove();
            $("#UserType").removeAttr("disabled");

        } else {
            $("#UserType option[value=" + customer + "]").remove();
            $("#UserType").append("<option value='" + customer + "'>" + customerDes + "</option>");
            $("#UserType").attr("disabled", true);
        }
        var _$modal = $('#CreateUserModal');
        _$modal.modal('show');
        ResetData();
        $("input[name=Id]").val(id);
        _$modal.find('a#tab1').trigger("click");
        if (id && id > 0) {
            getAllGrantedRoles(id);
            $("#CreateUserModal .modal-title span:last-child").html("编辑用户");
            $("form .btns-group").removeClass("hide");
            $.ajax({
                type: "get",
                url: "/api/user/GetUserEditDtoById?id=" + id,
                contentType: "application/json",
                data: null,
                beforeSend: function () { abp.ui.setBusy(); },
                error: function (err) {
                    abp.ui.clearBusy();
                    abp.notify.error(err.responseJSON.error.message, "通知");
                },
                success: function (result) {
                    abp.ui.clearBusy();
                    if (result.success) {
                        $("input[name=UserName]").val(result.data.userNameText);
                        $("input[name=Name]").val(result.data.nameText);
                        $("#PassWordDiv").hide();
                        $("input[name=EmailAddress]").val(result.data.emailAddressText);
                        $("input[name=PhoneNumber]").val(result.data.phoneNumberText);
                        $("input[name=IsActive]").attr("checked", result.data.isActive);

                        $("#UserType").val(result.data.userType);
                        $("#UserType").trigger("change");
                        if (result.data.userType == 20) {
                            $("#BankId").val(result.data.clientManagerBankId);
                            $("#BankId").trigger("change");
                            $("#DepartmentId").val(result.data.clientManagerDepartmentId);
                            $("#CollectionType").val(result.data.clientManagerCollectionType);
                            //$("#StaffNo").val(result.data.clientManagerStaffNo);
                        }
                        else if (result.data.userType == 40) {
                            $("#BusinessId").val(result.data.businessId);
                            $("#BusinessId").trigger("change");
                            $("#IsAdmin").prop("checked", result.data.isAdmin);
                            if (result.data.isAdmin == true) {
                                $("#BranchIdDiv").hide();
                            } else {
                                $("#BranchIdDiv").show();
                            }
                            $("#BranchId").val(result.data.branchId);
                        }
                        if (id == 0) {
                            $("#UserTypeHidden").val("");
                            $("#UserTypeHidden").attr("disabled", true);
                            $("#UserType").removeAttr("disabled");
                        } else {
                            $("#UserType").attr("disabled", true);
                            $("#UserTypeHidden").removeAttr("disabled");
                            $("#UserTypeHidden").val(result.data.userType);
                        }

                    }
                    abp.ui.clearBusy();
                }
            });

        } else {
            $("#CreateUserModal .modal-title span:last-child").html("新建用户");
            abp.ui.clearBusy();
        }
    });
    $(".page-content").on("click", ".btn-delete", function () {
        var id = $(this).data("id");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteUser(id);
                }
            });
    });
    //用户类型
    $("body").on("change", "#UserType", function () {
        var data = $(this).val();
        if (data == 0) {
            $(".bank").addClass("hidden");
            $(".business").addClass("hidden");
        } else if (data == 1) {
            $(".bank").addClass("hidden");
            $(".business").addClass("hidden");
        } else if (data == 10) {
            $(".bank").addClass("hidden");
            $(".business").addClass("hidden");
        } else if (data == 20) {
            $(".bank").removeClass("hidden");
            $(".business").addClass("hidden");
        } else if (data == 30) {
            $(".bank").addClass("hidden");
            $(".business").addClass("hidden");
        } else if (data == 40) {
            $(".business").removeClass("hidden");
            $(".bank").addClass("hidden");
        }

    });
    $("body").on("change", "#BankId", function () {
        var data = $(this).val();
        $.ajax({
            type: "get",
            url: "/api/department/GetDepartmentSimpleteDtoByBankId?id=" + data,
            contentType: "application/json",
            async: false,
            data: null,
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                abp.ui.clearBusy();
                if (result.success) {
                    if (result.data.length == 0) {
                        str = '<option value="0">--请选择--</option>';
                        $("#DepartmentId").html(str);
                    } else {
                        str = '<option value="0">--请选择--</option>';
                        for (var i = 0; i < result.data.length; i++) {
                            str += '<option value="' + result.data[i].id + '">' + result.data[i].name + '</option>';
                        }
                        $("#DepartmentId").html(str);
                    }
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });
    $("body").on("change", "#BusinessId", function () {
        var data = $(this).val();
        $.ajax({
            type: "get",
            url: "/api/branch/GetBranchSimpleDtoByBusinessId?id=" + data,
            contentType: "application/json",
            async: false,
            data: null,
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                abp.ui.clearBusy();
                if (result.success) {
                    if (result.data.length == 0) {
                        str = '<option value="0">--请选择--</option>';
                        $("#BranchId").html(str);
                    } else {
                        str = '<option value="0">--请选择--</option>';
                        for (var i = 0; i < result.data.length; i++) {
                            str += '<option value="' + result.data[i].id + '">' + result.data[i].name + '</option>';
                        }
                        $("#BranchId").html(str);
                    }
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });
    $("body").on("click", "#IsAdmin", function () {
        if ($(this).prop("checked") == false) {
            $("#BranchIdDiv").show();
        } else {
            $("#BranchIdDiv").hide();
        }
    });
    function deleteUser(id) {
        var data = { id: id };
        $.ajax({
            type: "post",
            url: "/api/user/Delete",
            contentType: "application/json",
            data: JSON.stringify(data),
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                abp.ui.clearBusy();
                if (result.success) {
                    abp.notify.success("删除成功", "通知");
                    DdJob.refresh();
                } else {
                    abp.notify.error("删除失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });

    };
    function ResetData() {
        $("input[name=UserName]").val("");
        $("#PassWordDiv").show();
        $("input[name=Name]").val("");
        $("input[name=EmailAddress]").val("");
        $("input[name=PhoneNumber]").val("");
        $("input[name=IsActive]").attr("checked", true);
        $("#UserType").val(0);
        $("#UserType").trigger("change");
        $("#BankId").val(0);
        $("#BankId").trigger("change");
        $("#DepartmentId").val(0);
        $("#CollectionType").val(0);
        $("#BusinessId").val(0);
        $("#BusinessId").trigger("change");
        $("#IsAdmin").attr("checked", true);
        $("#BranchId").val(0);
        $(".roleck").attr("checked", false);
    }


    //修改密码
    _$changemodel = $('#ChangePassWord');

    $(".page-content").on("click", ".btn-changepassword", function () {
        $("#PwdUserId").val($(this).data("id"));
        $("#NewPassword").val("");
        $("NewPasswordAgain").val("");
        _$changemodel.modal('show');
    });

    _$changemodel.find('#PwdSubmit').click(function (e) {
        var pwd = $("#NewPassword").val();
        var pwdAgain = $("#NewPasswordAgain").val();
        if (pwd.length < 6) {
            abp.notify.error("密码长度必须大于等于6位", "通知");
            return false;
        }
        if (pwd != pwdAgain) {
            abp.notify.error("两次输入不一致", "通知");
            return false;
        }
        var data = { UserId: $("#PwdUserId").val(), Password: pwd }
        $.ajax({
            type: "post",
            url: "/api/user/ChangePasswordCommon",
            contentType: "application/json",
            data: JSON.stringify(data),
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                abp.ui.clearBusy();
                if (result.success) {
                    _$changemodel.modal('hide');
                    abp.notify.success("密码修改成功", "通知");
                } else {
                    abp.notify.error("密码修改失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });

    });
});