﻿$(function () {
    //grid
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/customsetting/getCustomSettingByPage"
                }
            },
            pageSize: 20
        }
    });

    //grid search
    $(".q-condition .btn-search").click(function () {
        var grid = DdJob.kd.grid($(".q-result .q-grid"));
        if (grid) {
            grid.dataSource.page(1);
        }
    });

    var _$modal = $('#CreateDataModal');
    var _$baseform = _$modal.find('form.itemBaseForm');

    _$baseform.validate();

    _$baseform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$baseform.valid()) {
            return;
        }
        var model = _$baseform.serializeFormToObject();
        abp.ui.setBusy(_$modal);
        var data = JSON.stringify(model);
        $.ajax({
            type: "post",
            url: "/api/customsetting/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    _$modal.modal('hide');
                    abp.notify.success("基本信息保存成功", "通知");
                    abp.ui.clearBusy(_$modal);
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });
    $(".page-content").on("click", ".btn-showmodal", function () {
        var dataid = $(this).data("dataid");
        var _$modal = $('#CreateDataModal');
        //清空数据
        $("input[name=Id]").val(0);
        $("input[name=Key]").val("");
        $("input[name=Name]").val("");
        $("input[name=Value]").val("");
        $(".modal-dialog select[name=GroupType]").val("0");
        //编辑
        if (dataid && dataid > 0) {
            $.ajax({
                type: "get",
                url: "/api/customsetting/GetEntityById?id=" + dataid,
                contentType: "application/json",
                data: null,
                beforeSend: function () {
                    abp.ui.setBusy($('body'));
                },
                success: function (result) {
                    if (result.success) {
                        $(".modal-dialog input[name=Id]").val(result.data.id);
                        $(".modal-dialog input[name=Key]").val(result.data.key);
                        $(".modal-dialog input[name=Name]").val(result.data.name);
                        $(".modal-dialog input[name=Value]").val(result.data.value);
                        $(".modal-dialog select[name=GroupType]").val(result.data.groupType);
                        _$modal.modal('show');
                    }
                    abp.ui.clearBusy($('body'));

                },
                error: function (err) {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error(err.responseJSON.error.message, "通知");
                }
            });
        }
    });

    $(".page-content").on("click", ".btn-delete", function () {
        var dataid = $(this).data("dataid");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteCustomSetting();
                }
            });

        function deleteCustomSetting() {
            var data = { id: dataid };
            abp.ui.setBusy($('body'),
                $.ajax({
                    type: "post",
                    url: "/api/CustomSetting/Delete",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.success) {
                            abp.notify.success("删除成功", "通知");
                            DdJob.refresh();
                        } else {
                            abp.notify.error("删除失败", "通知");
                        }
                    },
                    error: function (err) {
                        abp.notify.error(err.responseJSON.error.message, "通知");
                    }
                }));
        };
    });

});