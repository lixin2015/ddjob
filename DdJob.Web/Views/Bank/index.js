﻿$(function () {
    //grid
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/bank/GetBanksByPage"
                }
            },
            pageSize: 20
        }
    });

    //grid search
    $(".q-condition .btn-search").click(function () {
        DdJob.refresh();
    });

    var _$modal = $('#CreateModal');
    var _$form = _$modal.find('form');

    _$form.validate();

    _$form.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$form.valid()) {
            return;
        }
        var model = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js          
        abp.ui.setBusy(_$modal);
        var data = JSON.stringify(model);
        $.ajax({
            type: "post",
            url: "/api/bank/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    abp.notify.success("保存成功", "通知");
                    _$modal.modal('hide');
                    abp.ui.clearBusy(_$modal);
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
                abp.notify.error("保存失败", "通知");
            }
        });
    });

    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });

    $(".page-content").on("click", ".btn-showmodal", function () {
        var uid = $(this).data("uid");
        var _$modal = $('#CreateModal');
        _$modal.modal('show');
       

        var eleName = $("input[name=Name]");
        var eleDescription = $("input[name=Description]");
        var eleAddress = $("input[name=Address]");
        var eleCode = $("input[name=Code]");
        var elePostalCode = $("input[name=PostalCode]");
        var eleIsActive = $("input[name=IsActive]");
        $("input[name=Id]").val(uid);
        if (uid && uid > 0) {
            $("#CreateModal .modal-title").html("编辑");
            $.ajax({
                type: "get",
                url: "/api/bank/getbyid?id=" + uid,
                contentType: "application/json",
                data: null,
                beforeSend: function () {
                    abp.ui.setBusy();
                },
                success: function (result) {
                    if (result.success) {
                        abp.ui.clearBusy();
                        eleName.val(result.data.name);
                        eleDescription.val(result.data.description);
                        eleCode.val(result.data.code);
                        elePostalCode.val(result.data.postalCode);
                        eleAddress.val(result.data.address);
                        eleIsActive.attr("checked", result.data.isActive);
                    }
                },
                error: function (err) {
                    abp.ui.clearBusy();
                    abp.notify.error(err.responseJSON.error.message, "通知");
                }
            });
        } else {
            $("#CreateModal .modal-title").html("新建");
            eleName.val("").attr("disabled", false);
            eleDescription.val("");
            eleCode.val("");
            elePostalCode.val("");
            eleAddress.val("");
            eleIsActive.attr("checked", true);
        }
    });

    $(".page-content").on("click", ".btn-delete", function () {
        var uid = $(this).data("uid");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteUser();
                }
            });

        function deleteUser() {
            var data = { id: uid };
            abp.ui.setBusy($('body'),
                $.ajax({
                    type: "post",
                    url: "/api/bank/Delete",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.success) {
                            abp.notify.success("删除成功", "通知");
                            DdJob.refresh();
                        } else {
                            abp.notify.error("删除失败", "通知");
                        }
                    },
                    error: function (err) {
                        abp.notify.error("删除失败", "通知");
                        abp.notify.error(err.responseJSON.error.message, "通知");
                    }
                }));
        };
    });

});