﻿using Abp.Web.Mvc.Views;
using DdJob.Core;

namespace DdJob.Web.Views
{
    public abstract class DdJobWebViewPageBase : DdJobWebViewPageBase<dynamic>
    {

    }

    public abstract class DdJobWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected DdJobWebViewPageBase()
        {
            LocalizationSourceName = DdJobConsts.LocalizationSourceName;
        }
    }
}