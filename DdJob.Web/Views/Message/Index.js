﻿$(function () {
    //grid
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/message/GetMessageByPage"
                }
            },
            pageSize: 20
        }
    });

    //grid search
    $(".q-condition .btn-search").click(function () {
        var grid = DdJob.kd.grid($(".q-result .q-grid"));
        if (grid) {
            grid.dataSource.page(1);
        }
    });

    var _$modal = $('#CreateMessageModal');
    var _$baseform = _$modal.find('form.itemBaseForm');

    _$baseform.validate();

    _$baseform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$baseform.valid()) {
            return;
        }
        var model = _$baseform.serializeFormToObject();
        if (model.BankId == 0) {
            abp.notify.error("请选择银行", "通知");
            return;
        }
        var data = JSON.stringify(model);
        $.ajax({
            type: "post",
            url: "/api/Message/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            beforeSend: function () { abp.ui.setBusy(); },
            success: function (result) {
                if (result.success) {
                    _$modal.modal('hide');
                    abp.notify.success("基本信息保存成功", "通知");
                    abp.ui.clearBusy();
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy();
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });
    $(".page-content").on("click", ".btn-showmodal", function () {
        var id = $(this).data("id");
        var _$modal = $('#CreateMessageModal');


        //清空数据
        $("input[name=Id]").val(0);
        $("#Title").val("");
        $("#Content").val("");
        $("#MessageType").val("0");
        $("#IsRead").prop("checked", true);
        $("#UserName").val("");
        $("#MediaPath").attr("src","");
        //编辑
        if (id && id > 0) {
            $.ajax({
                type: "get",
                url: "/api/message/GetMessageDtoById?id=" + id,
                contentType: "application/json",
                data: null,
                beforeSend: function () {
                    abp.ui.setBusy();
                },
                success: function (result) {
                    abp.ui.clearBusy();
                    if (result.success) {
                        $("input[name=Id]").val(result.data.id);
                        $("input[name=Name]").val(result.data.name);
                        $("input[name=Description]").val(result.data.description);
                        $("input[name=IsActive]").prop("checked", result.data.isActive);
                        $("#BankId").val(result.data.bankId);

                        $("input[name=Id]").val(result.data.id);
                        $("#Title").val(result.data.title);
                        $("#Content").val(result.data.content);
                        $("#MessageType").val(result.data.messageType);
                        $("#IsRead").prop("checked", result.data.isRead);
                        $("#UserName").val(result.data.userName);
                        $("#MediaPath").attr("src", result.data.mediaPath);
                        _$modal.modal('show');
                    }
                },
                error: function (err) {
                    abp.ui.clearBusy();
                    abp.notify.error(err.responseJSON.error.message, "通知");
                }
            });
        } else {
            _$modal.modal('show');
        }
    });

    $(".page-content").on("click", ".btn-delete", function () {
        var id = $(this).data("id");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteMessage();
                }
            });

        function deleteMessage() {
            var data = { id: id };
            abp.ui.setBusy($('body'),
                $.ajax({
                    type: "post",
                    url: "/api/message/Delete",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.success) {
                            abp.notify.success("删除成功", "通知");
                            DdJob.refresh();
                        } else {
                            abp.notify.error("删除失败", "通知");
                        }
                    },
                    error: function (err) {
                        abp.notify.error(err.responseJSON.error.message, "通知");
                    }
                }));
        };
    });

});