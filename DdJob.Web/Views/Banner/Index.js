﻿$(function () {
    //grid
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/banner/GetBannerByPage"
                }
            },
            pageSize: 20
        }
    });

    //grid search
    $(".q-condition .btn-search").click(function () {
        var grid = DdJob.kd.grid($(".q-result .q-grid"));
        if (grid) {
            grid.dataSource.page(1);
        }
    });

    var _$modal = $('#CreateDataModal');
    var _$baseform = _$modal.find('form.itemBaseForm');

    _$baseform.validate();

    _$baseform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$baseform.valid()) {
            return;
        }
        var model = _$baseform.serializeFormToObject();
        //检验分类
        model.MediaId = $("img.file-preview-image").data("id");
        abp.ui.setBusy();
        var data = JSON.stringify(model);

        $.ajax({
            type: "post",
            url: "/api/banner/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    _$modal.modal('hide');
                    abp.notify.success("基本信息保存成功", "通知");
                    abp.ui.clearBusy();
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy();
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy();
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });
    $(".page-content").on("click", ".btn-showmodal", function () {
        var dataid = $(this).data("dataid");
        var _$modal = $('#CreateDataModal');

        //清空数据

        $("input[name=Id]").val(0);
        $("input[name=MediaId]").val(0);
        $("#CategoryId").val(0);
        $("#LinkUrl").val("");
        $(".file-preview-thumbnails").html("");
        //编辑
        if (dataid && dataid > 0) {
            $.ajax({
                type: "get",
                url: "/api/banner/GetBannerById?id=" + dataid,
                contentType: "application/json",
                data: null,
                beforeSend: function () {
                    abp.ui.setBusy();
                },
                success: function (result) {
                    abp.ui.clearBusy();
                    if (result.success) {
                        $("input[name=Id]").val(result.data.id);
                        $("#LinkUrl").val(result.data.linkUrl);
                        var str = '<div class="file-preview-frame"><div class="kv-file-content"><img data-id="' + result.data.mediaId + '" src="' + result.data.mediaPath + '" class="file-preview-image" title="" alt="" /></div><p></p><i class="fa fa-trash btn-remove-file"></i></div>'
                        $(".file-preview-thumbnails").html(str);
                        _$modal.modal('show');
                        
                        if (result.data.categoryId == null || result.data.categoryId == 0) {
                            $("#CategoryId").val(result.data.categoryId);
                        } else {
                            $("#CategoryId").val(result.data.categoryId);
                           
                        }
                    }


                },
                error: function (err) {
                    abp.ui.clearBusy();
                    abp.notify.error(err.responseJSON.error.message, "通知");
                }
            });
        } else {

            _$modal.modal('show');
        }
    });

    $(".page-content").on("click", ".btn-delete", function () {
        var uid = $(this).data("dataid");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteBusiness();
                }
            });
        function deleteBusiness() {
            var data = { id: uid };
            abp.ui.setBusy($('body'),
                $.ajax({
                    type: "post",
                    url: "/api/banner/Delete",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.success) {
                            abp.notify.success("删除成功", "通知");
                            DdJob.refresh();
                        } else {
                            abp.notify.error("删除失败", "通知");
                        }
                    },
                    error: function (err) {
                        abp.notify.error(err.responseJSON.error.message, "通知");
                    }
                }));
        };
    });


    //图片上传
    $('#input-uploadimg').fileupload({
        url: "/api/file/upload",
        dataType: 'json',
        maxChunkSize: 1000000,
        done: function (e, data) {
            abp.ui.clearBusy();
            var result = data.result;
            if (result) {
                if (result.fileId > 0) {
                    $("#MediaId").val(result.fileId);
                    DdJob.appendPreviewImage(".product", result.netImageUrl, result.fileName, result.fileId, true);
                } else {
                    abp.notify.error("图片上传失败", "通知");
                }
            } else {
                abp.notify.error("图片上传失败", "通知");
            }
        },
        beforeSend: function () {
            abp.ui.setBusy();
        },
        fail: function (e, data) {
            abp.ui.clearBusy();
            abp.notify.error("上传错误", "通知");
        }
    });

    //$("body").on("change", "#CategoryOneId", function () {
    //    $.ajax({
    //        type: "get",
    //        url: "/api/category/GetCategoryByParentId?id=" + $(this).val(),
    //        contentType: "application/json",
    //        //data: JSON.stringify(data),
    //        beforeSend: function () {
    //            abp.ui.setBusy();
    //        },
    //        success: function (result) {
    //            abp.ui.clearBusy();
    //            if (result.success) {
    //                if (result.data.length == 0) {
    //                    $("#CategoryTwoId").addClass("hide");
    //                } else {
    //                    $("#CategoryTwoId").removeClass("hide");
    //                    str = '<option value="0">--请选择--</option>';
    //                    for (var i = 0; i < result.data.length; i++) {
    //                        str += '<option value="' + result.data[i].id + '">' + result.data[i].name + '</option>';
    //                    }
    //                    $("#CategoryTwoId").html(str);
    //                }

    //            } else {
    //                abp.notify.error("保存失败", "通知");
    //            }
    //        },
    //        error: function (err) {
    //            abp.ui.clearBusy();
    //            abp.notify.error(err.responseJSON.error.message, "通知");
    //        }
    //    });
    //});

    ////Category 兄弟分类获取
    //function SetSecondCategory(ParentId, CategoryId) {
    //    $.ajax({
    //        type: "get",
    //        url: "/api/category/GetCategoryByParentId?id=" + ParentId,
    //        contentType: "application/json",
    //        //data: JSON.stringify(data),
    //        beforeSend: function () {
    //            abp.ui.setBusy();
    //        },
    //        success: function (result) {
    //            abp.ui.clearBusy();
    //            if (result.success) {
    //                if (result.data.length == 0) {
    //                    $("#CategoryTwoId").addClass("hide");
    //                } else {
    //                    $("#CategoryTwoId").removeClass("hide");
    //                    str = '<option value="0">--请选择--</option>';
    //                    for (var i = 0; i < result.data.length; i++) {
    //                        str += '<option value="' + result.data[i].id + '">' + result.data[i].name + '</option>';
    //                    }
    //                    $("#CategoryTwoId").html(str);
    //                    $("#CategoryTwoId").val(CategoryId);
    //                }

    //            } else {
    //                abp.notify.error("保存失败", "通知");
    //            }
    //        },
    //        error: function (err) {
    //            abp.ui.clearBusy();
    //            abp.notify.error(err.responseJSON.error.message, "通知");
    //        }
    //    });
    //}
});