﻿$(function () {
    //grid
    DdJob.kd.grid({
        id: "gridList",
        dataSource: {
            transport: {
                read: {
                    url: "/api/business/getBusinessByPage"
                }
            },
            pageSize: 20
        }
    });

    //grid search
    $(".q-condition .btn-search").click(function () {
        var grid = DdJob.kd.grid($(".q-result .q-grid"));
        if (grid) {
            grid.dataSource.page(1);
        }
    });

    var _$modal = $('#CreateBusinessModal');
    var _$baseform = _$modal.find('form.itemBaseForm');

    _$baseform.validate();

    _$baseform.find('button[type="submit"]').click(function (e) {
        e.preventDefault();

        if (!_$baseform.valid()) {
            return;
        }
        var model = _$baseform.serializeFormToObject();
        abp.ui.setBusy(_$modal);
        var data = JSON.stringify(model);
        $.ajax({
            type: "post",
            url: "/api/business/CreateOrUpdate",
            contentType: "application/json",
            data: data,
            success: function (result) {
                if (result.success) {
                    _$modal.modal('hide');
                    abp.notify.success("基本信息保存成功", "通知");
                    abp.ui.clearBusy(_$modal);
                    DdJob.refresh();
                } else {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error("保存失败", "通知");
                }
            },
            error: function (err) {
                abp.ui.clearBusy(_$modal);
                abp.notify.error(err.responseJSON.error.message, "通知");
            }
        });
    });

    _$modal.on('shown.bs.modal', function () {
        _$modal.find('input:not([type=hidden]):first').focus();
    });
    $(".page-content").on("click", ".btn-showmodal", function () {
        var bid = $(this).data("bid");
        var _$modal = $('#CreateBusinessModal');


        //清空数据
        $("input[name=Id]").val(0);
        $("input[name=Code]").val("");
        $("input[name=Name]").val("");
        $("input[name=Description]").val("");
        $("input[name=IsActive]").prop("checked", true);
        $("input[name=Address]").val("");
        $("input[name=Phone]").val("");
        $("input[name=ReceiverName]").val("");
        $("input[name=ReceiverPhone]").val("");
        $("input[name=BankName]").val("");
        $("input[name=BankCardNum]").val("");
        $("input[name=BankAccountUser]").val("");
        //编辑
        if (bid && bid > 0) {
            $.ajax({
                type: "get",
                url: "/api/Business/GetEntityById?id=" + bid,
                contentType: "application/json",
                data: null,
                beforeSend: function () {
                    abp.ui.setBusy($('body'));
                },
                success: function (result) {
                    if (result.success) {
                        $("input[name=Id]").val(result.data.id);
                        $("input[name=Code]").val(result.data.code);
                        $("input[name=Name]").val(result.data.name);
                        $("input[name=Description]").val(result.data.description);
                        $("input[name=IsActive]").prop("checked", result.data.isActive);
                        $("input[name=Address]").val(result.data.address);
                        $("input[name=Phone]").val(result.data.phone);
                        $("input[name=ReceiverName]").val(result.data.receiverName);
                        $("input[name=ReceiverPhone]").val(result.data.receiverPhone);
                        $("input[name=BankName]").val(result.data.bankName);
                        $("input[name=BankCardNum]").val(result.data.bankCardNum);
                        $("input[name=BankAccountUser]").val(result.data.bankAccountUser);
                        _$modal.modal('show');
                    }
                    abp.ui.clearBusy($('body'));

                },
                error: function (err) {
                    abp.ui.clearBusy(_$modal);
                    abp.notify.error(err.responseJSON.error.message, "通知");
                }
            });
        } else {
            _$modal.modal('show');
        }
    });

    $(".page-content").on("click", ".btn-delete", function () {
        var uid = $(this).data("bid");
        abp.message.confirm(
            '您确定删除吗?',
            '提醒',
            function (isConfirmed) {
                if (isConfirmed) {
                    deleteBusiness();
                }
            });

        function deleteBusiness() {
            var data = { id: uid };
            abp.ui.setBusy($('body'),
                $.ajax({
                    type: "post",
                    url: "/api/business/Delete",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    success: function (result) {
                        if (result.success) {
                            abp.notify.success("删除成功", "通知");
                            DdJob.refresh();
                        } else {
                            abp.notify.error("删除失败", "通知");
                        }
                    },
                    error: function (err) {
                        abp.notify.error(err.responseJSON.error.message, "通知");
                    }
                }));
        };
    });

});