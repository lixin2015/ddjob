﻿(function () {

    $(function () {
        $('#LoginButton').click(function (e) {
            e.preventDefault();
            var obj = {
                tenancyName: "",
                usernameOrEmailAddress: $('#EmailAddressInput').val(),
                password: $('#PasswordInput').val(),
                returnUrl: DdJob.getQueryString("returnUrl")
            };
            var data = JSON.stringify(obj);
            abp.ui.setBusy(
                $('#LoginArea'),
                $.ajax({
                    type: "post",
                    url: "/Account/Login",
                    contentType: "application/json",
                    data: data,
                    success: function (result) {
                        result = JSON.parse(result);
                        if (result.success) {
                            location.href = result.data.url;
                        }
                        else {
                            abp.ajax.showError({ "message": "登录失败", "details": result.error.message });
                        }
                    },
                    error: function (err) {
                    }
                })
            );
        });

        //$('#ReturnUrlHash').val(location.hash);
        $('#LoginForm input:first-child').focus();
    });
})();