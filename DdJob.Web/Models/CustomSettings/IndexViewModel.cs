﻿using DdJob.Enums;
using DdJob.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DdJob.Web.Models.CustomSettings
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
        }
        public EnumItemCollection GroupTypeSource { get; set; }
    }
}