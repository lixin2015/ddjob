using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Abp.Authorization.Users;
using DdJob.Core.Users;

namespace DdJob.Web.Models.Account
{
    public class RegisterViewModel : IValidatableObject
    {
        /// <summary>
        /// Not required for single-tenant applications.
        /// </summary>
        //[StringLength(AbpTenantBase.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        //[Required]
        //[StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(User.MaxSurnameLength)]
        public string Surname { get; set; }

        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }

        public bool IsExternalLogin { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var emailRegex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
            if (!UserName.Equals(EmailAddress) && emailRegex.IsMatch(UserName))
            {
                yield return new ValidationResult("Username cannot be an email address unless it's same with your email address !");
            }
        }
    }
    public class MobileRegisterViewModel : IValidatableObject
    {
        public string TenancyName { get; set; }
        public string Name { get { return "gd" + UserName; } }
        public string Surname { get { return "gd" + UserName; } }

        public string UserName { get { return MD5Helper.GetStringHash(Mobile); } }
        public string EmailAddress { get { return UserName + "@gd_bank_customer.com"; } }

        [Required]
        [MaxLength(11)]
        public string Mobile { get; set; }
        [Required]
        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }
        [Required]
        [StringLength(6)]
        public string VerifyCode { get; set; }
        [Required]
        [StringLength(4)]
        public string ImageCode { get; set; }
        [Required]
        public string Agreement { get; set; }

        public bool IsExternalLogin { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var emailRegex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
            if (!UserName.Equals(EmailAddress) && emailRegex.IsMatch(UserName))
            {
                yield return new ValidationResult("Username cannot be an email address unless it's same with your email address !");
            }
        }
    }    
}