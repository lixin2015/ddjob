﻿using System.ComponentModel.DataAnnotations;

namespace DdJob.Web.Models.Account
{
    public class LoginViewModelBase
    {
        public string TenancyName { get; set; }
        public bool RememberMe { get; set; }
        public string ReturnUrl { get; set; }
    }
    public class LoginViewModel : LoginViewModelBase
    {
        [Required]
        public string UsernameOrEmailAddress { get; set; }
        [Required]
        public string Password { get; set; }
        public string OpenId { get; set; }
        public bool IsMobile { get; set; }
    }
    public class MobileCodeLoginViewModel : LoginViewModelBase
    {
        [Required]
        public string Mobile { get; set; }
        [Required]
        public string VerifyCode { get; set; }
    }

    public class WeiXinLoginViewModel : LoginViewModelBase
    {
        [Required]
        public string OpenId { get; set; }
    }
}