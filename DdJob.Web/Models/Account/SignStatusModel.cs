﻿using System.ComponentModel.DataAnnotations;

namespace DdJob.Web.Models.Account
{
    public class SignStatusModel
    {
        public SignStatusModel()
        {
            Success = true;
        }
        public bool Success { get; set; }
        public string Url { get; set; }
    }
}