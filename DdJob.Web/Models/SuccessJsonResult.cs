﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DdJob.Web.Extensions;

namespace DdJob.Web.Models
{
    public class SuccessResult<TData> : InvokeResult<TData>
    {
        public SuccessResult(TData data)
        {
            Success = true;
            Data = data;
        }
    }

    public class SuccessJsonResult: LowerCamelJsonResult
    {
        public SuccessJsonResult(object data)
        {
            Data = new SuccessResult<object>(data);
        }
    }
}