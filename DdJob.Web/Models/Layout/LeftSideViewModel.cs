﻿using System.Collections.Generic;
using DdJob.Application.Roles.Dto;

namespace DdJob.Web.Models.Layout
{
    public class LeftSideViewModel
    {
        public string UserName { get; set; }
        public string Name { get; set; }        
        public List<PermissionDto> GrantedPermissions { get; set; }
    }
}