﻿namespace DdJob.Web.Models.Layout
{
    public class HeaderViewModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string UserType{ get; set; }
        public string DisplayInfo{ get; set; }
        public string CreationTime { get; set; }
        public int MessageCount { get; set; }
    }
}