﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abp.Web.Models;

namespace DdJob.Web.Models
{
    public class InvokeResult<TData>
    {
        public bool Success { get; set; }
        public TData Data { get; set; }
        public ErrorInfo Error { get; set; }
    }
}