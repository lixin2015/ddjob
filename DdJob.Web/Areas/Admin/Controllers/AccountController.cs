﻿using System.Web.Mvc;
using ControllerBase = DdJob.Web.Controllers.ControllerBase;

namespace DdJob.Web.Areas.Admin.Controllers
{
    public class AccountController : ControllerBase
    {
        public ActionResult Login()
        {
            return View();
        }
    }
}