﻿using System.Web.Mvc;
using ControllerBase = DdJob.Web.Controllers.ControllerBase;

namespace DdJob.Web.Areas.Admin.Controllers
{
    public class HomeController : ControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}