﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DdJob.Web.Models.CustomSettings;
using DdJob.Authorization;
using Abp.Web.Mvc.Authorization;

namespace DdJob.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_CustomSetting)]
    public class CustomSettingController : ControllerBase
    {
        [AbpMvcAuthorize(PermissionNames.Pages_CustomSetting_Admin)]
        public ActionResult Index()
        {
            IndexViewModel viewModel = new IndexViewModel();
            return View(viewModel);
        }
    }
}