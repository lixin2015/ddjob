﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using DdJob.Authorization;

namespace DdJob.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Role)]
    public class RoleController : ControllerBase
    {
        public RoleController()
        {
        }

        [AbpMvcAuthorize(PermissionNames.Pages_RoleList_Admin)]
        public async Task<ActionResult> Index()
        {
            return View();
        }
    }
}