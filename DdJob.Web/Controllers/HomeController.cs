﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using DdJob.Application.Sessions;
using DdJob.Authorization;

namespace DdJob.Web.Controllers
{
    public class HomeController : ControllerBase
    {
        private readonly ISessionAppService _sessionAppService;

        public HomeController(ISessionAppService sessionAppService)
        {
            _sessionAppService = sessionAppService;
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}