﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;
using DdJob.Authorization;
using System.Collections.Generic;
using DdJob.Application;
using DdJob.Application.Users;

namespace DdJob.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_User)]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;



        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AbpMvcAuthorize(PermissionNames.Pages_UserList_Admin)]
        public ActionResult Index()
        {         
            return View();
        }
    }
}