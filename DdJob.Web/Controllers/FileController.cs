﻿using Abp.Web.Models;
using DdJob.Enums;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using DdJob.Application.Public;
using DdJob.Application.Public.Dto;

namespace DdJob.Web.Controllers
{
    public class FileController : ControllerBase
    {
        private readonly IMediaService _mediaService;

        private readonly string _tempPath;
        public FileController(IMediaService mediaService)
        {
            _mediaService = mediaService;
            _tempPath = string.Format("Media\\Images\\{0}\\{1}\\", DateTime.Now.Year, DateTime.Now.ToString("MM-dd"));
        }

        [Authorize]
        public async Task<ApiOutput<string>> Upload()
        {
            if (Request.ContentType == "")
            {
                return new ApiOutput<string>(new ErrorInfo() { Message = "上传失败!", Details = "不支持媒体文件。" });
            }

            // 文件保存目录路径
            var dirTempPath = AppDomain.CurrentDomain.BaseDirectory + _tempPath;
            lock (dirTempPath)
            {
                if (!Directory.Exists(dirTempPath))
                {
                    Directory.CreateDirectory(dirTempPath);
                }
            }

            // 最大文件大小
            const int maxSize = 1000000;
            // 定义允许上传的文件扩展名 
            const string fileTypes = "gif,jpg,jpeg,png,bmp";

            if (Request.Files.Count <= 0)
            {
                return new ApiOutput<string>(new ErrorInfo() { Message = "上传失败!", Details = "没有找到上传的文件。" });
            }

            //目前支持一张图片上传
            var fileItem = Request.Files[0];
            string orfilename = fileItem.FileName.TrimStart('"').TrimEnd('"');
            var fileExt = orfilename.Substring(orfilename.LastIndexOf('.'));

            if (fileItem.ContentLength > maxSize)
            {
                return new ApiOutput<string>(new ErrorInfo() { Message = "上传失败!", Details = "上传文件大小超过限制。" });
            }
            if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
            {
                return new ApiOutput<string>(new ErrorInfo() { Message = "上传失败!", Details = "不支持上传文件类型。" });
            }
            var ext = Path.GetExtension(orfilename);
            var fileName = Guid.NewGuid() + ext;
            fileItem.SaveAs(Path.Combine(dirTempPath, fileName));

            var mediaData = new MediaDto
            {
                Path = _tempPath + fileName,
                MediaType = MediaType.Image
            };
            var mediaId = await _mediaService.SaveMediaFile(mediaData);

            mediaData.Id = mediaId;
            return new ApiOutput<string>(mediaData.Id + mediaData.Path);
        }
    }
}