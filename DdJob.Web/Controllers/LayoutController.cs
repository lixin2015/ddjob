﻿using System.Web.Mvc;
using Abp.Application.Navigation;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.Runtime.Session;
using Abp.Threading;
using DdJob.Application.Sessions;
using DdJob.Application.Users;
using DdJob.Web.Models.Layout;

namespace DdJob.Web.Controllers
{
    public class LayoutController : ControllerBase
    {
        private readonly IUserNavigationManager _userNavigationManager;
        private readonly ISessionAppService _sessionAppService;
        private readonly IMultiTenancyConfig _multiTenancyConfig;
        private readonly ILanguageManager _languageManager;
        private readonly IUserService _userService;

        public LayoutController(
            IUserNavigationManager userNavigationManager,
            ISessionAppService sessionAppService,
            IMultiTenancyConfig multiTenancyConfig,
            ILanguageManager languageManager,
            IUserService userService)
        {
            _userNavigationManager = userNavigationManager;
            _sessionAppService = sessionAppService;
            _multiTenancyConfig = multiTenancyConfig;
            _languageManager = languageManager;
            _userService = userService;
        }

        [ChildActionOnly]
        public PartialViewResult TopMenu(string activeMenu = "")
        {
            var test = _userNavigationManager.GetMenuAsync("MainMenu", AbpSession.ToUserIdentifier());
            var model = new TopMenuViewModel
            {
                MainMenu = AsyncHelper.RunSync(() => _userNavigationManager.GetMenuAsync("MainMenu", AbpSession.ToUserIdentifier())),
                ActiveMenuItemName = activeMenu
            };

            return PartialView("_TopMenu", model);
        }
        //[ChildActionOnly]
        //public PartialViewResult UserMenuOrLoginLink()
        //{
        //    UserMenuOrLoginLinkViewModel model;

        //    if (AbpSession.UserId.HasValue)
        //    {
        //        model = new UserMenuOrLoginLinkViewModel
        //        {
        //            LoginInformations = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentLoginInformations()),
        //            IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled,
        //        };
        //    }
        //    else
        //    {
        //        model = new UserMenuOrLoginLinkViewModel
        //        {
        //            IsMultiTenancyEnabled = _multiTenancyConfig.IsEnabled
        //        };
        //    }

        //    return PartialView("_UserMenuOrLoginLink", model);
        //}


        [ChildActionOnly]
        public PartialViewResult AdminLteLeftSideBar()
        {
            var current = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentLoginInformations());

            var model = new LeftSideViewModel()
            {
                UserName = current != null ? current.User.UserName : "",
                Name = current != null ? current.User.Name : "",
                GrantedPermissions = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentAllGrantedPermissions())
            };

            return PartialView("_AdminLteLeftSideBar", model);
        }

        [ChildActionOnly]
        public PartialViewResult AdminLteHeader()
        {
            var model = new HeaderViewModel();
            var user = _userService.GetEntityDtoById(AbpSession.GetUserId());
            if (user != null)
            {
                model.Name = user.Name;
                model.UserName = user.UserName;
                model.UserType = user.UserTypeText;
                model.CreationTime = user.CreationTime.ToString(DefaultString.DefaultShortDate);
            }

            return PartialView("_AdminLteHeader", model);
        }

        [ChildActionOnly]
        public PartialViewResult WinxinLeftSideBar()
        {
            var current = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentLoginInformations());

            var model = new LeftSideViewModel()
            {
                UserName = current != null ? current.User.UserName : "",
                Name = current != null ? current.User.Name : "",
                GrantedPermissions = AsyncHelper.RunSync(() => _sessionAppService.GetCurrentAllGrantedPermissions())
            };

            return PartialView("_WinxinLeftSideBar", model);
        }
        [ChildActionOnly]
        public PartialViewResult WinxinHeader()
        {
            var model = new HeaderViewModel();
            var user = _userService.GetEntityDtoById(AbpSession.GetUserId());
            if (user != null)
            {
                model.UserName = user.UserName;
                model.UserType = user.UserTypeText;
                model.CreationTime = user.CreationTime.ToString(DefaultString.DefaultShortDate);
            }

            return PartialView("_WeixinHeader", model);
        }
    }
}