﻿using System.Web.Mvc;
using DdJob.Web.Extensions;

namespace DdJob.Web
{
    /// <summary>
    /// Class providing manipulation with the application filters.
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Registers the global filters.
        /// </summary>
        /// <param name="filters">The global filters collection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //Add your filters registration here
            filters.Add(new MvcExceptionFilterAttribute());
        }
    }
}
