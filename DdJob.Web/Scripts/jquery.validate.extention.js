/*!
 * jQuery Validation Plugin v1.14.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2015 Jörn Zaefferer
 * Released under the MIT license
 */

// Custom selectors
$.extend($.validator.messages, {
    required: "该字段必填",
    remote: "请修正该字段",
    email: "邮箱格式不正确",
    url: "网络地址不正确",
    date: "日期格式不正确",
    dateISO: "请输入合法的日期 (ISO).",
    number: "请输入数字",
    digits: "只能输入整数",
    creditcard: "请输入合法的信用卡号",
    equalTo: "两次输入不一致",
    accept: "请输入拥有合法后缀名的字符",
    maxlength: $.validator.format("该字段最大长度为 {0}"),
    minlength: $.validator.format("该字段最小长度为 {0}"),
    rangelength: $.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符"),
    range: $.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
    max: $.validator.format("请输入一个最大为{0} 的值"),
    min: $.validator.format("请输入一个最小为{0} 的值")
});