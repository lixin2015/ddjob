﻿(function () {
    window.DdJob = {
        post: function (url, data, successCallBack, errorCallback) {
            data = "ajax=true&ts=" + new Date().getTime() + "&" + data;
            $.ajax({
                type: "post",
                url: url,
                contentType: "application/x-www-form-urlencoded",
                data: data,
                success: function (result) {
                    successCallBack && successCallBack(result);
                },
                error: function (err) {
                    if (err.status !== 0) {
                        errorCallback && errorCallback(err);
                    }
                }
            });
        },

        get: function (url, successCallBack, errorCallback) {
            var loadUrl;
            if (url.indexOf("?") < 0) {
                loadUrl = url + "?ajax=true&ts=" + new Date().getTime();
            } else {
                loadUrl = url + "&ajax=true&ts=" + new Date().getTime();
            }
            $.ajax({
                type: "get",
                url: loadUrl,
                contentType: "text/html",
                success: function (result) {
                    successCallBack && successCallBack(result);
                },
                error: function (ex) {
                    errorCallback && errorCallback(ex);
                }
            });
        },

        onEnterKeydown: function (inputSelector, callback) {
            $(inputSelector).keydown(function (e) {
                if (e.keyCode === 13) {
                    callback();
                }
            });
        },

        selectNavItem: function (selector) {
            $("ul.navbar-nav li").removeClass("active");
            $(selector).addClass("active");
        }
    };

    DdJob.kd = {
        _defaultOption: {
            dataSource: {
                type: "json",
                transport: {
                    read: {
                        type: "POST",
                        dataType: "json",
                        data: function () { return DdJob.getJsonData($(".q-condition")); }
                    }
                },
                schema: {
                    data: "Data",
                    total: "Total",
                    errors: "Errors",
                    parse: function (response) {
                        return response;
                    }
                },
                requestStart: function (e) {
                },
                requestEnd: function (e) {
                },
                error: function (e) {
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },
            //是否允许排序
            sortable: false,
            pageable: {
                refresh: true,
                pageSizes: [20, 50, 100]
            },
            editable: {
                confirmation: false,
                mode: "inline"
            },
            scrollable: false
        },
        grid: function (options) {
            var container;
            if (options instanceof jQuery) {
                container = options;
            } else if (typeof (options) == "string") {
                container = $(options);
            }
            if (container) {
                return container.data("kendoGrid");
            }

            var grid = $.extend(true, {}, this._defaultOption, options);
            if (!grid.dataSource.serverPaging) {
                grid.dataSource.schema = {
                    data: function (response) {
                        return response;
                    }
                };
            }
            var gridContainer = $("#" + grid.id);
            if (gridContainer.size() == 0) {
                return null;
            }
            var dataSource = new kendo.data.DataSource(grid.dataSource);
            grid.dataSource = dataSource;
            window[grid.id] = grid;
            gridContainer.data("kendoGrid", grid);
            gridContainer.find(".km-grid-pager").kendoPager({
                autoBind: false,
                dataSource: window[grid.id].dataSource,
                refresh: true
            });
            var viewModel = kendo.observable(grid);
            kendo.bind(gridContainer, viewModel);
            return grid;
        },
        initKendoControls: function (container) {
            container = $(container);
            container.find(":input").not(".no-kd").each(function () {
                var me = $(this);

                if (me.is("select")) {
                    me.is("[multiple]") ? me.kendoMultiSelect({ autoClose: false, filter: "contains" }) : me.kendoDropDownList();
                } else {
                    var type = (me.attr("type") || "").toLowerCase();
                    switch (type) {
                        case "email":
                        case "url":
                        case "tel":
                        case "password":
                        case "text": me.addClass("k-textbox"); break;
                        case "number":
                        case "range":
                            var decimals = me.data("decimals");
                            if (decimals === undefined) {
                                decimals = 0;
                            }
                            me.kendoNumericTextBox({ decimals: decimals, format: "" }); break;
                        case "date":
                            if (me.prop("required")) {
                                me.kendoDatePicker();
                            } else {
                                me.kendoDatePicker({
                                    footer: '<a href="javascript:;" title=' + commonRes.Clear + ' class="k-clear-calendar"><span class="glyphicon glyphicon-remove-circle"></span></a>',
                                    open: function (e) {
                                        //click.kendoCalendar
                                        $("#" + me.attr("id") + "_dateview .k-nav-today").unbind("click.kendoCalendar");
                                    }
                                });
                            }
                            me.attr("readonly", "readonly");
                            break;
                        case "time": me.kendoTimePicker(); break;
                        case "datetime": me.kendoDateTimePicker(); break;
                        //case "file": me.kendoUpload({ multiple: false }); break;
                        case "checkbox":
                            var checkboxWrapper = $('<span class="ckbox ckbox-primary mt10"></span>');
                            checkboxWrapper.insertBefore(me);
                            checkboxWrapper.append(me).append(String.format('<label for="{0}"></label>', me.attr("id")));
                            break;
                        default:

                    }
                }
            });

            container.find(":input.nofuturedate").each(function () {
                var me = $(this);
                var datepicker = me.data("kendoDatePicker");
                datepicker.max(new Date());
            });

        },
        getKdControl: function (context) {
            switch (context.data('role')) {
                case 'datepicker': return context.data("kendoDatePicker");
                case 'timepicker': return context.data('kendoTimePicker');
                case 'datetimepicker': return context.data('kendoDateTimePicker');
                case "numerictextbox": return context.data('kendoNumericTextBox');
                case 'dropdownlist': return context.data('kendoDropDownList');
                case 'multiselect': return context.data('kendoMultiSelect');
                default: return null;
            }
        },
        setValue: function (input, value) {
            var kdControl = lms.kd.getKdControl(input);
            kdControl ? kdControl.value(value) : input.val(value);
        }
    };

    DdJob.getJsonData = function (wrapperSelector) {
        var manipulationRcheckableType = /^(?:checkbox|radio)$/i,
            rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
            rsubmittable = /^(?:input|select|textarea|keygen)/i;
        var $wrapper = $(wrapperSelector);
        var result = {};
        $wrapper.find("*").filter(function () {
            var type = this.type;
            //hack: for asp.net mvc checkbox
            if (type == "hidden" && this.name) {
                if ($wrapper.find(":checkbox[name='" + this.name + "']").size()) {
                    return false;
                }
            }
            return this.name && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) &&
                (this.checked || !manipulationRcheckableType.test(type));
        }).each(function (i, elem) {
            var $field = $(elem);
            if (elem.type == "checkbox") {
                result[elem.name] = $field.val() === "true" ? elem.checked : $field.val();
            } else {
                var val = $field.val();
                if (val != null) {
                    result[elem.name] = $field.val();
                }
            }
        });
        return result;
    };

    DdJob.refresh = function () {
        var grid = DdJob.kd.grid($(".q-result .q-grid"));
        if (grid) {
            grid.dataSource.page(1);
        }
    };

    DdJob.compareDate = function tab(date1, date2) {
        var oDate1 = new Date(date1);
        var oDate2 = new Date(date2);
        if (oDate1.getTime() >= oDate2.getTime()) {
            return true;
        } else {
            return false;
        }
    };

    //下载地址：url='/File/Export/银行接入电子卷兑换记录-管理员端20170307203859.xlsx'
    DdJob.downloadFile = function (url) {
        try {
            try {
                var ele = document.createElement("iframe");
                ele.id = "down-file-iframe";
                ele.src = url;
                ele.style.display = "none";
                document.body.appendChild(ele);
                document.body.removeChild(iframe);
            } catch (e) {
            }
        } catch (e) {
        }
    };

    DdJob.getQueryString = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return unescape(r[2]);
        return "";
    };

    DdJob.formatGridData = function (data) {
        if (data || data == null)
            return "";
        else return data;
    };

    DdJob.serializetojson = function (wrapperSelector) {
        var manipulationRcheckableType = /^(?:checkbox|radio)$/i,
            rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
            rsubmittable = /^(?:input|select|textarea|keygen)/i;
        var $wrapper = $(wrapperSelector);
        var result = {};
        $wrapper.find("*").filter(function () {
            var type = this.type;
            return this.name && !jQuery(this).is(":disabled") &&
                rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) &&
                (this.checked || !manipulationRcheckableType.test(type));
        }).each(function (i, elem) {
            var $field = $(elem);
            var fieldName = elem.name;
            if (elem.type == "checkbox") {
                result[fieldName] = elem.checked;
            } else {
                var val = $field.val();
                if (val != null) {
                    result[fieldName] = $field.val();
                }
            }
        });
        return result;
    };

    DdJob.validatemobile = function (mobile) {
        if (mobile.length == 0 || mobile.length != 11) return false;
        var myreg = /^1[3|4|5|8][0-9]\d{4,8}$/;
        return !!myreg.test(mobile);
    };

    //获取QueryString的数组
    DdJob.getQueryString = function getQueryString() {
        var result = location.search.match(new RegExp("[\?\&][^\?\&]+=[^\?\&]+", "g"));
        if (result == null) {
            return "";
        }
        for (var i = 0; i < result.length; i++) {
            result[i] = result[i].substring(1);
        }
        return result;
    }

    //根据QueryString参数名称获取值
    DdJob.getQueryStringByName = function getQueryStringByName(name) {
        var result = location.search.match(new RegExp("[\?\&]" + name + "=([^\&]+)", "i"));
        if (result == null || result.length < 1) {
            return "";
        }
        return result[1];
    }

    //根据QueryString参数索引获取值
    DdJob.getQueryStringByIndex = function getQueryStringByIndex(index) {
        if (index == null) {
            return "";
        }
        var queryStringList = getQueryString();
        if (index >= queryStringList.length) {
            return "";
        }
        var result = queryStringList[index];
        var startIndex = result.indexOf("=") + 1;
        result = result.substring(startIndex);
        return result;
    }

    DdJob.redirect = function (url, delay) {
        if (delay)
            window.setTimeout("window.location=" + url, 2000);
        window.location = url;
    }

    //parentSelector 父选择器
    //path 图片路径
    //name 图片名称
    //id 图片对应数据库中Id
    DdJob.appendPreviewImage = function (parentSelector, path, name, id, onlyOne = false) {
        if ($(parentSelector).length <= 0)
            return;

        var imgHtml = '<div class="file-preview-frame"><div class="kv-file-content"><img data-id="' + id + '" src="' + path + '" class="file-preview-image" title="' + name +
            '" alt="' + name + '" /></div><p>' + name + '</p><i class="fa fa-trash btn-remove-file"></i></div>';
        if (onlyOne) {
            $(parentSelector).find(".file-preview-thumbnails .file-preview-frame").remove();
        }
        $(parentSelector).find(".file-preview-thumbnails").append(imgHtml);
    }

    $(document).ready(function () {
        if (window.toastr) {
            toastr.options = {
                "showDuration": "300",
                "hideDuration": "500",
                "timeOut": "3000",
                "extendedTimeOut": "500"
            };
        }

        $(".file-preview-thumbnails").on("click", ".btn-remove-file", function () {
            $(this).closest('.file-preview-frame').remove();
        });
    });
})();