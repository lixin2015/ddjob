﻿(function () {
    //  ajax请求错误处理
    //$(document).ajaxError(function (event, xhr, options, exc) {
    //    console.log(xhr);
    //    console.log(exc);
    //    if (xhr.readyState === 4) {
    //        alert("网络出了点故障，稍后再试试吧～");
    //    } else {
    //        console.log("网络请求出错了");
    //    }
    //});

    // banner
    if (document.getElementById("my-swiper")) {
        var myswiper = new Swiper('#my-swiper', {
            direction: "horizontal",
            loop: true,
            autoplay: "5000",
            lazyLoading: true,
            pagination: '#my-swiper .swiper-pagination'
        });
    }

    // tabs
    var mytabs = $("#my-tabs .tabs");
    $("#my-tabs .hd .tab-btn").click(function () {
        var index = $(this).index();
        $(this).addClass("on").siblings().removeClass("on");
        mytabs.eq(index).show().siblings().hide();
    }).eq(0).trigger("click");

    //swich list view
    $(".gridList-head").on("click", ".swichBtn", function () {
        var gridList = $(".gridList");
        if ($(this).hasClass("gridBtn")) {
            $(this).removeClass("gridBtn");
            $(this).addClass("listBtn");
            gridList.removeClass("grid-view");
            gridList.addClass("list-view");
        } else {
            $(this).removeClass("listBtn");
            $(this).addClass("gridBtn");
            gridList.removeClass("list-view");
            gridList.addClass("grid-view");
        }
        scroll(0, 0);
    });
})();
