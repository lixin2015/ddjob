(function ($) {
    var success = [
        "您已支付成功",
        "收货地址添加成功",
        "删除成功",
        "修改成功",
        "修改昵称成功",
        "收藏成功",
        "取消收藏成功",
        "领取成功",
        "领取成功",
        "设置成功",
        "已提醒卖家处理退款，请小主耐心等待",
        "已提醒卖家发货，请小主耐心等待",
    ];
    //需要显示！！感叹号图标的字符串数组
    var exclamation = [
        "请完善订单信息",
        "删除订单失败",
        "请输入验证码",
        "请选择配送方式",
        "请输入手机号码",
        "您已超出促销数量限制",
        "您要操作的地址不存在",
        "您输入的卡号不存在",
        "原密码错误",
        "请选择商品属性",
        "宝贝不能再减少了～",
        "库存不足了～",
        "账号或密码错误",
        "库存不足",
        "请选择商品属性",
        "没有需要清空的宝贝",
        "操作异常",
        "您还没有选择商品哦～",
        "您还没有选择宝贝哦",
        "输入无效",
        "商品数量至少为1哦",
        "订单24小时后才可提醒卖家发货",
        "您尚未登录无法收藏",
        "24小时方可提醒卖家，请耐心等候",
        "优惠券单人限领量超限",
        "验证码错误",
        "优惠券不可用",
    ];

    //  modifyBy QQ 2017-10-18
    //  设置全局post请求参数
    if ($('meta[name="csrf-token"]').length != 0 || $('meta[name="csrf_token"]').length != 0) {
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') || $('meta[name="csrf_token"]').attr('content')
            }
        });
    }

    jQuery.extend({
        formatFloat: function (value) {
            // var xsd = value.toString().split("."),
            //     returnStr = '',
            //     decimal = '.00';
            // if (xsd.length == 1) {
            //     returnStr = value + decimal;
            // } else {
            //     decimal = xsd[1].substr(0, 2);
            //
            //     if (decimal.length < 2) {
            //         decimal = decimal + '0';
            //     }
            //
            //     returnStr = xsd[0] + '.' + decimal;
            // }
            // return parseFloat(returnStr);

            //  modifyBy QQ 2017-10-18
            //  重写两位小数
            var zoomStr = parseInt(value * 100).toString();
            var a = zoomStr.substring(0, zoomStr.length - 2) || "0";
            var b = zoomStr.substring(zoomStr.length - 2);
            return a + "." + (b.length < 2 ? "0" + b : b);


        },
        showWait: function () { },
        closeWait: function () { },
        hint: function (param, timeOUt) {
            if (!param) return;
            var type = (typeof param).toLowerCase();
            var windowsize = param.windowsize || "auto"; //窗口尺寸的设置有两种 default默认大小 || auto自适应大小
            var tit = "",
                msg = "请确认",
                timeOUt = timeOUt ? timeOUt : 1000;
            //需要显示check成功图标的字符串数组
            if (type === "string" || type === "number") {
                msg = param;
            } else {
                tit = param.title || "";
                msg = param.msg;
            }
            var iscover = param.is_cover || false;

            if (typeof (param.timeout) == 'number') {
                timeOUt = param.timeout;
            }
            if ($.inArray(msg, success) !== -1) {
                msg = "<i class='icon success'></i>" + msg;
            }
            if ($.inArray(msg, exclamation) !== -1) {
                msg = "<i class='icon exclamation'></i>" + msg;
            }
            var hint = $("<div></div>");
            hint.html('' +
                '<section class="subwin">' +
                '   <div class="maskWin maskWinWhite"></div>' +
                '   <div class="winInfo winInfoBlack winInfoFixed">' +
                '       <div class="title">' + tit + '</div>' +
                '       <div class="msg">' + msg + '</div>' +
                '   </div>' +
                '</section>' +
                '');
            $("body").append(hint);
            $("body").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
            $("html").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
            if (!tit) {
                hint.find(".title").hide();
            }
            if (windowsize == "auto") {
                hint.find(".winInfo").removeClass("winInfoFixed");
            }
            setTimeout(function () {
                hint.remove();
                if (!iscover) {
                    $("body").css({ "overflowY": "auto", "height": "auto" });
                    $("html").css({ "overflowY": "auto", "height": "auto" });
                }
                hint = null;
                param.fn && param.fn();
            }, timeOUt);
            return hint;
        },
        toDecimal2: function (x) {
            var f = parseFloat(x);
            if (isNaN(f)) {
                return false;
            }
            var f = Math.round(x * 100) / 100;
            var s = f.toString();
            var rs = s.indexOf('.');
            if (rs < 0) {
                rs = s.length;
                s += '.';
            }
            while (s.length <= rs + 2) {
                s += '0';
            }
            return s;
        },
        showCover: function () {
            if ($("#shadow").length < 1) {
                $("body").append('<div id="shadow"></div>');
            }

            $("#shadow").css({
                'width': '100%',
                'height': '100%',
                'background': 'rgba(0, 0, 0, 0.75)',
                'position': 'absolute',
                'left': 0,
                'top': 0,
                'z-index': 3,
                'display': 'block',
            });
            $("body").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
            $("html").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
        },
        closeCover: function () {
            $("#shadow").css({ 'display': 'none' });
            $("body").css({ "overflowY": "auto", "height": "auto" });
            $("html").css({ "overflowY": "auto", "height": "auto" });
        },
        confWin: function (opts) {
            var defaultOpt = {
                title: '',
                msg: '',
                url: '',
                clearReload: false,
                clearnBut: '取消',
                okBut: '确定',
            }

            var param = $.extend(true, {}, defaultOpt, opts);

            if (!param) return;
            var tit = param.title || "";
            var msg = param.msg || "请确认";
            var url = param.url || "javascript:;";
            var res = false;    //接收确认窗口的操作结果，默认选择“取消”
            var confwin = $("<div></div>");
            confwin.html('' +
                '<section class="subwin" id="confirm-window">' +
                '   <div class="maskWin maskWinBlack"></div>' +
                '   <div class="winInfo winInfoWhite">' +
                '       <div class="title">' + tit + '</div>' +
                '       <div class="msg">' + msg + '</div>' +
                '       <div class="option">' +
                '           <a href="javascript:;" class="cancel">' + param.clearnBut + '</a>' +
                '           <a href="' + url + '" class="ok red">' + param.okBut + '</a>' +
                '       </div>' +
                '   </div>' +
                '</section>' +
                '');
            confwin.addClass("confWin");
            $("body").append(confwin);
            $("body").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
            $("html").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
            if (!tit) {
                $(".subwin .title").hide();
            }
            confwin.delegate(".cancel", "click", function () {
                confwin.res = false;
                confwin.remove();
                $("body").css({ "overflowY": "auto", "height": "auto" });
                $("html").css({ "overflowY": "auto", "height": "auto" });
                confwin = null;
                if (param.clearReload) {
                    window.location.reload();
                }
                param.cancel && param.cancel();
            });
            confwin.delegate(".ok", "click", function () {
                confwin.res = true;
                confwin.remove();
                confwin = null;
                $("body").css({ "overflowY": "auto", "height": "auto" });
                $("html").css({ "overflowY": "auto", "height": "auto" });
                param.ok && param.ok();
            });
            return confwin;
        }
    });
    $.fn.extend({
        sendCode: function (opts) {
            if (opts.validate == false) {
                return false;
            }
            var mobile = $('#' + opts.dateKey).val();
            var type = $(this).attr('data-type');
            var _this = $(this);
            var timer = null;
            var total = 60; //60s有效时间,测试用
            if (!mobile) {
                hint('请输入手机号码');
                return false;
            }
            var data = $.extend(opts.data, { 'mobile': mobile, 'type': type }, {});
            if (_this.hasClass("disable")) {
                return false;
            }

            $.post(opts.url, data, function (data) { }, 'json');
            _this.addClass("disable").text(total + "s");
            timer = setInterval(function () {
                total--;
                _this.text(total + "s");
                if (total == 0) {
                    clearInterval(timer);
                    _this.removeClass("disable").addClass("able").text("重新获取");
                }
            }, 1000);
        },
    });
})(jQuery);
/**
 * 发送验证码
 * obj:点击发送验证码的按钮  （＊必填项）
 * param:交互ajax需要的参数
 *    param:{
 *        url:""  ajax交互地址（＊必填项）
 *        data:{} ajax交互数据 （＊不填默认为空）
 *    }
 */
function sendSms(obj, param) {
    if (!obj) return;
    var param = param || {};
    if (!param.url) return;
    var data = param.data || {};
    obj.click(function () {
        $(this).sendCode(param);
    });
}

/**
 * 确认窗口
 * param:交互ajax需要的参数
 *    param:{
 *        title:""  确认窗口标题
 *        msg:""  确认窗口文本信息
 *        url:""  确定按钮的href值
 *        ok:function(){} 点击确定 （＊不填默认为空）
 *    }
 */
function confWin(param) {
    if (!param) return;
    var tit = param.title || "";
    var msg = param.msg || "请确认";
    var url = param.url || "javascript:;";
    var res = false;    //接收确认窗口的操作结果，默认选择“取消”
    var confwin = $("<div></div>");
    confwin.html('' +
        '<section class="subwin">' +
        '   <div class="maskWin maskWinBlack"></div>' +
        '   <div class="winInfo winInfoWhite">' +
        '       <div class="title">' + tit + '</div>' +
        '       <div class="msg">' + msg + '</div>' +
        '       <div class="option">' +
        '           <a href="javascript:;" class="cancel">取消</a>' +
        '           <a href="' + url + '" class="ok red">确定</a>' +
        '       </div>' +
        '   </div>' +
        '</section>' +
        '');
    confwin.addClass("confWin");
    $("body").append(confwin);
    $("body").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
    $("html").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
    if (!tit) {
        $(".subwin .title").hide();
    }
    confwin.delegate(".cancel", "click", function () {
        confwin.res = false;
        confwin.remove();
        $("body").css({ "overflowY": "auto", "height": "auto" });
        $("html").css({ "overflowY": "auto", "height": "auto" });
        confwin = null;
    });
    confwin.delegate(".ok", "click", function () {
        confwin.res = true;
        param.ok && param.ok();
        confwin.remove();
        $("body").css({ "overflowY": "auto", "height": "auto" });
        $("html").css({ "overflowY": "auto", "height": "auto" });
        confwin = null;
    });
    return confwin;
}

function alertWin(param) {
    if (!param) return;
    var type = (typeof arguments[0]).toLowerCase();
    var tit = "",
        msg = "提示",
        url = "javascript:;";
    if (type == "string" || type == "number") {
        msg = param;
    } else {
        tit = param.title || "";
        msg = param.msg || "提示";
        url = param.url || "javascript:;";
    }
    var alertWin = $("<div></div>");
    alertWin.html('' +
        '<section class="subwin">' +
        '   <div class="maskWin maskWinWhite"></div>' +
        '   <div class="winInfo winInfoBlack">' +
        '       <div class="title">' + tit + '</div>' +
        '       <div class="msg">' + msg + '</div>' +
        '       <div class="option">' +
        '           <a href="' + url + '" class="ok blue">确定</a>' +
        '       </div>' +
        '   </div>' +
        '</section>' +
        '');
    $("body").append(alertWin);
    $("body").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
    $("html").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
    if (!tit) {
        alertWin.find(".title").hide();
    }
    alertWin.delegate(".ok", "click", function () {
        param.ok && param.ok();
        alertWin.remove();
        $("body").css({ "overflowY": "auto", "position": "relative" });
        $("html").css({ "overflowY": "auto", "position": "relative" });
        alertWin = null;
    });
    return alertWin;
}

//提示信息窗口
function hint(param) {
    return $.hint(param);
}

/**
 * 等待窗口
 * param:交互ajax需要的参数
 *    param:{
 *        msg:""  确认窗口文本信息
 *    }
 */
function showWait(param) {
    return $.showWait(param);
}
//关闭等待窗口
function closeWait(obj) {
    $.closeWait(obj);
}
//加载更多
function loadList(param) {
    var defaultOpt = {
        'ls-container': 'ls-container',
        'load-more': 'load-more'
    };
    var param = $.extend(defaultOpt, param, {});
    var oLoadBtn = $('.' + param['load-more']);
    if (oLoadBtn.length == 0) return;//没有加载功能
    if (!param.url) return;//没有传入url
    //if(!param.fn) return;//没有组装函数
    param.data = param.data || "";//处理data默认值
    var win = $(window);
    var height = win.height();
    var oLoadFlag = false;      //是否加载标志
    var iPage = 2;              //当前页码
    var loadTimer = null;       //定时器
    //var urlArr = param.url.split('page') || []; //url数组
    //var assemble = param.fn;                    //组装函数
    win.bind('scroll', judge);
    loadTimer = setInterval(function () {
        if (oLoadFlag) {
            oLoadBtn.text("处理中......");
            $.ajax({
                type: "POST",
                url: param.url,
                cache: false,
                data: param.data,
                dataType: 'json',
                success: function (json) {
                    oLoadBtn.text("加载更多");
                    var list = json.html || [];
                    var pageInfo = json.pageInfo || { current_page: iPage, last_page: -1 };
                    $('#' + param['ls-container']).append(list);
                    //if($.fn.lazyload) {
                    //    $('img').lazyload();
                    //}
                    param.domchange && param.domchange();
                    if (pageInfo.current_page == pageInfo.last_page) {
                        oLoadBtn.text('没有更多了....');
                        clearInterval(loadTimer);
                        return;
                    }
                    iPage++;
                    param.data.page && (param.data.page = iPage);
                    win.bind('scroll', judge);
                }
            });
            win.off('scroll', judge);
            oLoadFlag = false;
        }
    }, 500);
    function judge() {
        var oT = win.scrollTop();    //窗口滚动高度
        var oT2 = oLoadBtn.position().top;    //按钮top值
        if (oT + height > oT2) {
            oLoadFlag = true;
        }
    }
}
function showImg(param) {
    if (!param) return;
    var type = (typeof param).toLowerCase();
    var url = '';
    if (type === "object") {
        url = param.attr("src") || "{{Theme::asset('images/default_goods.png')}}";
    } else if (type == "string") {
        url = param;
    } else {
        url = "{{Theme::asset('images/default_goods.png')}}";
    }
    var oBig = $("<div></div>");
    oBig.html('' +
        '<section class="subwin">' +
        '   <div class="maskWin" style="background:rgba(0,0,0,0.9);"></div>' +
        '   <div class="winImg">' +
        '       <img src="' + url + '" >' +
        '   </div>' +
        '</section>' +
        '');
    oBig.click(function () {
        url = "{{Theme::asset('images/default_goods.png')}}";
        oBig.remove();
    });
    $("body").append(oBig);
}
// function lazyload() {
//     var imgs = $("img");
//     var top = $(window).scrollTop() || 0;
//     var height = $(window).height();
//     imgs.each(function () {
//         var _this = $(this);
//         var top = _this.offset().top;
//         _this.attr("data-top", top);
//     });
//     $(window).scroll(function () {
//         top = $(window).scrollTop();
//         loadimg();
//     });
//     function loadimg() {
//         $("img").each(function (i, e) {
//             var _this = $(this);
//             if (parseInt(_this.attr("data-top")) < (top + height + 600)) {
//                 _this.attr("src", _this.attr("data-src"));
//             }
//         });
//     }
//
//     loadimg();
// }
function scrollToTop() {
    var top = 0;
    var wHeight = $(window).height();
    var oT = $(".function .toTop");
    $(window).on("scroll", judgeH);
    $(".function .toTop a").click(function () {
        $(window).off("scroll", judgeH);
        $("body").stop().animate({ "scrollTop": 0 }, 300, function () {
            oT.hide();
            $(window).on("scroll", judgeH);
        });
    });
    function judgeH() {
        top = $(window).scrollTop();
        if (top > 160) {
            oT.show();
        } else {
            oT.hide();
        }
    }
}
//上传图片
/**
 * 上传图片
 * param:交互ajax需要的参数
 *    param:{
 *        fileInput:""  文件按钮原生对象
 *        number:number  文件数量（可选项）
 *        url:""  图片上传地址
 *        callback:function(data){} 上传成功执行的回调函数（可选项）
 *    }
 */
function uploadImage(param) {
    var fileInput = param.fileInput;
    if (!fileInput) return;
    var url = param.url;
    if (!url) return;
    var number = param.number;
    var subwin = $("#uploadWin");
    fileInput.onchange = function () {
        subwin && subwin.hide();
        if (!this.files.length) return;
        var files = Array.prototype.slice.call(this.files);
        if (number && files.length > number) {
            alertWin("最多同时只可上传" + number + "张图片");
            return;
        }
        //出现提示窗口
        var waitwin = showWait({ msg: '请等待...' });
        files.forEach(function (file, i) {
            if (!/\/(?:jpeg|png|gif|jpg)/i.test(file.type.toLowerCase())) return;
            var reader = new FileReader();
            reader.onload = function () {
                var result = this.result;
                var img = new Image();
                img.src = result;
                //小于指定长度，直接上传
                if (result.length <= 20000000) {   //2000000不到2M的文件
                    uploadImg();
                    img = null;
                    return;
                }
                if (img.complete) {
                    imgback();
                } else {
                    img.onload = imgback();
                }
                function imgback() {
                    result = compress(img);
                    uploadImg();
                    img = null;
                    return;
                }

                function uploadImg() {
                    EXIF.getData(file, function () {
                        var ori = EXIF.getTag(this, 'Orientation');
                        switch (ori) {
                            case 1:
                                //0°
                                upload(result, file, 1, waitwin);
                                break;
                            case 3:
                                //180°
                                upload(result, file, 3, waitwin);
                                break;
                            case 6:
                                //顺时针90°
                                upload(result, file, 6, waitwin);
                                break;
                            case 8:
                                //逆时针90°
                                upload(result, file, 8, waitwin);
                                break;
                            default:
                                upload(result, file, 1, waitwin);
                                break;
                        }
                    });
                    subwin && subwin.hide();
                }
            };
            reader.readAsDataURL(file);
        })
    };
    //压缩图片
    function compress(img) {
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext("2d");
        var initSize = img.src.length;
        var width = img.width;
        var height = img.height;
        //如果图片大于四百万像素，计算压缩比并将大小压至400万以下
        var ratio;
        if ((ratio = width * height / 4000000) > 1) {
            ratio = Math.sqrt(ratio);
            width /= ratio;
            height /= ratio;
        } else {
            ratio = 1;
        }
        canvas.width = width;
        canvas.height = height;
        //	铺底色
        ctx.fillStyle = "#fff";
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        //如果图片像素大于100万则使用瓦片绘制
        var count;
        if ((count = width * height / 1000000) > 1) {
            count = ~~(Math.sqrt(count) + 1); //计算要分成多少块瓦片
            //	    计算每块瓦片的宽和高
            var nw = ~~(width / count);
            var nh = ~~(height / count);
            var tCanvas = document.createElement("canvas");
            var tctx = tCanvas.getContext("2d");
            tCanvas.width = nw;
            tCanvas.height = nh;
            for (var i = 0; i < count; i++) {
                for (var j = 0; j < count; j++) {
                    tctx.drawImage(img, i * nw * ratio, j * nh * ratio, nw * ratio, nh * ratio, 0, 0, nw, nh);
                    ctx.drawImage(tCanvas, i * nw, j * nh, nw, nh);
                }
            }
        } else {
            ctx.drawImage(img, 0, 0, width, height);
        }
        //进行最小压缩
        var ndata = canvas.toDataURL('image/jpeg', 0.1);
        //console.log('压缩前：' + initSize);
        //console.log('压缩后：' + ndata.length);
        //console.log('压缩率：' + ~~(100 * (initSize - ndata.length) / initSize) + "%");
        tCanvas.width = tCanvas.height = canvas.width = canvas.height = 0;
        return ndata;
    }

    //上传图片
    //图片上传，将base64的图片转成二进制对象，塞进formdata上传
    function upload(basestr, file, ori, win) {
        var timer = null;
        var type = file.type;
        var name = file.name;
        var text = window.atob(basestr.split(",")[1]);
        var buffer = new ArrayBuffer(text.length);
        var ubuffer = new Uint8Array(buffer);
        for (var i = 0; i < text.length; i++) {
            ubuffer[i] = text.charCodeAt(i);
        }
        var Builder = window.WebKitBlobBuilder || window.MozBlobBuilder;
        var blob;
        if (Builder) {
            var builder = new Builder();
            builder.append(buffer);
            blob = builder.getBlob(type);
        } else {
            blob = new window.Blob([buffer], { type: type });
        }
        var xhr = new XMLHttpRequest();
        var formdata = new FormData();
        formdata.append('imagefile', blob);
        formdata.append('orientation', ori);
        formdata.append('name', file.name);
        xhr.open('post', url); //后一个参数是url路径
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                clearTimeout(timer);
                if (xhr.status >= 200 && xhr.status < 300 || xhr.status == 304) {
                    var data = xhr.responseText;
                    var json = eval('(' + data + ')');
                    if (param.callback) {
                        param.callback(json);
                        win && setTimeout(function () {
                            closeWait(win);
                        }, 800);
                    } else {
                        hint(json.msg);
                        closeWait(win);
                    }
                } else {
                    alertWin("修改失败");
                    closeWait(win);
                }
            }
        };
        xhr.send(formdata);
        timer = setTimeout(function () {
            closeWait(win);
            alertWin('您的网络不给力..');
            xhr.onreadystatechange = null;
        }, 10000);
    }
}
//地址联动
function region() {
    function address(param) {
        var param = param || {};
        var dataProv = param.province || {},
            dataCity = param.city || {},
            dataArea = param.area || {};
        var pId = dataProv.id || '', //省
            cId = dataCity.id || '', //市
            aId = dataArea.id || ''; //区
        var oSelect1 = $('#province_id'), //省模块
            oSelect2 = $('#city_id'), //市模块
            oSelect3 = $('#area_id'), //区模块
            oBox = $('#areaBox'); //区信息的包裹层
        var arrayProvince = [],
            arrayCity = [],
            arrayArea = [];
        //收集省份信息
        for (var i = 0; i < area.length; i++) {
            if (area[i].id == pId) {
                //找到对应的省
                arrayCity = area[i].children;
            }
            arrayProvince[i] = {
                id: area[i].id,
                pid: area[i].pid,
                name: area[i].name
            };
        }
        //收集区级信息
        for (var j = 0; j < arrayCity.length; j++) {
            if (arrayCity[j].id == cId) {
                //找到对应的市
                arrayArea = arrayCity[j].children;
            }
        }
        //初始化地址信息
        makeAddr(arrayProvince, pId, oSelect1, "省份");
        makeAddr(arrayCity, cId, oSelect2, "城市");
        makeAddr(arrayArea, aId, oSelect3, "地区");
        if (arrayArea.length == 0) {
            oBox.hide();
        }
        oSelect1.change(function () {
            var clue = $(this).find('option:selected').attr('data-index');
            if (clue == -1) {
                arrayCity = [];
            } else {
                arrayCity = area[clue].children;
            }
            makeAddr(arrayCity, "", oSelect2, "城市");
            makeAddr([], "", oSelect3, "地区");
            oBox.hide();
        });
        oSelect2.change(function () {
            var clue = $(this).find('option:selected').attr('data-index');
            if (clue == -1) {
                arrayArea = [];
            } else {
                arrayArea = arrayCity[clue].children;
            }
            if (arrayArea.length == 0) { //如果没有三级地区
                oBox.hide();
            } else {
                oBox.show();
                makeAddr(arrayArea, "", oSelect3, "地区");
            }
        });
    }

    function makeAddr(dataArr, dataId, container, str) {
        var str = str || '';
        var html = '';
        if (!dataId) {
            html += '<option data-index="-1" value="" selected>请选择' + str + '</option>';
        }
        $(dataArr).each(function (index, ele) {
            if (ele.id == dataId) {
                html += '<option data-index="' + index + '" value="' + ele.id + '_' + ele.name + '" selected>' + ele.name + '</option>';
            } else {
                html += '<option data-index="' + index + '" value="' + ele.id + '_' + ele.name + '">' + ele.name + '</option>';
            }
        });
        container.html(html);
    }

    address({
        province: {
            id: $('#province_id').data('id')
        },
        city: {
            id: $('#city_id').data('id')
        },
        area: {
            id: $('#area_id').data('id')
        }
    });
}


//  addBy QQ 2017-10-17
//  ajax请求错误处理
//$(document).ajaxError(function(event,xhr,options,exc){
//    console.log(xhr);
//    console.log(exc);
//    if(xhr.readyState === 4) {
//        alert("网络出了点故障，稍后再试试吧～");
//    } else {
//        console.log("网络请求出错了");
//    }
//});