$(function () {

    var global = {
        add_qty_url: "/test/123",
        decrease_qty_url: "/test/123",
        clear_url: "/test/123",
        update_qty_url: "/test/123",
        del_url: "/test/123",
        home: "/test/123"
    };


    //重置商品规格
    function resetPdData(obj) {
        var oNum = obj.find(".quantity");
        var realNum = obj.find(".num").val();
        oNum.text("x " + realNum);
        obj.data("number", realNum);
    }

});