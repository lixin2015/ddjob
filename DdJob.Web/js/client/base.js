﻿(function ($) {
    window.client = {
        post: function (url, data, successCallBack, errorCallback) {
            data = "ajax=true&ts=" + new Date().getTime() + "&" + data;
            $.ajax({
                type: "post",
                url: url,
                contentType: "application/x-www-form-urlencoded",
                data: data,
                success: function (result) {
                    successCallBack && successCallBack(result);
                },
                error: function (err) {
                    if (err.status !== 0) {
                        errorCallback && errorCallback(err);
                    }
                }
            });
        },

        get: function (url, successCallBack, errorCallback) {
            var loadUrl;
            if (url.indexOf("?") < 0) {
                loadUrl = url + "?ajax=true&ts=" + new Date().getTime();
            } else {
                loadUrl = url + "&ajax=true&ts=" + new Date().getTime();
            }
            $.ajax({
                type: "get",
                url: loadUrl,
                contentType: "text/html",
                success: function (result) {
                    successCallBack && successCallBack(result);
                },
                error: function (ex) {
                    errorCallback && errorCallback(ex);
                }
            });
        },

        onEnterKeydown: function (inputSelector, callback) {
            $(inputSelector).keydown(function (e) {
                if (e.keyCode === 13) {
                    callback();
                }
            });
        },

        selectNavItem: function (selector) {
            $("ul.navbar-nav li").removeClass("active");
            $(selector).addClass("active");
        }
    };

    //client.getQueryString = function (name) {
    //    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    //    var r = window.location.search.substr(1).match(reg);
    //    if (r !== null)
    //        return unescape(r[2]);
    //    return "";
    //};

    client.getQueryString = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return decodeURI(r[2]); return null;
    }

    client.confirm = function (param) {
        if (!param) return;
        var tit = param.title || "";
        var msg = param.msg || "请确认";
        var url = param.url || "javascript:;";
        var $confirm = $("<div></div>");
        $confirm.html("" +
            '<section class="subwin">' +
            '   <div class="maskWin maskWinBlack"></div>' +
            '   <div class="winInfo winInfoWhite">' +
            '       <div class="title">' + tit + "</div>" +
            '       <div class="msg">' + msg + "</div>" +
            '       <div class="option">' +
            '           <a href="javascript:;" class="cancel">取消</a>' +
            '           <a href="' + url + '" class="ok red">确定</a>' +
            "       </div>" +
            "   </div>" +
            "</section>" +
            "");
        $confirm.addClass("confWin");
        $("body").append($confirm);
        $("body").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
        $("html").css({ "overflowY": "hidden", "position": "relative", "height": "100%" });
        if (!tit) {
            $(".subwin .title").hide();
        }
        $confirm.delegate(".cancel", "click", function () {
            $confirm.res = false;
            $confirm.remove();
            $("body").css({ "overflowY": "auto", "height": "auto" });
            $("html").css({ "overflowY": "auto", "height": "auto" });
            $confirm = null;
        });
        $confirm.delegate(".ok", "click", function () {
            $confirm.res = true;
            param.ok && param.ok();
            $confirm.remove();
            $("body").css({ "overflowY": "auto", "height": "auto" });
            $("html").css({ "overflowY": "auto", "height": "auto" });
            $confirm = null;
        });
        return $confirm;
    }

    client.showMask = function() {
        $("#screen-mask").show();
        $("body").css("overflow-y", "hidden");
    }

    client.closeMask = function () {
        $("#screen-mask").hide();
        $("body").css("overflow-y", "auto");
    }

    client.setBusy = function () {
        $("#busy-loading").show();
        $("body").css("overflow-y", "hidden");
    }

    client.closeBusy = function () {
        $("#busy-loading").hide();
        $("body").css("overflow-y", "auto");
    }

    client.loadMore = function(func) {
        $(window).scroll(function () {
            var scrollTop = $(this).scrollTop(), scrollHeight = $(document).height(), windowHeight = $(this).height();
            var positionValue = (scrollTop + windowHeight) - scrollHeight;
            if (positionValue === 0) {
                if (jQuery.isFunction(func)) {
                    func();
                }
            }
        });
    }

    client.basePath = window.location.host;
    client.relativePath = function () {
        var url = document.location.toString();
        var arrUrl = url.split("//");
        var start = arrUrl[1].indexOf("/");
        var relUrl = arrUrl[1].substring(start);
        if (relUrl.indexOf("?") !== -1) {
            relUrl = relUrl.split("?")[0];
        }
        return relUrl;
    }
    client.checkAuth = function() {
        if (window.$sys.isAuth)
            return true;
        var url = "/client/account/login?returnUrl=" + client.relativePath();
        $.confWin({ msg: "您还尚未登录！", okBut: "去登陆", url: url });
        return false;
    }

    client.tempHtml = function (template, obj) {
        return template.replace(/\$\w+\$/gi, function (matchs) {
            var returns = obj[matchs.replace(/\$/g, "")];
            return (returns + "") === "undefined" ? "" : returns;
        });
    };

    client.setCookie = function (name, value, expiredays) {
        if (!expiredays) {
            document.cookie = name + "=" + escape(value) + ";path=/";
        } else {
            var exdate = new Date();
            exdate.setDate(new Date().getDate() + expiredays);
            document.cookie = name + "=" + escape(value) + ";expires=" + exdate.toString() + ";path=/";
        }
    };

    client.getCookie = function (name) {
        if (document.cookie.length === 0)
            return "";

        var start = document.cookie.indexOf(name + "=");
        if (start >= 0) {
            start = start + name.length + 1;
            var end = document.cookie.indexOf(";", start);
            if (end === -1)
                end = document.cookie.length;
            return unescape(document.cookie.substring(start, end));
        }
        return "";
    };

    client.isNullOrEmpty = function (obj) {
        if (obj === null) return true;
        if (typeof obj === 'undefined') {
            return true;
        }
        if (typeof obj === 'string') {
            if (obj === "") {
                return true;
            }
            var reg = new RegExp("^([ ]+)|([　]+)$");
            return reg.test(obj);
        }
        return false;
    }

})(jQuery);

