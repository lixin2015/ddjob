﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using DdJob.Authorization.Roles;
using DdJob.Core.Models.Address;
using DdJob.Core.Models.Authentications;
using DdJob.Core.Models.Collections;
using DdJob.Core.Models.Common;
using DdJob.Core.Models.Notices;
using DdJob.Core.Models.Notifications;
using DdJob.Core.Models.Projects;
using DdJob.Core.Models.Public;
using DdJob.Core.Models.Settings;
using DdJob.Core.Models.Works;
using DdJob.Core.MultiTenancy;
using DdJob.Core.Users;

namespace DdJob.EntityFramework
{
    public class DdJobDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        //Users
        public IDbSet<WeChatUser> WeChatUsers { get; set; }
        public IDbSet<Employer> Employers { get; set; }
        public IDbSet<Worker> Workers { get; set; }

        //Address
        public IDbSet<Province> Provinces { get; set; }
        public IDbSet<City> Cities { get; set; }
        public IDbSet<County> Counties { get; set; }

        //Authentications
        public IDbSet<EmployerAuthentication> EmployerAuthentications { get; set; }
        public IDbSet<ClientAuthentication> ClientAuthentications { get; set; }

        //Collections
        public IDbSet<ProjectCollection> ProjectCollections { get; set; }
        public IDbSet<WorkerCollection> WorkerCollections { get; set; }

        //News & Notices
        public IDbSet<Notice> Notices { get; set; }

        //Notification
        public IDbSet<Notification> Notificationss { get; set; }

        //Project
        public IDbSet<Project> Projects { get; set; }
        public IDbSet<ProjectWorkType> ProjectWorkTypes { get; set; }

        //Public
        public IDbSet<Banner> Banners { get; set; }
        public IDbSet<CaseStudy> CaseStudys { get; set; }
        public IDbSet<CaseStudyMedia> CaseStudyMedias { get; set; }
        public IDbSet<Feedback> Feedbacks { get; set; }
        public IDbSet<Media> Medias { get; set; }
        public IDbSet<ProjectComplaint> ProjectComplaints { get; set; }
        public IDbSet<SmsCode> SmsCodes { get; set; }

        //Settings
        public IDbSet<CustomSetting> CustomSettings { get; set; }

        //Works
        public IDbSet<WorkerExpectCounty> WorkerExpectCounties { get; set; }
        public IDbSet<WorkerWorkType> WorkerWorkTypes { get; set; }
        public IDbSet<WorkType> WorkTypes { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public DdJobDbContext()
            : base("Default")
        {

        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in DdJobDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of DdJobDbContext since ABP automatically handles it.
         */
        public DdJobDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        //This constructor is used in tests
        public DdJobDbContext(DbConnection connection)
            : base(connection, true)
        {

        }
    }
}
