﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace DdJob.EntityFramework.Repositories
{
    public abstract class DdJobRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<DdJobDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected DdJobRepositoryBase(IDbContextProvider<DdJobDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class DdJobRepositoryBase<TEntity> : DdJobRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected DdJobRepositoryBase(IDbContextProvider<DdJobDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
