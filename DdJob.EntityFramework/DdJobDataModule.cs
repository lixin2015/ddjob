﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using DdJob.Core;
using DdJob.EntityFramework;

namespace DdJob
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(DdJobCoreModule))]
    public class DdJobDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DdJobDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
