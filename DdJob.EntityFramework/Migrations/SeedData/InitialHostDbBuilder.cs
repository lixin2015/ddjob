﻿using DdJob.EntityFramework;
using EntityFramework.DynamicFilters;

namespace DdJob.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly DdJobDbContext _context;

        public InitialHostDbBuilder(DdJobDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            new DefaultRegionCreator(_context).Create();
        }
    }
}
