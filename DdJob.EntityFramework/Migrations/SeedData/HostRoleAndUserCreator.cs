using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using DdJob.Authorization;
using DdJob.Authorization.Roles;
using DdJob.Core.Users;
using DdJob.EntityFramework;
using Microsoft.AspNet.Identity;
using DdJob.Enums;

namespace DdJob.Migrations.SeedData
{
    public class HostRoleAndUserCreator
    {
        private readonly DdJobDbContext _context;

        public HostRoleAndUserCreator(DdJobDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateHostRoleAndUsers();
        }

        private void CreateHostRoleAndUsers()
        {
            //add Admin role
            var adminRoleForHost = _context.Roles.FirstOrDefault(r => r.TenantId == null && r.Name == StaticRoleNames.Host.Admin);
            if (adminRoleForHost == null)
            {
                adminRoleForHost = _context.Roles.Add(new Role
                {
                    Name = StaticRoleNames.Host.Admin,
                    DisplayName = StaticRoleNames.Host.DisplayName,
                    IsStatic = true
                });
                _context.SaveChanges();

                //Grant permissions
                var permissions = PermissionFinder
                    .GetAllPermissions(new DdJobAuthorizationProvider())
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Host))
                    .ToList();

                foreach (var permission in permissions)
                {
                    _context.Permissions.Add(
                        new RolePermissionSetting
                        {
                            Name = permission.Name,
                            IsGranted = true,
                            RoleId = adminRoleForHost.Id
                        });
                }

                _context.SaveChanges();
            }

            //Admin user for tenancy host

            var adminUserForHost = _context.Users.FirstOrDefault(u => u.TenantId == null && u.UserName == "super" && u.EmailAddress == "super@DdJob.com");
            if (adminUserForHost == null)
            {
                adminUserForHost = _context.Users.Add(
                    new User
                    {
                        UserName = "super",
                        Name = "System",
                        Surname = "Administrator",
                        EmailAddress = "super@DdJob.com",
                        IsEmailConfirmed = true,
                        Password = new PasswordHasher().HashPassword(User.DefaultPassword)
                    });

                _context.SaveChanges();

                _context.UserRoles.Add(new UserRole(null, adminUserForHost.Id, adminRoleForHost.Id));

                _context.SaveChanges();
            }
        }
    }
}