﻿using System.Linq;
using Abp.Configuration;
using Abp.Localization;
using Abp.Net.Mail;
using DdJob.Core.Models.Common;
using DdJob.Core.Models.Settings;
using DdJob.EntityFramework;
using DdJob.Enums;

namespace DdJob.Migrations.SeedData
{
    public class DefaultSettingsCreator
    {
        private readonly DdJobDbContext _context;

        public DefaultSettingsCreator(DdJobDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            AddCustomSetting();

            ////Emailing
            //AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, DefaultString.SystemEmail);
            //AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, DefaultString.SystemName);

            ////Languages
            //AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "zh-CN");
        }

        public void AddCustomSetting()
        {
            //if (!_context.CustomSettings.Any(x => x.Key == CustomSettingKey.DefultReceiverAddress))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = CustomSettingKey.DefultReceiverAddress, Name = "默认收货地址", Value = "中国光大银行重庆分行-渝中区名族路168号", GroupType = GroupType.Admin });
            //}
            //if (!_context.CustomSettings.Any(x => x.Key == CustomSettingKey.WeChatPaymentUrl))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = CustomSettingKey.WeChatPaymentUrl, Name = "微信支付URL", Value = "http://test.dctpay.com", GroupType = GroupType.Admin });
            //}
            //if (!_context.CustomSettings.Any(x => x.Key == CustomSettingKey.SmsAccount))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = CustomSettingKey.SmsAccount, Name = "短信发送用户名", Value = "18908322059", GroupType = GroupType.Admin });
            //}
            //if (!_context.CustomSettings.Any(x => x.Key == CustomSettingKey.SmsPassword))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = CustomSettingKey.SmsPassword, Name = "短信发送密码", Value = "3880DE89760C6CE9C0B90C2AB46E", GroupType = GroupType.Admin });
            //}
            //if (!_context.CustomSettings.Any(x => x.Key == CustomSettingKey.SmsSign))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = CustomSettingKey.SmsSign, Name = "短信发送签名", Value = "光大银行重庆分行", GroupType = GroupType.Admin });
            //}

            //if (!_context.CustomSettings.Any(x => x.Key == WeChatKey.userid))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = WeChatKey.userid, Name = "微信支付道客巴巴UserId", Value = "99254474", GroupType = GroupType.Admin });
            //}

            //if (!_context.CustomSettings.Any(x => x.Key == WeChatKey.appid))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = WeChatKey.appid, Name = "微信公众号AppId", Value = "wx001232121212", GroupType = GroupType.Client });
            //}
            //if (!_context.CustomSettings.Any(x => x.Key == WeChatKey.appsecret))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = WeChatKey.appsecret, Name = "微信公众号Appsecret", Value = "asd123maoqilwqs", GroupType = GroupType.Client });
            //}

            //if (!_context.CustomSettings.Any(x => x.Key == WeChatKey.domain))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = WeChatKey.domain, Name = "微商城微信回调域名", Value = "test.dctpay.cn", GroupType = GroupType.Client });
            //}

            //if (!_context.CustomSettings.Any(x => x.Key == WeChatKey.returnUrl))
            //{
            //    _context.CustomSettings.Add(new CustomSetting() { Key = WeChatKey.returnUrl, Name = "微商城微信回调地址", Value = "http://test.dctpay.cn/client/home/Index", GroupType = GroupType.Client });
            //}
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}