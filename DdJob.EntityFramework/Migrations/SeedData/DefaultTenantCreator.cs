using System.Linq;
using DdJob.Core.MultiTenancy;
using DdJob.EntityFramework;

namespace DdJob.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly DdJobDbContext _context;

        public DefaultTenantCreator(DdJobDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
