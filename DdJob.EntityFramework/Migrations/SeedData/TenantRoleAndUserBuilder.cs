using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Microsoft.AspNet.Identity;
using DdJob.Authorization;
using DdJob.Authorization.Roles;
using DdJob.Core.Enums;
using DdJob.Core.Users;
using DdJob.EntityFramework;
using DdJob.Enums;

namespace DdJob.Migrations.SeedData
{
    public class TenantRoleAndUserBuilder
    {
        private readonly DdJobDbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(DdJobDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }


        private void CreateRolesAndUsers()
        {
            #region 添加角色和授权
            //Admin role
            var adminRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.DisplayName) { IsStatic = true });
                _context.SaveChanges();

                //Grant all permissions to admin role
                var permissions = PermissionFinder
                    .GetAllPermissions(new DdJobAuthorizationProvider())
                    .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Tenant))
                    .ToList();

                foreach (var permission in permissions)
                {
                    _context.Permissions.Add(
                        new RolePermissionSetting
                        {
                            TenantId = _tenantId,
                            Name = permission.Name,
                            IsGranted = true,
                            RoleId = adminRole.Id
                        });
                }

                _context.SaveChanges();
            }
            #endregion 添加角色和授权

            #region 为每个添加角色添加一个用户
            //add admin user
            var adminUser = _context.Users.FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == "admin" && u.UserType == UserType.Admin);
            if (adminUser == null)
            {
                adminUser = new User
                {
                    TenantId = _tenantId,
                    UserName = "admin",
                    Name = "admin",
                    EmailAddress = "admin@DdJob.com",
                    UserType = UserType.Admin,
                    Surname = DefaultString.DefaultSurname,
                    IsEmailConfirmed = true,
                    Password = new PasswordHasher().HashPassword(User.DefaultPassword),
                    IsActive = true
                };
                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();
            }
            #endregion 添加角色和授权
        }
    }
}