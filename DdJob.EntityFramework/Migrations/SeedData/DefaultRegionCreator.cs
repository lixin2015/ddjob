using System.Linq;
using DdJob.EntityFramework;
using System.Collections.Generic;

namespace DdJob.Migrations.SeedData
{
    public class DefaultRegionCreator
    {
        private readonly DdJobDbContext _context;

        public DefaultRegionCreator(DdJobDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateRegions();
        }

        private void CreateRegions()
        {
            //Default Regions   Province  County  City
        }
    }
}
