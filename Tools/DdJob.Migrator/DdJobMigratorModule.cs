using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using DdJob.EntityFramework;

namespace DdJob.Migrator
{
    [DependsOn(typeof(DdJobDataModule))]
    public class DdJobMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<DdJobDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}