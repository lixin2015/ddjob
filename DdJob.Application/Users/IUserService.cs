using System.Threading.Tasks;
using DdJob.Application.Pageable;
using DdJob.Application.Users.Dto;
using DdJob.Core.Users;

namespace DdJob.Application.Users
{
    public interface IUserService : IServiceBase<User, long, UserDto>
    {
    }
}