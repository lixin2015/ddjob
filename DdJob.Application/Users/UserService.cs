using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.UI;
using DdJob.Application.Pageable;
using DdJob.Application.Users.Dto;
using DdJob.Authorization.Roles;
using DdJob.Core.Users;
using Microsoft.AspNet.Identity;

namespace DdJob.Application.Users
{
    /* THIS IS JUST A SAMPLE. */
    //[AbpAuthorize(PermissionNames.Pages_Users)]
    public class UserService : ServiceBase<User, long, UserDto>, IUserService
    {
        private readonly IRepository<User, long> _userRepository;

        public UserService(IRepository<User, long> userRepository)
        {
            _userRepository = userRepository;
        }
    }
}