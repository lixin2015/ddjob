using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DdJob.Core.Enums;
using DdJob.Core.Users;
using DdJob.Enums;
using DdJob.Utilities;

namespace DdJob.Application.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        public string Name { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }

        public string PhoneNumberText { get { return PhoneNumber.DecryptPhone(); } }

        public int? CustomerIntegral { get; set; }

        public DateTime? LastLoginTime { get; set; }
        public string LastLoginTimeText
        {
            get { return LastLoginTime.ToReportDateTimeFormat(); }
        }

        public bool IsActive { get; set; }
        public string IsActiveText
        {
            get { return IsActive.ToYesNoChn(); }
        }

        public UserType UserType { get; set; }
        public string UserTypeText
        {
            get { return EnumHelper.GetEnumDescription(UserType); }
        }

        public DateTime CreationTime { get; set; }
        public string CreationTimeText
        {
            get { return CreationTime.ToReportDateTimeFormat(); }
        }

        public long? BankId { get; set; }
        public long? BusinessId { get; set; }
        public long? BranchId { get; set; }
        public string RoleNames { get; set; }
    }
}