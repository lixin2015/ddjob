using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Users;
using DdJob.Application.Pageable;
using DdJob.Core.Enums;
using DdJob.Core.Users;
using DdJob.Enums;

namespace DdJob.Application.Users.Dto
{
    public class UserPagedRequestInput : PagedRequestInput
    {
        public UserType UserType { get; set; }
    }

    public class UserCreateOrUpdateInput
    {
        [Required]
        public long Id { get; set; }
        [Required]
        [StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        //[Required]
        [StringLength(64)]
        public string PhoneNumber { get; set; }

        public UserType UserType { get; set; }

        public bool IsActive { get; set; }
    }

    public class UpdateUserRolesInput
    {
        [Required]
        public long UserId { get; set; }
        public string RoleNames { get; set; }
    }

    public class ChangePasswordInput
    {
        /// <summary>
        /// ԭ����
        /// </summary>
        [Required]
        [MinLength(6)]
        [MaxLength(User.MaxPlainPasswordLength)]
        public string OldPassword { get; set; }
        /// <summary>
        /// ������
        /// </summary>
        [Required]
        [MinLength(6)]
        [MaxLength(User.MaxPlainPasswordLength)]
        public string NewPassword { get; set; }
    }

    public class ChangePasswordCommonInput
    {
        public long UserId { get; set; }
        public string Password { get; set; }
    }

    public class IdentifyCodeInput
    {
        [Required]
        public string PhoneNumber { get; set; }

        [Required]
        public SMSCodeType SMSCodeType { get; set; }
    }

    public class MobileForgotPasswordInput
    {
        public string UserName { get { return Mobile.EncryptPhone(); } }

        [Required]
        [MaxLength(11)]
        public string Mobile { get; set; }
        [Required]
        [StringLength(User.MaxPlainPasswordLength)]
        public string Password { get; set; }
        [Required]
        [StringLength(6)]
        public string VerifyCode { get; set; }
        [Required]
        [StringLength(4)]
        public string ImageCode { get; set; }
    }
}