using System.IO;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using DdJob.Application.Public.Dto;
using DdJob.Core.Models.Common;
using DdJob.Enums;

namespace DdJob.Application.Public
{
    public class MediaService : ServiceBase<Media, long, MediaDto>, IMediaService
    {
        private readonly IRepository<Media, long> _mediaRepository;

        public MediaService(IRepository<Media, long> mediaRepository)
        {
            _mediaRepository = mediaRepository;
        }

        public async Task<long> SaveMediaFile(MediaDto input)
        {
            if (string.IsNullOrEmpty(input.Path))
            {
                return 0;
            }
            var media = new Media()
            {
                Path = input.Path,
                MediaType = input.MediaType,
                IsActive = true
            };
            return await _mediaRepository.InsertAndGetIdAsync(media);
        }

        public long SaveMediaFile(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return 0;
            }
            var media = new Media()
            {
                Path = path,
                MediaType = MediaType.Image,
                IsActive = true
            };
            var id = _mediaRepository.InsertAndGetId(media);
            return id;
        }

        public MediaDto UploadAttachmentFile(FileInfo fileinfo, string dirTempPath, string fileExt, string localFilePath)
        {
            //var file = fileinfo.Create();

            //var ext = Path.GetExtension(orfilename);
            //var fileName = Guid.NewGuid() + ext;
            //fileItem.SaveAs(Path.Combine(dirTempPath, fileName));
            return null;

            //throw new System.NotImplementedException();
        }
    }
}