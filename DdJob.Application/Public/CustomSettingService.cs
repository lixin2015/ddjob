﻿using Abp.Domain.Repositories;
using Abp.UI;
using DdJob.Application.Pageable;
using System.Linq;
using System.Threading.Tasks;
using DdJob.Core.Models.Settings;

namespace DdJob.Application.Public
{
    public class CustomSettingService : ServiceBase<CustomSetting, long, CustomSettingDto>, ICustomSettingService
    {
        private readonly IRepository<CustomSetting, long> _customSettingRepository;
        public CustomSettingService(IRepository<CustomSetting, long> customSettingRepository)
        {
            _customSettingRepository = customSettingRepository;
        }

        public PagedResultOutput<CustomSettingDto> GetCustomSettingByPage(CustomSettingPagedRequestInput input)
        {
            var data = _customSettingRepository.GetAll().Where(x => x.Editable);

            if (!string.IsNullOrEmpty(input.Key))
            {
                input.Key = input.Key.Trim();
                data = data.Where(t => t.Name.Contains(input.Key) || t.Key.Contains(input.Key));
            }
            if (input.GroupType != Enums.SettingGroupType.None)
            {
                data = data.Where(t => t.GroupType == input.GroupType);
            }
            data = data.OrderByDescending(t => t.CreationTime);
            var temp = GetAll(input, data);
            return temp;
        }

        public async Task<long> CreateOrUpdate(CustomSettingCreateOrUpdateInput input)
        {
            if (_customSettingRepository.GetAll().Where(t => t.Key == input.Key && t.Id != input.Id).Count() > 0)
            {
                throw new UserFriendlyException("Key已经存在");
            }

            if (input.Id > 0)
            {
                var model = await _customSettingRepository.GetAsync(input.Id);
                if (model == null)
                {
                    throw new UserFriendlyException("设置信息不存在");
                }
                model.Value = input.Value;
                HttpRuntimeCache.Remove(input.Key);//如果有修改，清除缓存数据
                await _customSettingRepository.UpdateAsync(model);
            }
            return input.Id;
        }

        /// <summary>
        /// 通过key得到系统设置数据
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public SettingBaseDto GetByKey(string key)
        {
            if (string.IsNullOrEmpty(key))
                return null;

            var settingData = HttpRuntimeCache.Get(key);
            if (settingData != null)
                return (SettingBaseDto)settingData;

            var setting = _customSettingRepository.FirstOrDefault(x => x.Key == key);
            if (setting != null)
            {
                var settingBase = AutoMapper.Mapper.Map<CustomSetting, SettingBaseDto>(setting);
                HttpRuntimeCache.Set(key, settingBase);
                return settingBase;
            }

            return null;
        }
    }
}
