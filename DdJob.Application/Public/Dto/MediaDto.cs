﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DdJob.Core.Models.Common;
using DdJob.Enums;

namespace DdJob.Application.Public.Dto
{
    [AutoMapFrom(typeof(Media))]
    public class MediaDto : EntityDto<long>
    {
        public MediaType MediaType { get; set; }

        public string Path { get; set; }
    }
}
