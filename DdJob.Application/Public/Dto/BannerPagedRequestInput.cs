﻿using DdJob.Application.Pageable;

namespace DdJob.Application.Public.Dto
{
    public class BannerPagedRequestInput : PagedRequestInput
    {
        public string LinkUrl { get; set; }
    }


    public class BannerCreateOrUpdateInput
    {
        public long Id { get; set; }
        public long MediaId { get; set; }

        public long? CategoryId { get; set; }

        public string LinkUrl { get; set; }
    }
}
