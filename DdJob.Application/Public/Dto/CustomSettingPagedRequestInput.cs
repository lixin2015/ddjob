﻿using DdJob.Application.Pageable;
using DdJob.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DdJob.Application.Public
{
    public class CustomSettingPagedRequestInput : PagedRequestInput
    {
        public SettingGroupType GroupType { get; set; }
    }

    public class CustomSettingCreateOrUpdateInput
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public SettingGroupType GroupType { get; set; }
    }
}
