﻿using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DdJob.Core.Models.Common;

namespace DdJob.Application.Public.Dto
{
    [AutoMapFrom(typeof(Banner))]
    public class BannerDto : BannerBaseDto
    {
        public long MediaId { get; set; }

        public string MediaName { get; set; }

        public long CategoryId { get; set; }

        public string CategoryParentId { get; set; }

        public DateTime CreationTime { get; set; }

        public string CreationTimeText
        {
            get { return CreationTime.ToString(DefaultString.DefaultLongTime); }
        }
    }

    public class BannerBaseDto : EntityDto<long>
    {
        public string CategoryName { get; set; }

        public string MediaPath { get; set; }

        public string LinkUrl { get; set; }
    }
}
