﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DdJob.Enums;
using DdJob.Utilities;
using System;
using DdJob.Core.Models.Common;
using DdJob.Core.Models.Settings;

namespace DdJob.Application.Public
{
    [AutoMapFrom(typeof(CustomSetting))]
    public class CustomSettingDto : EntityDto<long>
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public SettingGroupType GroupType { get; set; }

        public string GroupTypeText
        {
            get { return EnumHelper.GetEnumDescription(GroupType); }
        }
        public DateTime CreationTime { get; set; }
        public string CreationTimeText
        {
            get { return CreationTime.ToString(DefaultString.DefaultLongTime); }
        }
    }

    [AutoMapFrom(typeof(CustomSetting))]
    public class SettingBaseDto : EntityDto<long>
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
