using System.IO;
using System.Threading.Tasks;
using DdJob.Application.Public.Dto;
using DdJob.Core.Models.Common;

namespace DdJob.Application.Public
{
    public interface IMediaService : IServiceBase<Media, long, MediaDto>
    {
        Task<long> SaveMediaFile(MediaDto input);

        long SaveMediaFile(string path);

        MediaDto UploadAttachmentFile(FileInfo fileinfo, string dirTempPath, string fileExt, string localFilePath);
    }
}