﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DdJob.Application.Pageable;
using DdJob.Application.Public.Dto;
using DdJob.Core.Models.Common;

namespace DdJob.Application.Public
{
    public interface IBannerService : IServiceBase<Banner, long, BannerDto>
    {
        /// <summary>
        /// 分页获取Banner数据
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagedResultOutput<BannerDto> GetBannerByPage(BannerPagedRequestInput input);


        /// <summary>
        /// 创建或者更新Business
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<long> CreateOrUpdate(BannerCreateOrUpdateInput input);
    }
}
