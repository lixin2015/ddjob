﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.UI;
using DdJob.Application.Pageable;
using DdJob.Application.Public.Dto;
using DdJob.Core.Models.Common;

namespace DdJob.Application.Public
{
    public class BannerService : ServiceBase<Banner, long, BannerDto>, IBannerService
    {
        private readonly IRepository<Banner, long> _bannerRepository;
        public BannerService(IRepository<Banner, long> bannerRepository)
        {
            _bannerRepository = bannerRepository;
        }

        public async Task<long> CreateOrUpdate(BannerCreateOrUpdateInput input)
        {
            input.CategoryId = input.CategoryId == 0 ? null : input.CategoryId;
            if (input.Id > 0)
            {
                var model = await _bannerRepository.GetAsync(input.Id);
                if (model == null)
                {
                    throw new UserFriendlyException("Banner不存在");
                }
                model.MediaId = input.MediaId;
                model.LinkUrl = input.LinkUrl;
                await _bannerRepository.UpdateAsync(model);
            }
            else
            {
                var model = new Banner
                {
                    MediaId = input.MediaId,
                    LinkUrl = input.LinkUrl,
                    CreatorUserId = AbpSession.UserId
                };
                input.Id = await _bannerRepository.InsertAndGetIdAsync(model);
            }
            return input.Id;
        }

        public async Task<Banner> GetBannerById(long id)
        {
            return await _bannerRepository.GetAsync(id);
        }

        public PagedResultOutput<BannerDto> GetBannerByPage(BannerPagedRequestInput input)
        {
            var data = _bannerRepository.GetAll();

            if (!string.IsNullOrEmpty(input.LinkUrl))
            {
                data = data.Where(t => t.LinkUrl.Contains(input.LinkUrl));
            }
            return GetAll(input, data);
        }
    }
}
