﻿using DdJob.Application.Pageable;
using System.Threading.Tasks;
using DdJob.Core.Models.Settings;

namespace DdJob.Application.Public
{
    public interface ICustomSettingService : IServiceBase<CustomSetting, long, CustomSettingDto>
    {
        PagedResultOutput<CustomSettingDto> GetCustomSettingByPage(CustomSettingPagedRequestInput input);

        Task<long> CreateOrUpdate(CustomSettingCreateOrUpdateInput input);

        SettingBaseDto GetByKey(string key);
    }
}
