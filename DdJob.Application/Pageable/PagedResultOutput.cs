﻿using System.Collections.Generic;

namespace DdJob.Application.Pageable
{
    public class PagedResultOutput<T>
    {
        public IEnumerable<T> Data { get; set; }
        public int Total { get; set; }
        /// <summary>
        /// Page Number
        /// </summary>
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public bool IsEnd
        {
            get
            {
                return Page * PageSize >= Total || Skip + Take >= Total;
            }
        }

        public PagedResultOutput()
        {

        }
        public PagedResultOutput(int total, IReadOnlyList<T> items)
        {
            Total = total;
            Data = items;
        }
    }
}