﻿namespace DdJob.Application.Pageable
{
    public class PagedRequestInput
    {
        /// <summary>
        /// 
        /// </summary>
        public int Take { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Skip { get; set; }
        /// <summary>
        /// 页码
        /// </summary>
        public int Page { get; set; }
        /// <summary>
        /// 每页展示数量
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// 关键字搜索
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// 排序字段名
        /// </summary>
        public string Orderby { get; set; }
        /// <summary>
        /// 是否倒序
        /// </summary>
        public bool IsDesc { get; set; }
    }
}