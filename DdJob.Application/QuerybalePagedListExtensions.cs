using System.Collections.Generic;
using System.Linq;
using Abp.Domain.Entities;
using DdJob.Application.Pageable;

namespace DdJob
{
    public static class QuerybalePagedListExtensions
    {
        public static PagedResultOutput<TDestination> ToPagedResultOutput<TSource, TPrimaryKey, TDestination>(
            this IQueryable<TSource> query, int skipCount, int page, int pageSize, bool asc = false)
            where TSource : Entity<TPrimaryKey>
        {
            var totalCount = query.Count();
            var ret = asc ? query.OrderBy(d => d.Id).Skip(skipCount).Take(pageSize).ToList() : query.OrderByDescending(d => d.Id).Skip(skipCount).Take(pageSize).ToList();
            var result = AutoMapper.Mapper.Map<List<TSource>, List<TDestination>>(ret);
            return new PagedResultOutput<TDestination>(totalCount, result)
            {
                Page = page,
                PageSize = pageSize,
            };
        }

        public static PagedResultOutput<TEntity> ToPagedResultOutput<TEntity>(this IQueryable<TEntity> query,
            int skipCount, int page, int pageSize)
        {
            var totalCount = query.Count();
            var result = query.Skip(skipCount).Take(pageSize).ToList();
            return new PagedResultOutput<TEntity>(totalCount, result)
            {
                Page = page,
                PageSize = pageSize,
            };
        }
    }
}