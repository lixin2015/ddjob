﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;
using DdJob.Application.Users.Dto;
using DdJob.Core;
using DdJob.Core.Users;
//using DdJob.Cards.Dto;
//using DdJob.Exchange.Dto;

namespace DdJob
{
    [DependsOn(typeof(DdJobCoreModule), typeof(AbpAutoMapperModule))]
    public class DdJobApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(mapper =>
            {
                //Add your custom AutoMapper mappings here...
                //User
                mapper.CreateMap<User, UserDto>()
                .ForMember(dest => dest.PhoneNumber, opt => opt.NullSubstitute(""));
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            //AutoMapperWebConfig.Configure();//一次性加载所有映射配置
        }
    }
}
