﻿using System.Threading.Tasks;
using DdJob.Application.Pageable;
using DdJob.Application.Roles.Dto;
using DdJob.Authorization.Roles;

namespace DdJob.Application.Roles
{
    public interface IRoleAppService : IServiceBase<Role, int, RoleDto>
    {
        PagedResultOutput<RoleDto> GetRolesByPage(RolePagedRequestInput input);

        Task<int> CreateOrUpdate(RoleCreateOrUpdateInput input);

        Task UpdateRolePermissions(UpdateRolePermissionsInput input);

        PermissionDto GetAllPermissions();

        Task<string> GetGrantedPermissionsById(int roleId);
    }
}
