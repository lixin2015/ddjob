﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Collections.Extensions;
using Abp.Domain.Uow;
using Abp.UI;
using DdJob.Application.Pageable;
using DdJob.Application.Roles.Dto;
using DdJob.Authorization;
using DdJob.Authorization.Roles;

namespace DdJob.Application.Roles
{
    /* THIS IS JUST A SAMPLE. */
    public class RoleAppService : ServiceBase<Role, int, RoleDto>, IRoleAppService
    {
        private readonly RoleManager _roleManager;
        private readonly IPermissionManager _permissionManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public RoleAppService(RoleManager roleManager, IPermissionManager permissionManager, IUnitOfWorkManager unitOfWorkManager)
        {
            _roleManager = roleManager;
            _permissionManager = permissionManager;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public PagedResultOutput<RoleDto> GetRolesByPage(RolePagedRequestInput input)
        {
            var banks = _roleManager.Roles;
            if (!string.IsNullOrEmpty(input.Key))
                banks =
                    banks.Where(
                        c =>
                            c.Name.Contains(input.Key) || c.Name.Contains(input.Key) ||
                            c.DisplayName.Contains(input.Key));
            return GetAll(input, banks);
        }

        public async Task<int> CreateOrUpdate(RoleCreateOrUpdateInput input)
        {
            if (input.Id > 0)
            {
                var model = await _roleManager.GetRoleByIdAsync(input.Id);
                if (model != null)
                {
                    if (model.IsStatic)
                    {
                        throw new UserFriendlyException("该权限不可编辑");
                    }
                    var exist = await _roleManager.FindByNameAsync(input.Name);
                    if (exist != null && exist.Id != model.Id)
                        throw new UserFriendlyException("该角色名称已被使用");
                    model.Name = input.Name;
                    model.DisplayName = input.DisplayName;
                    await _roleManager.UpdateAsync(model);
                }
                return input.Id;
            }
            else
            {
                try
                {
                    var model = await _roleManager.FindByNameAsync(input.Name);
                    if (model != null)
                        throw new UserFriendlyException("该角色名称已被使用");
                    model = new Role(null, input.Name, input.DisplayName);
                    var id = _roleManager.InsertAndGetId(model);

                    var role = await _roleManager.GetRoleByIdAsync(id);
                    if (role == null)
                        return 0;
                    //设置默认权限
                    var permission = _permissionManager.GetPermission(PermissionNames.Pages);
                    await _roleManager.GrantPermissionAsync(role, permission);

                    return id;
                }
                catch (Exception ex)
                {
                    throw new UserFriendlyException(ex.Message);
                }
            }
        }

        public async Task UpdateRolePermissions(UpdateRolePermissionsInput input)
        {
            var role = await _roleManager.GetRoleByIdAsync(input.Id);
            var permissions = input.Permissions.Split('|');
            var grantedPermissions = _permissionManager
                .GetAllPermissions()
                .Where(p => permissions.Contains(p.Name))
                .ToList();

            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);
        }

        public PermissionDto GetAllPermissions()
        {
            var entity = _permissionManager.GetAllPermissions(true).FirstOrDefault(c=>c.Name == PermissionNames.Pages);

            var model = GetPermission(entity);

            return model;
        }

        public async Task<string> GetGrantedPermissionsById(int roleId)
        {
            var items = await _roleManager.GetGrantedPermissionsAsync(roleId);
            return items == null ? "" : items.Select(c => c.Name).ToList().JoinAsString("|");
        }

        private PermissionDto GetPermission(Permission input)
        {
            if (input == null)
                return null;
            var dto = new PermissionDto()
            {
                Name = input.Name,
                DisplayName = L(input.Name),
            };
            if (!input.Children.Any())
                return dto;

            dto.Children = new List<PermissionDto>();
            foreach (var child in input.Children)
            {
                var per = GetPermission(child);
                dto.Children.Add(per);
            }
            return dto;
        }
    }
}