using System.ComponentModel.DataAnnotations;
using Abp.Authorization.Roles;
using DdJob.Application.Pageable;
using DdJob.Authorization.Roles;

namespace DdJob.Application.Roles.Dto
{
    public class RolePagedRequestInput : PagedRequestInput
    {
    }

    public class RoleCreateOrUpdateInput
    {
        public int Id { get; set; }

        [Required]
        [StringLength(AbpRoleBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(Role.MaxDisplayNameLength)]
        public string DisplayName { get; set; }
    }

    public class UpdateRolePermissionsInput
    {
        [Range(1, int.MaxValue)]
        public int Id { get; set; }

        [Required]
        public string Permissions { get; set; }
    }
}