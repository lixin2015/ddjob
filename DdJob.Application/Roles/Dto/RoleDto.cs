using System;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using DdJob.Authorization.Roles;

namespace DdJob.Application.Roles.Dto
{
    public class RoleBaseDto : EntityDto<int>
    {
        public string Name { get; set; }
    }

    [AutoMapFrom(typeof(Role))]
    public class RoleDto : RoleBaseDto
    {
        public string DisplayName { get; set; }

        public bool IsStatic{ get; set; }
        public string IsStaticText
        {
            get { return IsStatic ? "��" : "��"; }
        }

        public DateTime CreationTime { get; set; }
        public string CreationTimeText
        {
            get { return CreationTime.ToString(DefaultString.DefaultShortDate); }
        }
    }
}