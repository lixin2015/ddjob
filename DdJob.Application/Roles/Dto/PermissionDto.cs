using System.Collections.Generic;
using Abp.Authorization;
using Abp.AutoMapper;

namespace DdJob.Application.Roles.Dto
{
    [AutoMapFrom(typeof(Permission))]
    public class PermissionDto
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public IList<PermissionDto> Children { get; set; }
    }
}