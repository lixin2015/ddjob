﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using DdJob.Application.MultiTenancy.Dto;

namespace DdJob.Application.MultiTenancy
{
    public interface ITenantAppService : IApplicationService
    {
        ListResultDto<TenantListDto> GetTenants();

        //Task CreateTenant(CreateTenantInput input);
    }
}
