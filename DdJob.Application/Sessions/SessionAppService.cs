﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Auditing;
using Abp.Authorization;
using Abp.AutoMapper;
using DdJob.Application.Roles.Dto;
using DdJob.Application.Sessions.Dto;
using DdJob.Authorization.Roles;

namespace DdJob.Application.Sessions
{
    [AbpAuthorize]
    public class SessionAppService : DdJobAppServiceBase, ISessionAppService
    {
        private readonly RoleManager _roleManager;
        public SessionAppService(RoleManager roleManager)
        {
            _roleManager = roleManager;
        }

        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                User = (await GetCurrentUserAsync()).MapTo<UserLoginInfoDto>()
            };

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = (await GetCurrentTenantAsync()).MapTo<TenantLoginInfoDto>();
            }

            return output;
        }

        public async Task<List<PermissionDto>> GetCurrentAllGrantedPermissions()
        {
            var permissions = await UserManager.GetGrantedPermissionsAsync(await GetCurrentUserAsync());
            var list = permissions.Select(per => new PermissionDto()
            {
                Name = per.Name,
                DisplayName = L(per.Name)
            }).ToList();

            return list;
        }
    }
}