﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using DdJob.Application.Roles.Dto;
using DdJob.Application.Sessions.Dto;

namespace DdJob.Application.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();

        Task<List<PermissionDto>> GetCurrentAllGrantedPermissions();
    }
}
