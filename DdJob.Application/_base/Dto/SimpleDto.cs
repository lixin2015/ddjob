﻿namespace DdJob.Application
{
    public class SimpleDto
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }

        public SimpleDto() { }

        public SimpleDto(long id, string name, bool isSelected = false)
        {
            Id = id;
            Name = name;
            IsSelected = isSelected;
        }
    }
}
