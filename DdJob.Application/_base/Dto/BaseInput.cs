﻿namespace DdJob.Application
{
    public class BaseInput<TPrimaryKey>
    {
        public TPrimaryKey Id { get; set; }
    }
}
