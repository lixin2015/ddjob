﻿using System;

namespace DdJob.Application
{
    [Serializable]
    public class BatchDeleteEntitiesInput<TPrimaryKey>
    {
        public TPrimaryKey[] Ids { get; set; }
    }

    #region 导出数据Loading效果控制

    public interface IExportCookLoading
    {
        string LoadingId { get; set; }
    }

    #endregion
}