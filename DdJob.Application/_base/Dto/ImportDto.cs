﻿using System.ComponentModel.DataAnnotations;

namespace DdJob.Application
{
    public class ImportDto
    {
        [Required]
        public string FilePath { get; set; }
    }

    public class ImportWhiteListDto
    {
        [Required]
        public string FilePath { get; set; }

        public int ProductId { get; set; }
    }
}
