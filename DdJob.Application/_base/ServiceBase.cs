using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Dependency;
using Abp.Domain.Entities;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using DdJob.Application.Pageable;

namespace DdJob.Application
{
    public class ServiceBase : DdJobAppServiceBase, IServiceBase
    {
        protected IRepository<TEntity, TPrimiaryKey> GetRepository<TEntity, TPrimiaryKey>()
            where TEntity : Entity<TPrimiaryKey>, new()
        {
            return IocManager.Instance.Resolve<IRepository<TEntity, TPrimiaryKey>>();
        }
    }

    public class ServiceBase<TEntity, TPrimaryKey> : ServiceBase, IServiceBase<TEntity, TPrimaryKey>
        where TEntity : Entity<TPrimaryKey>, new()
    {
        private IRepository<TEntity, TPrimaryKey> _repository;

        protected IRepository<TEntity, TPrimaryKey> Repository
        {
            get
            {
                return _repository ?? (_repository = IocManager.Instance.Resolve<IRepository<TEntity, TPrimaryKey>>());
            }
        }

        public virtual async Task Delete(EntityDto<TPrimaryKey> input)
        {
            await Repository.DeleteAsync(input.Id);
        }

        public virtual async Task BatchDelete(BatchDeleteEntitiesInput<TPrimaryKey> input)
        {
            await Repository.DeleteAsync(d => input.Ids.Contains(d.Id));
        }

        #region IServiceBase<TEntity> Members
        [UnitOfWork]
        protected TEntity GetEntityById(TPrimaryKey id)
        {
            return Repository.Get(id);
        }

        #endregion
    }

    public class ServiceBase<TEntity, TPrimaryKey, TDto> : ServiceBase<TEntity, TPrimaryKey>,
        IServiceBase<TEntity, TPrimaryKey, TDto>
        where TEntity : Entity<TPrimaryKey>, new()
        where TDto : IEntityDto<TPrimaryKey>
    {
        public virtual TDto GetEntityDtoById(TPrimaryKey id)
        {
            var entity = GetEntityById(id);
            return AutoMapper.Mapper.Map<TEntity, TDto>(entity);
        }

        public virtual TEntity GetById(TPrimaryKey id)
        {
            return GetEntityById(id);
        }

        public virtual ListResultDto<TDto> GetAll()
        {
            var retval = Repository.GetAllList();
            return new ListResultDto<TDto>(AutoMapper.Mapper.Map<IList<TEntity>, IList<TDto>>(retval).ToList());
        }
        public virtual PagedResultOutput<TDto> GetAll(PagedRequestInput input)
        {
            return Repository.GetAll()
                .ToPagedResultOutput<TEntity, TPrimaryKey, TDto>(input.Skip, input.Page, input.PageSize);
        }
        protected virtual PagedResultOutput<TDto> GetAll(PagedRequestInput input, IQueryable<TEntity> query)
        {
            return query.ToPagedResultOutput<TEntity, TPrimaryKey, TDto>(input.Skip, input.Page, input.PageSize);
        }
    }
}