﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using DdJob.Application.Pageable;

namespace DdJob.Application
{
    public interface IServiceBase : IApplicationService
    {
    }

    public interface IServiceBase<out TEntity, TPrimaryKey> : IServiceBase
        where TEntity : IEntity<TPrimaryKey>
    {
        Task BatchDelete(BatchDeleteEntitiesInput<TPrimaryKey> input);
        Task Delete(EntityDto<TPrimaryKey> input);
    }

    public interface IServiceBase<out TEntity, TPrimaryKey, TDto> : IServiceBase<TEntity, TPrimaryKey>
        where TEntity : IEntity<TPrimaryKey>, new()
        where TDto : IEntityDto<TPrimaryKey>
    {
        TDto GetEntityDtoById(TPrimaryKey id);
        TEntity GetById(TPrimaryKey id);
        ListResultDto<TDto> GetAll();
        PagedResultOutput<TDto> GetAll(PagedRequestInput input);
    }
}