﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using DdJob.Application;
using NPOI.SS.UserModel;

namespace DdJob.Api
{
    public static class ExportResultExecute
    {
        public static HttpResponseMessage ExecuteResult(this IExportCookLoading load, Task<IWorkbook> workBook, string fileName)
        {
            var tempFilePath = HostingEnvironment.MapPath("~/File/Export/" + fileName);
            FileStream fs;
            using (fs = new FileStream(tempFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                workBook.Result.Write(fs);
            }
            var response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StreamContent(new FileStream(tempFilePath, FileMode.Open, FileAccess.Read))
            };
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };

            #region 数据处理完成，回写Cookie值

            var cookie = new HttpCookie(load.LoadingId, "true") { Expires = DateTime.Now.AddMinutes(5) };
            HttpContext.Current.Response.Cookies.Add(cookie);

            #endregion
            return response;
        }
    }
}