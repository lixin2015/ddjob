﻿using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;
using NPOI.SS.UserModel;

namespace DdJob.Api
{
    public class FileResultBuilder
    {
        public static string BuildExcelResult(Task<IWorkbook> workBook, string fileName)
        {
            var tempFilePath = HostingEnvironment.MapPath("~/Media/Files/Export/" + fileName);
            FileStream fs;
            using (fs = new FileStream(tempFilePath, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                workBook.Result.Write(fs);
            }
            return "/Media/Files/Export/" + fileName;
        }
    }
}
