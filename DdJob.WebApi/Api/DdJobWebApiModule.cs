﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Abp.Configuration.Startup;
using Abp.Modules;
using Abp.WebApi;
using Swashbuckle.Application;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Net.Http.Formatting;

namespace DdJob.Api
{
    [DependsOn(typeof(AbpWebApiModule), typeof(DdJobApplicationModule))]
    public class DdJobWebApiModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            //Configuration.Modules.AbpWebApi().DynamicApiControllerBuilder
            //    .ForAll<IApplicationService>(typeof(DdJobApplicationModule).Assembly, "app")
            //    .Build();

            Configuration.Modules.AbpWebApi().HttpConfiguration.Filters.Add(new HostAuthenticationFilter("Bearer"));

            //API时间格式化处理和NULL处理
            var iosDateConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };
            var serializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new UnderlineSplitContractResolver()
            };
            serializerSettings.Converters.Add(iosDateConverter);
            var formatter = new JsonMediaTypeFormatter { Indent = true, SerializerSettings = serializerSettings };
            Configuration.Modules.AbpWebApi().HttpConfiguration.Formatters.Insert(0, formatter);
            //API时间格式化处理和NULL处理 结束

            //初始化SwaggerUi
            ConfigureSwaggerUi();
        }

        /// <summary>
        /// 配置SwaggerUi
        /// </summary>
        private void ConfigureSwaggerUi()
        {
            Configuration.Modules.AbpWebApi().HttpConfiguration
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v1.0", "DdJob web API document");
                    c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    //将application层中的注释添加到SwaggerUI中
                    var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;

                    var commentsFileName = "bin//DdJob.Application.XML";
                    var commentsFile = Path.Combine(baseDirectory, commentsFileName);
                    //将注释的XML文档添加到SwaggerUI中
                    c.IncludeXmlComments(commentsFile);
                }).EnableSwaggerUi("api/help/{*assetPath}");
        }
    }

    public class UnderlineSplitContractResolver : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
        private string CamelCaseToUnderlineSplit(string name)
        {
            return name.ToLower();
        }

        //Solve API NULL And time format issues
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return type.GetProperties()
                   .Select(p =>
                   {
                       var jp = base.CreateProperty(p, memberSerialization);
                       if (jp.PropertyType == typeof(System.String))
                           jp.ValueProvider = new NullToEmptyStringValueProvider(p);
                       if (jp.PropertyType.ToString().Contains("System.DateTime"))
                           jp.Converter = new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };
                       return jp;
                   }).ToList();
        }
    }

    public class NullToEmptyStringValueProvider : IValueProvider
    {
        PropertyInfo _MemberInfo;
        public NullToEmptyStringValueProvider(PropertyInfo memberInfo)
        {
            _MemberInfo = memberInfo;
        }
        public object GetValue(object target)
        {
            object result = _MemberInfo.GetValue(target);
            if (_MemberInfo.PropertyType == typeof(string) && result == null)
                result = "";
            return result;
        }
        public void SetValue(object target, object value)
        {
            _MemberInfo.SetValue(target, value);
        }
    }
}
