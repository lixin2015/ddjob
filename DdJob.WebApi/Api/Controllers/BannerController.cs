﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.Application.Services.Dto;
using DdJob.Application.Public;
using DdJob.Application.Public.Dto;
using DdJob.Core.Models.Common;

namespace DdJob.Api.Controllers
{
    [RoutePrefix("api/banner")]
    public class BannerController : ApiControllerBase<Banner, long, BannerDto>
    {
        private readonly IBannerService _bannerService;


        public BannerController(IBannerService bannerService) : base(bannerService)
        {
            _bannerService = bannerService;
        }

        #region 管理后台
        [HttpPost]
        public IHttpActionResult GetBannerByPage(BannerPagedRequestInput input)
        {
            return Json(_bannerService.GetBannerByPage(input));
        }


        [HttpPost]
        public async Task<ApiOutput<long>> CreateOrUpdate(BannerCreateOrUpdateInput input)
        {
            CheckModelState();
            var id = await _bannerService.CreateOrUpdate(input);
            return GetSuccessOutput(id);
        }


        [HttpGet]
        public async Task<ApiOutput<BannerDto>> GetBannerById(long id)
        {

            var data = _bannerService.GetEntityDtoById(id);
            var result = GetSuccessOutput(data);
            return result;
        }
        #endregion
    }
}
