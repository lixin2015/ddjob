﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Abp.Collections.Extensions;
using DdJob.Application.Sessions;
using DdJob.Application.Users;
using DdJob.Application.Users.Dto;
using DdJob.Core.Users;

namespace DdJob.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/user")]
    public class UserController : ApiControllerBase<User, long, UserDto>
    {
        private readonly IUserService _userService;
        private readonly ISessionAppService _sessionAppService;

        public UserController(IUserService userService, ISessionAppService sessionAppService)
            : base(userService)
        {
            _userService = userService;
            _sessionAppService = sessionAppService;
        }


    }
}
