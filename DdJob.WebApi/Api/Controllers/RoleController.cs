﻿using System.Threading.Tasks;
using System.Web.Http;
using Abp.Application.Services.Dto;
using Abp.UI;
using DdJob.Application.Roles;
using DdJob.Application.Roles.Dto;
using DdJob.Authorization.Roles;

namespace DdJob.Api.Controllers
{
    [Authorize]
    //[AbpAuthorize(PermissionNames.Pages_Role)] todo
    //[AbpAuthorize(PermissionNames.Pages_RoleList_Admin)] todo
    [RoutePrefix("api/role")]
    public class RoleController : ApiControllerBase<Role, int, RoleDto>
    {
        private readonly IRoleAppService _roleAppService;

        public RoleController(IRoleAppService roleAppService)
            : base(roleAppService)
        {
            _roleAppService = roleAppService;
        }

        [HttpPost]
        public IHttpActionResult GetRolesByPage(RolePagedRequestInput input)
        {
            return Json(_roleAppService.GetRolesByPage(input));
        }

        [HttpPost]
        public async Task<ApiOutput<int>> CreateOrUpdate(RoleCreateOrUpdateInput input)
        {
            CheckModelState();
            var re = await _roleAppService.CreateOrUpdate(input);
            return GetSuccessOutput(re);
        }

        [HttpPost]
        public async Task<ApiOutput> SingleDelete(EntityDto<int> input)
        {
            var model = _roleAppService.GetEntityDtoById(input.Id);
            if (model == null)
                return GetSuccessOutput();
            if (!model.IsStatic)
                await _roleAppService.Delete(input);
            else
                throw new UserFriendlyException("该角色不可删除");

            return GetSuccessOutput();
        }

        [HttpPost]
        public async Task<ApiOutput> UpdatePermissions(UpdateRolePermissionsInput input)
        {
            CheckModelState();
            await _roleAppService.UpdateRolePermissions(input);
            return GetSuccessOutput();
        }

        public ApiOutput<PermissionDto> GetAllPermissions()
        {
            var model = _roleAppService.GetAllPermissions();

            return GetSuccessOutput(model);
        }

        public async Task<ApiOutput<string>> GetGrantedPermissions(int id)
        {
            var str = await _roleAppService.GetGrantedPermissionsById(id);

            return GetSuccessOutput(str);
        }
    }
}
