﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using DdJob.Application.Public;

namespace DdJob.Api.Controllers
{
    [RoutePrefix("api/file")]
    public class FileController : ApiControllerBase
    {
        private readonly IMediaService _mediaService;

        private readonly string _tempPath;
        private readonly string _tempExeclPath;
        private readonly string _tempKdEditerPath;
        public FileController(IMediaService mediaService)
        {
            _mediaService = mediaService;
            _tempPath = string.Format("/Media/Images/{0}/{1}/", DateTime.Now.Year, DateTime.Now.ToString("MM-dd"));
            _tempExeclPath = string.Format("/Media/Files/Import/");
            _tempKdEditerPath = "/Media/KindEditor/";
        }

        [Authorize]
        public string UploadFile()
        {
            if (Request.Content.IsMimeMultipartContent())
            {
                // 文件保存目录路径
                var dirTempPath = AppDomain.CurrentDomain.BaseDirectory + _tempPath;
                //Save file
                MultipartFormDataStreamProvider provider = new MultipartFormDataStreamProvider(dirTempPath);

                string filename = "Not set";

                IEnumerable<HttpContent> parts = null;
                Task.Factory
                    .StartNew(() =>
                    {
                        parts = Request.Content.ReadAsMultipartAsync(provider).Result.Contents;
                        filename = "Set Success";
                    },
                    CancellationToken.None,
                    TaskCreationOptions.LongRunning, // guarantees separate thread
                    TaskScheduler.Default)
                    .Wait();

                return filename;
            }
            else
            {
                return "Invalid.";
            }
        }

        [Authorize]
        public async Task<Hashtable> Upload()
        {
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            // 文件保存目录路径
            var dirTempPath = AppDomain.CurrentDomain.BaseDirectory + _tempPath;
            if (!Directory.Exists(dirTempPath))
            {
                Directory.CreateDirectory(dirTempPath);
            }
            // 导入文件保存目录路径
            var dirExcelTempPath = AppDomain.CurrentDomain.BaseDirectory + _tempExeclPath;
            if (!Directory.Exists(dirExcelTempPath))
            {
                Directory.CreateDirectory(dirExcelTempPath);
            }

            // 设置上传目录 
            var provider = new MultipartFormDataStreamProvider(dirTempPath);

            var imgHashtables = new List<Hashtable>();
            var task = Request.Content.ReadAsMultipartAsync(provider).
                  ContinueWith<Hashtable>(o =>
                  {
                      var hash = new Hashtable();
                      var file = provider.FileData[0];

                      // 最大文件大小
                      int maxSize = 1000000;
                      // 定义允许上传的文件扩展名 
                      string fileTypes = "gif,jpg,jpeg,png,bmp";

                      string orfilename = file.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');

                      var isExcel = false;
                      //上传Excel
                      if (file.Headers.ContentType.MediaType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || file.Headers.ContentType.MediaType == ".csv" || file.Headers.ContentType.MediaType == "application/vnd.ms-excel")
                      {
                          isExcel = true;
                          maxSize = 10000000;
                          fileTypes = "xlsx,xls,csv";
                          
                          if (File.Exists(Path.Combine(dirExcelTempPath, orfilename)))
                          {
                              hash["Code"] = -1;
                              hash["Message"] = "该文件已经被上传。";
                              return hash;
                          }
                      }

                      var fileinfo = new FileInfo(file.LocalFileName);
                      if (fileinfo.Length <= 0)
                      {
                          hash["Code"] = -1;
                          hash["Message"] = "请选择上传文件。";
                      }
                      else if (fileinfo.Length > maxSize)
                      {
                          hash["Code"] = -1;
                          hash["Message"] = "上传文件大小超过限制。";
                      }
                      else
                      {
                          var fileExt = orfilename.Substring(orfilename.LastIndexOf('.'));
                          if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
                          {
                              hash["Code"] = -1;
                              hash["Message"] = "不支持上传文件类型。";
                          }
                          else
                          {
                              try
                              {
                                  var fileName = Guid.NewGuid() + fileExt;
                                  var path = _tempPath + fileName;
                                  if (isExcel)
                                  {
                                      fileName = orfilename;
                                      path = _tempExeclPath + fileName;
                                      fileinfo.CopyTo(Path.Combine(dirExcelTempPath, fileName), true); //拷贝Excel文件
                                  }
                                  else
                                  {
                                      fileinfo.CopyTo(Path.Combine(dirTempPath, fileName), true); //拷贝图片文件
                                  }

                                  var mediaId = isExcel ? 1 : _mediaService.SaveMediaFile(path);

                                  hash["Code"] = 0;
                                  hash["Message"] = "上传成功";
                                  hash["FileId"] = mediaId;
                                  hash["FileName"] = fileName;
                                  hash["NetImageUrl"] = path;
                                  hash["url"] = path;
                              }
                              catch (Exception exception)
                              {
                                  throw new Exception("上传失败!", exception);
                              }
                          }
                      }
                      return hash;
                  });

            return await task;
        }

        [Authorize]
        public async Task<Hashtable> KindEditorUpload()
        {
            if (!Request.Content.IsMimeMultipartContent("form-data"))
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            // 文件保存目录路径
            var dirTempPath = AppDomain.CurrentDomain.BaseDirectory + _tempKdEditerPath;
            if (!Directory.Exists(dirTempPath))
            {
                Directory.CreateDirectory(dirTempPath);
            }

            // 设置上传目录 
            var provider = new MultipartFormDataStreamProvider(dirTempPath);

            var imgHashtables = new List<Hashtable>();
            var task = Request.Content.ReadAsMultipartAsync(provider).
                  ContinueWith<Hashtable>(o =>
                  {
                      var hash = new Hashtable();
                      var file = provider.FileData[0];
                      // 最大文件大小
                      const int maxSize = 1000000;
                      // 定义允许上传的文件扩展名 
                      const string fileTypes = "gif,jpg,jpeg,png,bmp";

                      string orfilename = file.Headers.ContentDisposition.FileName.TrimStart('"').TrimEnd('"');
                      var fileinfo = new FileInfo(file.LocalFileName);
                      if (fileinfo.Length <= 0)
                      {
                          hash["Code"] = -1;
                          hash["Message"] = "请选择上传文件。";
                      }
                      else if (fileinfo.Length > maxSize)
                      {
                          hash["Code"] = -1;
                          hash["Message"] = "上传文件大小超过限制。";
                      }
                      else
                      {
                          var fileExt = orfilename.Substring(orfilename.LastIndexOf('.'));
                          if (string.IsNullOrEmpty(fileExt) || Array.IndexOf(fileTypes.Split(','), fileExt.Substring(1).ToLower()) == -1)
                          {
                              hash["Code"] = -1;
                              hash["Message"] = "不支持上传文件类型。";
                          }
                          else
                          {
                              try
                              {
                                  var fileName = Guid.NewGuid() + fileExt;
                                  fileinfo.CopyTo(Path.Combine(dirTempPath, fileName), true); //拷贝文件
                                  hash["Url"] = _tempKdEditerPath + fileName;
                                  hash["Error"] = 0;
                              }
                              catch (Exception exception)
                              {
                                  throw new Exception("上传失败!", exception);
                              }
                          }
                      }
                      return hash;
                  });

            return await task;
        }
    }
}
