﻿using System;
using Abp.Web.Models;
using Newtonsoft.Json;

namespace DdJob
{
    [Serializable]
    public class ApiOutput : ApiOutput<object>
    {
        public ApiOutput()
        {
        }

        public ApiOutput(bool success) : base(success)
        {
        }

        public ApiOutput(object result) : base(result)
        {
        }

        public ApiOutput(ErrorInfo errorInfo, bool unAuthorizedRequest = false) : base(errorInfo, unAuthorizedRequest)
        {
        }
    }

    [Serializable]
    public class ApiOutput<TResult>
    {
        public bool Success { get; set; }

        //[JsonProperty("Result")]
        public TResult Data { get; set; }

        public ErrorInfo Error { get; set; }

        public bool UnAuthorizedRequest { get; set; }

        public ApiOutput(TResult data)
        {
            Data = data;
            Success = true;
        }

        public ApiOutput()
        {
            Success = true;
        }

        public ApiOutput(bool success)
        {
            Success = success;
        }

        public ApiOutput(ErrorInfo error, bool unAuthorizedRequest = false)
        {
            Error = error;
            UnAuthorizedRequest = unAuthorizedRequest;
            Success = false;
        }
    }
}