﻿using System.Threading.Tasks;
using System.Web.Http;
using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using Abp.UI;
using Abp.Web.Models;
using Abp.WebApi.Controllers;
using DdJob.Application;
using DdJob.Application.Pageable;

namespace DdJob
{
    public class ApiControllerBase : AbpApiController
    {
        protected virtual void CheckModelState()
        {
            if (!ModelState.IsValid)
            {
                throw new UserFriendlyException(L("The form is not valid."));
            }
        }

        protected void CommonParamError()
        {
            throw new UserFriendlyException("Error!");
        }

        protected ApiOutput GetSuccessOutput()
        {
            return new ApiOutput(true);
        }

        protected ApiOutput<TOutputDto> GetSuccessOutput<TOutputDto>(TOutputDto result)
        {
            return new ApiOutput<TOutputDto>(result);
        }

        protected ApiOutput GetFailureOutput(string message, string errorDetail)
        {
            return new ApiOutput(new ErrorInfo { Message = message, Details = errorDetail });
        }

        protected string MakeUrl(string resource)
        {
            var host = CommonHelper.ServerVariables("SERVER_NAME");
            var port = CommonHelper.ServerVariables("SERVER_PORT");
            var temp = "http://{0}:{1}";
            return string.Format(temp, host, port) + resource;
        }
    }

    public class ApiControllerBase<TEntity, TPrimaryKey, TDto> : ApiControllerBase
        where TDto : IEntityDto<TPrimaryKey>, new()
        where TEntity : IEntity<TPrimaryKey>, new()
    {
        public ApiControllerBase(IServiceBase<TEntity, TPrimaryKey, TDto> service)
        {
            Service = service;
        }

        private IServiceBase<TEntity, TPrimaryKey, TDto> Service { get; set; }

        [Authorize]
        [HttpGet]
        public virtual ApiOutput<TDto> GetById(TPrimaryKey id)
        {
            return GetSuccessOutput(Service.GetEntityDtoById(id));
        }
        [Authorize]
        [HttpGet]
        public virtual ApiOutput<TEntity> GetEntityById(TPrimaryKey id)
        {
            return GetSuccessOutput(Service.GetById(id));
        }

        [Authorize]
        [HttpGet]
        public ApiOutput<ListResultDto<TDto>> GetAll()
        {
            return GetSuccessOutput(Service.GetAll());
        }

        [Authorize]
        [HttpPost]
        public virtual IHttpActionResult GetPagedResult(PagedRequestInput input)
        {
            var re = Service.GetAll(input);
            re.Page = input.Page;
            re.PageSize = input.PageSize;
            return Json(re);
        }

        [Authorize]
        [HttpPost]
        public async Task<ApiOutput> Delete(EntityDto<TPrimaryKey> input)
        {
            await Service.Delete(input);
            return GetSuccessOutput();
        }

        [Authorize]
        [HttpPost]
        public async Task<ApiOutput> BatchDelete(BatchDeleteEntitiesInput<TPrimaryKey> input)
        {
            await Service.BatchDelete(input);
            return GetSuccessOutput();
        }
    }
}