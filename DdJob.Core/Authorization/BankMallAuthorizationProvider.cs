﻿using Abp.Authorization;
using Abp.Localization;
using DdJob.Core;

namespace DdJob.Authorization
{
    public class DdJobAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //Common permissions
            var pages = context.GetPermissionOrNull(PermissionNames.Pages) ?? context.CreatePermission(PermissionNames.Pages);

            //Home
            pages.CreateChildPermission(PermissionNames.Pages_Home_Admin);
            pages.CreateChildPermission(PermissionNames.Pages_Home_Bank);
            pages.CreateChildPermission(PermissionNames.Pages_Home_Business);
            pages.CreateChildPermission(PermissionNames.Pages_Home_Branch);

            //User 用户模块管理权限
            var user = pages.CreateChildPermission(PermissionNames.Pages_User);
            user.CreateChildPermission(PermissionNames.Pages_UserList_Admin);

            //Role 角色模块管理权限
            var role = pages.CreateChildPermission(PermissionNames.Pages_Role);
            role.CreateChildPermission(PermissionNames.Pages_RoleList_Admin);

            //Card 电子卷模块管理权限
            var card = pages.CreateChildPermission(PermissionNames.Pages_Card);
            card.CreateChildPermission(PermissionNames.Pages_CardList_Admin);
            card.CreateChildPermission(PermissionNames.Pages_CardPrefabricate_Admin);

            //Bank 银行模块管理权限
            var bank = pages.CreateChildPermission(PermissionNames.Pages_Bank);
            bank.CreateChildPermission(PermissionNames.Pages_BankList_Admin);

            //Department 部门模块管理权限
            var department = pages.CreateChildPermission(PermissionNames.Pages_Department);
            department.CreateChildPermission(PermissionNames.Pages_DepartmentList_Admin);

            //Business 商户模块管理权限
            var business = pages.CreateChildPermission(PermissionNames.Pages_Business);
            business.CreateChildPermission(PermissionNames.Pages_BusinessList_Admin);

            //Branch 门店模块管理权限
            var branch = pages.CreateChildPermission(PermissionNames.Pages_Branch);
            branch.CreateChildPermission(PermissionNames.Pages_BranchList_Admin);
            branch.CreateChildPermission(PermissionNames.Pages_BranchList_Business);

            //Order 订单管理
            var order = pages.CreateChildPermission(PermissionNames.Pages_Order);
            order.CreateChildPermission(PermissionNames.Pages_OrderList_Admin);
            order.CreateChildPermission(PermissionNames.Pages_OrderList_Business);

            //Category 产品类别管理
            var cateogry = pages.CreateChildPermission(PermissionNames.Pages_Category);
            cateogry.CreateChildPermission(PermissionNames.Pages_CategoryList_Admin);

            //Product 产品管理
            var product = pages.CreateChildPermission(PermissionNames.Pages_Product);
            product.CreateChildPermission(PermissionNames.Pages_ProductList_Admin);

            //Product 产品属性管理
            var productAttribute = pages.CreateChildPermission(PermissionNames.Pages_ProductAttribute);
            productAttribute.CreateChildPermission(PermissionNames.Pages_ProductAttributeList_Admin);

            //Pages_ProductHomePartition 产品首页分区管理
            var productHomePartition = pages.CreateChildPermission(PermissionNames.Pages_ProductHomePartition);
            productHomePartition.CreateChildPermission(PermissionNames.Pages_ProductHomePartitionList_Admin);

            //Banner 管理
            var banner = pages.CreateChildPermission(PermissionNames.Pages_Banner);
            banner.CreateChildPermission(PermissionNames.Pages_Banner_Admin);

            //Integral 管理
            var integral = pages.CreateChildPermission(PermissionNames.Pages_Integral);
            integral.CreateChildPermission(PermissionNames.Pages_IntegralList_Admin);
            integral.CreateChildPermission(PermissionNames.Pages_IntegralImport_Admin);

            //Message 管理
            var message = pages.CreateChildPermission(PermissionNames.Pages_Message);
            message.CreateChildPermission(PermissionNames.Pages_MessageList_Admin);

            //VirtualTicket 管理
            var virtualTicket = pages.CreateChildPermission(PermissionNames.Pages_VirtualTicket);
            virtualTicket.CreateChildPermission(PermissionNames.Pages_VirtualTicketlList_Admin);
            virtualTicket.CreateChildPermission(PermissionNames.Pages_VirtualTicketImport_Admin);
            virtualTicket.CreateChildPermission(PermissionNames.Pages_VirtualTicketCreate_Admin);
            virtualTicket.CreateChildPermission(PermissionNames.Pages_VirtualTicketChange_Admin);




            //CustomSetting 管理
            var customSetting = pages.CreateChildPermission(PermissionNames.Pages_CustomSetting);
            customSetting.CreateChildPermission(PermissionNames.Pages_CustomSetting_Admin);

            //Report 管理
            var report = pages.CreateChildPermission(PermissionNames.Pages_Report);
            report.CreateChildPermission(PermissionNames.Pages_ReportDetail_Admin);
            report.CreateChildPermission(PermissionNames.Pages_ReportGather_Admin);
            report.CreateChildPermission(PermissionNames.Pages_ReportProduct_Admin);
            report.CreateChildPermission(PermissionNames.Pages_ReportDetail_Business);
            report.CreateChildPermission(PermissionNames.Pages_ReportGather_Business);
            report.CreateChildPermission(PermissionNames.Pages_ReportProduct_Business);


            //Integral 管理
            var wihteList = pages.CreateChildPermission(PermissionNames.Pages_WhiteList);
            wihteList.CreateChildPermission(PermissionNames.Pages_WhiteListList_Admin);
            wihteList.CreateChildPermission(PermissionNames.Pages_WhiteListImport_Admin);

            //BusinessType
            var businessType = pages.CreateChildPermission(PermissionNames.Pages_BusinessType);
            businessType.CreateChildPermission(PermissionNames.Pages_BusinessTypeList_Admin);

            //CollectionData
            var collectionData = pages.CreateChildPermission(PermissionNames.Pages_CollectionData);
            collectionData.CreateChildPermission(PermissionNames.Pages_CollectionDataList_Admin);


            //AdvisoryRequest
            var advisoryRequest = pages.CreateChildPermission(PermissionNames.Pages_AdvisoryRequest);
            advisoryRequest.CreateChildPermission(PermissionNames.Pages_AdvisoryRequestList_Admin);

        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DdJobConsts.LocalizationSourceName);
        }
    }
}
