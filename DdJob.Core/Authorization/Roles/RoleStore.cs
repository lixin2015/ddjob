using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using DdJob.Core.Users;

namespace DdJob.Authorization.Roles
{
    public class RoleStore : AbpRoleStore<Role, User>
    {
        private readonly IRepository<Role> _roleRepository;
        public RoleStore(
            IRepository<Role> roleRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<RolePermissionSetting, long> rolePermissionSettingRepository)
            : base(
                roleRepository,
                userRoleRepository,
                rolePermissionSettingRepository)
        {
            _roleRepository = roleRepository;
        }

        public int InsertAndGetId(Role model)
        {
            return _roleRepository.InsertAndGetId(model);
        }
    }
}