using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Domain.Uow;
using Abp.Runtime.Caching;
using Abp.Zero.Configuration;
using DdJob.Core.Users;
using Microsoft.AspNet.Identity;

namespace DdJob.Authorization.Roles
{
    public class RoleManager : AbpRoleManager<Role, User>
    {
        private readonly RoleStore _store;
        public RoleManager(
            RoleStore store,
            IPermissionManager permissionManager,
            IRoleManagementConfig roleManagementConfig,
            ICacheManager cacheManager,
            IUnitOfWorkManager unitOfWorkManager)
            : base(
                store,
                permissionManager,
                roleManagementConfig,
                cacheManager,
                unitOfWorkManager)
        {
            _store = store;
        }
        public int InsertAndGetId(Role role)
        {
            return _store.InsertAndGetId(role);
        }
    }
}