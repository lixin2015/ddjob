namespace DdJob.Authorization.Roles
{
    public static class StaticRoleNames
    {
        /// <summary>
        /// 超级管理员，启动多租户时才有这个概念
        /// </summary>
        public static class Host
        {
            public const string Admin = "SuperAdmin";
            public const string DisplayName = "超级管理员";
        }

        /// <summary>
        /// 启用时为租户管理员，未启用时为一般管理员
        /// 本系统未启用多租户，所以为平台管理员角色
        /// </summary>
        public static class Tenants
        {
            public const string Admin = "Admin";
            public const string DisplayName = "平台管理员";
        }

        public static class Staff
        {
            public const string Admin = "Staff";
            public const string DisplayName = "平台用户";
        }

        public static class Bank
        {
            public const string Admin = "Bank";
            public const string DisplayName = "第三方用户";
        }

        public static class Business
        {
            public const string Admin = "Business";
            public const string DisplayName = "商户用户";
        }

        public static class Customer
        {
            public const string Admin = "Customer";
            public const string DisplayName = "个人用户";
        }
    }
}