﻿namespace DdJob.Authorization
{
    public static class PermissionNames
    {
        public const string Pages = "Pages";

        public const string Pages_Tenants = "Pages.Tenants";

        //Home
        public const string Pages_Home_Admin = "Pages.Home.Admin";
        public const string Pages_Home_Bank = "Pages.Home.Bank";
        public const string Pages_Home_Business = "Pages.Home.Business";
        public const string Pages_Home_Branch = "Pages.Home.Branch";

        //User
        public const string Pages_User = "Pages.User";
        public const string Pages_UserList_Admin = "Pages.UserList.Admin";

        //user
        public const string Pages_Role = "Pages.Role";
        public const string Pages_RoleList_Admin = "Pages.RoleList.Admin";

        //Card
        public const string Pages_Card = "Pages.Card";
        public const string Pages_CardList_Admin = "Pages.CardList.Admin";
        public const string Pages_CardPrefabricate_Admin = "Pages.CardPrefabricate.Admin";

        //Bank
        public const string Pages_Bank = "Pages.Bank";
        public const string Pages_BankList_Admin = "Pages.BankList.Admin";
        //Deparment
        public const string Pages_Department = "Pages.Department";
        public const string Pages_DepartmentList_Admin = "Pages.DepartmentList.Admin";


        //Business
        public const string Pages_Business = "Pages.Business";
        public const string Pages_BusinessList_Admin = "Pages.BusinessList.Admin";

        //Branch
        public const string Pages_Branch = "Pages.Branch";
        public const string Pages_BranchList_Admin = "Pages.BranchList.Admin";
        public const string Pages_BranchList_Business = "Pages.BranchList.Business";

        //Order
        public const string Pages_Order = "Pages.Order";
        public const string Pages_OrderList_Admin = "Pages.OrderList.Admin";
        public const string Pages_OrderList_Business = "Pages.OrderList.Business";

        //Category
        public const string Pages_Category = "Pages.Category";
        public const string Pages_CategoryList_Admin = "Pages.CategoryList.Admin";

        //Product
        public const string Pages_Product = "Pages.Product";
        public const string Pages_ProductList_Admin = "Pages.ProductList.Admin";

        //ProductAttribute
        public const string Pages_ProductAttribute = "Pages.ProductAttribute";
        public const string Pages_ProductAttributeList_Admin = "Pages.ProductAttributeList.Admin";

        //ProductHomePartition
        public const string Pages_ProductHomePartition = "Pages.ProductHomePartition";
        public const string Pages_ProductHomePartitionList_Admin = "Pages.ProductHomePartitionList.Admin";

        //Banner
        public const string Pages_Banner = "Pages.Banner";
        public const string Pages_Banner_Admin = "Pages.BannerList.Admin";


        //Integral
        public const string Pages_Integral = "Pages.Integral";
        public const string Pages_IntegralList_Admin = "Pages.IntegralList.Admin";
        public const string Pages_IntegralImport_Admin = "Pages.IntegralImport.Admin";
        //Message
        public const string Pages_Message = "Pages.Message";
        public const string Pages_MessageList_Admin = "Pages.MessageList.Admin";

        //VirtualTicket
        public const string Pages_VirtualTicket = "Pages.VirtualTicket";
        public const string Pages_VirtualTicketlList_Admin = "Pages.VirtualTicketList.Admin";
        public const string Pages_VirtualTicketImport_Admin = "Pages.VirtualTicketImport.Admin";
        public const string Pages_VirtualTicketCreate_Admin = "Pages.VirtualTicketCreate.Admin";
        public const string Pages_VirtualTicketChange_Admin = "Pages.VirtualTicketChange.Admin";

        //CustomSetting
        public const string Pages_CustomSetting = "Pages.CustomSetting";
        public const string Pages_CustomSetting_Admin = "Pages.CustomSettingList.Admin";

        //Report
        public const string Pages_Report = "Pages.Report";
        public const string Pages_ReportGather_Admin = "Pages.ReportGather.Admin";
        public const string Pages_ReportDetail_Admin = "Pages.ReportDetail.Admin";
        public const string Pages_ReportProduct_Admin = "Pages.ReportProduct.Admin";
        public const string Pages_ReportGather_Business = "Pages.ReportGather.Business";
        public const string Pages_ReportDetail_Business = "Pages.ReportDetail.Business";
        public const string Pages_ReportProduct_Business = "Pages.ReportProduct.Business";


        //Integral
        public const string Pages_WhiteList = "Pages.WhiteList";
        public const string Pages_WhiteListList_Admin = "Pages.WhiteListList.Admin";
        public const string Pages_WhiteListImport_Admin = "Pages.WhiteListImport.Admin";

        //BusinessType
        public const string Pages_BusinessType = "Pages.BusinessType";
        public const string Pages_BusinessTypeList_Admin = "Pages.BusinessTypeList.Admin";
        //CollectionData
        public const string Pages_CollectionData = "Pages.CollectionData";
        public const string Pages_CollectionDataList_Admin = "Pages.CollectionDataList.Admin";


        //AdvisoryRequest
        public const string Pages_AdvisoryRequest = "Pages.AdvisoryRequest";
        public const string Pages_AdvisoryRequestList_Admin = "Pages.AdvisoryRequestList.Admin";

    }
}