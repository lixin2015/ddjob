﻿using Abp.Authorization;
using DdJob.Authorization.Roles;
using DdJob.Core.MultiTenancy;
using DdJob.Core.Users;

namespace DdJob.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
