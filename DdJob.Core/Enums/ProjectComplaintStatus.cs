﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 工程投诉状态
    /// </summary>
    public enum ProjectComplaintStatus
    {
        [Description("待确认")]
        Pending = 0,

        [Description("确认中")]
        Processing = 10,

        [Description("投诉属实")]
        Fact = 20,

        [Description("投诉不属实")]
        NotFact = 30
    }
}
