﻿using System.ComponentModel;

namespace DdJob.Enums
{
    /// <summary>
    /// 错误码
    /// </summary>
    public enum ResultCode
    {
        [Description("未知")]
        None = 0,

        [Description("成功")]
        Success = 200,

        [Description("交易失败")]
        Failed = 400,

        //1000-2000错误为公用错误
        [Description("签名错误")]
        SignError = 1001,

        [Description("验证失败")]
        ExchangeFaild = 1002,

        [Description("内部错误")]
        InternalError = 1003,

        [Description("身份验证失败")]
        AuthenticationFailed = 1004,

        [Description("参数错误")]
        ParameterError = 1005,

        [Description("无权限")]
        PoorPermission = 1006,
        
    }
}
