﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// Banner类型
    /// </summary>
    public enum BannerType
    {
        [Description("首页")]
        Home = 10
    }
}
