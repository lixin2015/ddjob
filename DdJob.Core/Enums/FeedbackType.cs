﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 反馈类型
    /// </summary>
    public enum FeedbackType
    {
        [Description("--请选择--")]
        None = 0,

        [Description("改进建议")]
        Worker = 10,

        [Description("错误反馈")]
        Employer = 20
    }
}
