﻿using System.ComponentModel;

namespace DdJob.Enums
{
    /// <summary>
    /// 消息类型
    /// </summary>
    public enum MessageType
    {
        [Description("--请选择--")]
        None = 0,

        [Description("物流信息")]
        Logistics = 1,

        [Description("获得权限消息")]
        RewardAchieve = 10,

        [Description("积分提醒信息")]
        IntegralNotice = 20,

        [Description("积分到期提醒信息")]
        IntegralExpiredNotice = 30,

        [Description("系统通知")]
        Other = 40
    }
}
