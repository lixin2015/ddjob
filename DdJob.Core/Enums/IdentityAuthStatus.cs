﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 身份认证状态
    /// </summary>
    public enum IdentityAuthStatus
    {
        [Description("未认证")]
        None = 0,

        [Description("待审核")]
        Pending = 10,

        [Description("已认证")]
        Success = 20,

        [Description("认证失败")]
        Refused = 30
    }
}
