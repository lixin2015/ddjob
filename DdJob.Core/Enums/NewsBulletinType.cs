﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 新闻公告类型
    /// </summary>
    public enum NoticeType
    {
        [Description("--请选择--")]
        None = 0,

        [Description("新闻")]
        News = 10,

        [Description("公告")]
        Notice = 20
    }
}
