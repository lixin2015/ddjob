﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 工程状态
    /// </summary>
    public enum ProjectStatus
    {
        [Description("--请选择--")]
        None = 0,

        [Description("草稿")]
        Draft = 10,

        [Description("待审核")]
        Pending = 20,

        [Description("审核通过")]
        Success = 30,

        [Description("审核未通过")]
        Refused = 40,

        [Description("已关闭")]
        Closed = 50,

        [Description("已禁用")]
        Disabled = 60
    }
}
