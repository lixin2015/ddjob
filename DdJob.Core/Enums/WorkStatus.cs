﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 工作状态
    /// </summary>
    public enum WorkStatus
    {
        [Description("--请选择--")]
        None = 0,

        [Description("闲置中")]
        Free = 10,

        [Description("工作中")]
        Busy = 20
    }
}
