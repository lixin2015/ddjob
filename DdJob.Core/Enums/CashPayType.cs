﻿using System.ComponentModel;

namespace DdJob.Enums
{
    public enum MediaType
    {
        [Description("--请选择--")]
        None = 0,

        /// <summary>
        /// 图片
        /// </summary>
        [Description("图片")]
        Image = 1,

        /// <summary>
        /// 文件
        /// </summary>
        [Description("文件")]
        File = 10,

        /// <summary>
        /// 视频
        /// </summary>
        [Description("视频")]
        Video = 20
    }
    
    public enum SettingGroupType
    {
        [Description("--请选择--")]
        None = 0,

        [Description("普通设置")]
        General = 100,

        [Description("前端设置")]
        Client = 200,

        [Description("后台设置")]
        Admin = 300
    }

    public enum SMSCodeType
    {
        [Description("--请选择--")]
        None = 0,

        /// <summary>
        /// 快速登录
        /// </summary>
        [Description("快速登录")]
        Login = 1,

        /// <summary>
        /// 注册
        /// </summary>
        [Description("注册")]
        Register = 10,

        /// <summary>
        /// 忘记密码
        /// </summary>
        [Description("忘记密码")]
        ForgotPwd = 20,

        /// <summary>
        /// 手机绑定
        /// </summary>
        [Description("手机绑定")]
        BindPhone = 30
    }
    
}
