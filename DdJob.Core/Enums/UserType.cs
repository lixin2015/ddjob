﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 用户类型
    /// </summary>
    public enum UserType
    {
        [Description("--请选择--")]
        None = 0,

        [Description("工匠")]
        Worker = 20,

        [Description("企业")]
        Employer = 30,

        [Description("管理员")]
        Admin = 100
    }
}
