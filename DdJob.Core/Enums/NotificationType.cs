﻿using System.ComponentModel;

namespace DdJob.Core.Enums
{
    /// <summary>
    /// 通知消息类型
    /// </summary>
    public enum NotificationType
    {
        [Description("--请选择--")]
        None = 0,

        [Description("身份审核")]
        IdentityAudit = 10,

        [Description("智能推荐")]
        IntelligentRecommend = 20,

        [Description("工程审核")]
        ProjectAudit = 30,
    }
}
