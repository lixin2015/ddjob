﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;

namespace DdJob.Core.Models.Notices
{
    [Table("Notice")]
    public class Notice : FullAuditedEntity<long>
    {
        public string Title { get; set; }
        public NoticeType Type { get; set; }
        public string Brief { get; set; }
        public string Content { get; set; }
        public int DisplyOrder { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        /// <summary>
        /// 点赞量
        /// </summary>
        public virtual int PraiseNum { get; set; }
    }
}
