﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Enums;

namespace DdJob.Core.Models.Settings
{
    [Table("CustomSetting")]
    public class CustomSetting : FullAuditedEntity<long>
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public bool Editable { get; set; }
        public SettingGroupType GroupType { get; set; }

        public CustomSetting()
        {
            Editable = true;
        }
    }
}
