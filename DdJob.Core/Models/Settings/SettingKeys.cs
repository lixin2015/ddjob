﻿using DdJob.Enums;

namespace DdJob.Core.Models.Settings
{
    public class SettingKey
    {
        public string Name { get; set; }
        public string Key { get; set; }
        public SettingGroupType GroupType { get; set; }
    }
    /// <summary>
    /// Setting Keys
    /// </summary>
    public class SettingKeys
    {
        public static SettingKey IsTest
        {
            get { return new SettingKey() {GroupType = SettingGroupType.General, Name = "测试环境", Key = "IsTest"}; }
        }
    }
}
