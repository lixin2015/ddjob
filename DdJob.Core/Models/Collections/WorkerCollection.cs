﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Collections
{
    [Table("WorkerCollection")]
    public class WorkerCollection : FullAuditedEntity<long>
    {
        public virtual long WorkerId { get; set; }
        [ForeignKey("WorkerId")]
        public virtual Worker Worker { get; set; }

        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}