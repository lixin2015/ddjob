﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Models.Projects;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Collections
{
    [Table("ProjectCollection")]
    public class ProjectCollection : FullAuditedEntity<long>
    {
        public virtual long ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}