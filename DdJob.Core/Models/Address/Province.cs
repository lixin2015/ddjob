﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DdJob.Core.Models.Address
{
    /// <summary>
    /// 省（直辖市）
    /// </summary>
    [Table("Province")]
    public class Province : CreationAuditedEntity<long>
    {
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }
    }
}
