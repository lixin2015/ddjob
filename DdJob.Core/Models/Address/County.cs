﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DdJob.Core.Models.Address
{
    /// <summary>
    /// 区、县
    /// </summary>
    [Table("County")]
    public class County : CreationAuditedEntity<long>
    {
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }

        public virtual long CityId { get; set; }

        [ForeignKey("CityId")]
        public virtual City City { get; set; }
    }
}
