﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DdJob.Core.Models.Address
{
    /// <summary>
    /// 城市（直辖市）
    /// </summary>
    [Table("City")]
    public class City : CreationAuditedEntity<long>
    {
        public virtual string Name { get; set; }

        public virtual string Code { get; set; }

        public virtual long ProvinceId { get; set; }

        [ForeignKey("ProvinceId")]
        public virtual Province Province { get; set; }

    }
}
