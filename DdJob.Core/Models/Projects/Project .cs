﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Models.Address;
using DdJob.Core.Models.Works;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Projects
{
    [Table("Project")]
    public class Project : FullAuditedEntity<long>
    {
        public virtual string Title { get; set; }
        public virtual string Brief { get; set; }
        public virtual string Content { get; set; }
        public virtual string Tags { get; set; }
        public virtual string ContactName { get; set; }
        public virtual string ContactNumber { get; set; }


        public virtual long? ProvinceId { get; set; }
        public virtual Province Province { get; set; }

        public virtual long? CityId { get; set; }
        public virtual City City { get; set; }

        public virtual long? CountyId { get; set; }
        public virtual County County { get; set; }
        public virtual string Address { get; set; }
        /// <summary>
        /// 经度
        /// </summary>
        [DataType("decimal(10,7)")]
        public virtual decimal Longitude { get; set; }
        /// <summary>
        /// 纬度
        /// </summary>
        [DataType("decimal(10,7)")]
        public virtual decimal Latitude { get; set; }

        public virtual IList<WorkType> WorkTypes { get; set; }

        public virtual long? UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ProjectStatus Status { get; set; }
        public virtual bool HasAuthenticated { get; set; }
        public virtual DateTime? StartDate { get; set; }
        public virtual DateTime? EndDate { get; set; }
        /// <summary>
        /// 关注量
        /// </summary>
        public virtual int FollowNum { get; set; }
        /// <summary>
        /// 点赞量
        /// </summary>
        public virtual int PraiseNum { get; set; }
    }
}