﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Models.Works;

namespace DdJob.Core.Models.Projects
{
    [Table("ProjectWorkType")]
    public class ProjectWorkType : CreationAuditedEntity<long>
    {
        public virtual long ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public virtual long WorkTypeId { get; set; }
        [ForeignKey("WorkTypeId")]
        public virtual WorkType WorkType { get; set; }
        /// <summary>
        /// 所需该工种人数
        /// </summary>
        public virtual int WorkerCount { get; set; }
    }
}