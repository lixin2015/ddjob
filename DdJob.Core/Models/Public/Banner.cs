﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;

namespace DdJob.Core.Models.Common
{
    [Table("Banner")]
    public class Banner : FullAuditedEntity<long>
    {
        public virtual string Title { get; set; }
        public virtual BannerType Type { get; set; }
        public virtual long MediaId { get; set; }
        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }
        public virtual string LinkUrl { get; set; }
        public virtual bool IsActive { get; set; }
    }
}
