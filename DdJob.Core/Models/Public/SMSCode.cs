﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Enums;

namespace DdJob.Core.Models.Common
{
    [Table("SmsCode")]
    public class SmsCode : CreationAuditedEntity<long>
    {
        public string Code { get; set; }
        public SMSCodeType Type { get; set; }
        public string Phone { get; set; }
        public bool IsUsed { get; set; }
        public virtual DateTime OverdueDate { get; set; }
    }
}
