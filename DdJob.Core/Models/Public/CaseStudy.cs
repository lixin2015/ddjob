﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Models.Common;

namespace DdJob.Core.Models.Public
{
    [Table("CaseStudy")]
    public class CaseStudy : FullAuditedEntity<long>
    {
        public string Title { get; set; }
        public string Brief { get; set; }
        public string Content { get; set; }
        public int DisplyOrder { get; set; }
        public virtual bool IsActive { get; set; }
        public virtual IList<Media> Medias { get; set; }
        /// <summary>
        /// 点赞量
        /// </summary>
        public virtual int PraiseNum { get; set; }
    }
}
