﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Models.Common;

namespace DdJob.Core.Models.Public
{
    [Table("CaseStudyMedia")]
    public class CaseStudyMedia : CreationAuditedEntity<long>
    {
        public virtual long CaseStudyId { get; set; }
        [ForeignKey("CaseStudyId")]
        public virtual CaseStudy CaseStudy { get; set; }

        public virtual long MediaId { get; set; }
        [ForeignKey("MediaId")]
        public virtual Media Media { get; set; }
    }
}
