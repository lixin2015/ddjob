﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Public
{
    [Table("Feedback")]
    public class Feedback : FullAuditedEntity<long>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public FeedbackType Type { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public bool IsProcessed { get; set; }
        public string ProcessedResult { get; set; }
        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
