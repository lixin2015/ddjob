﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Models.Projects;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Public
{
    [Table("ProjectComplaint")]
    public class ProjectComplaint : FullAuditedEntity<long>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string ContactName { get; set; }
        public string ContactNumber { get; set; }
        public ProjectComplaintStatus Status { get; set; }
        public string ProcessedResult { get; set; }
        public virtual long ProjectId { get; set; }
        [ForeignKey("ProjectId")]
        public virtual Project Project { get; set; }

        public virtual long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
