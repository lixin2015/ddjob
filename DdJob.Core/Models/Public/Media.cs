﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Enums;

namespace DdJob.Core.Models.Common
{
    [Table("Media")]
    public class Media : CreationAuditedEntity<long>
    {
        public virtual MediaType MediaType { get; set; }

        public virtual string Path { get; set; }

        public virtual bool IsActive { get; set; }
    }
}
