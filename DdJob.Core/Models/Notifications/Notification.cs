﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Notifications
{
    [Table("Notification")]
    public class Notification : FullAuditedEntity<long>
    {
        public NotificationType ActivityType { get; set; }
        public long? AssociatedId { get; set; }
        public string Content { get; set; }
        public string ContentLink { get; set; }
        public bool IsRead { get; set; }

        public long? SenderId { get; set; }
        [ForeignKey("SenderId")]
        public User Sender { get; set; }

        public long RecipientId { get; set; }
        [ForeignKey("RecipientId")]
        public User Recipient { get; set; }
    }
}
