﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Models.Common;
using DdJob.Core.Users;
using DdJob.Enums;

namespace DdJob.Core.Models.Authentications
{
    [Table("ClientAuthentication")]
    public class ClientAuthentication : FullAuditedEntity<long>
    {
        public virtual long ClientId { get; set; }
        [ForeignKey("ClientId")]
        public virtual User Client { get; set; }

        public virtual IdentityAuthStatus Status { get; set; }

        public virtual string RefusedReason { get; set; }
        public virtual string IDCard { get; set; }

        public virtual long IDFaceMediaId { get; set; }
        public virtual string IDFaceMediaPath { get; set; }

        public virtual long IDBackMediaId { get; set; }
        public virtual string IDBackMediaPath { get; set; }
    }
}