﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Users;
using DdJob.Enums;

namespace DdJob.Core.Models.Authentications
{
    [Table("EmployerAuthentication")]
    public class EmployerAuthentication : FullAuditedEntity<long>
    {
        public virtual long EmployerId { get; set; }
        [ForeignKey("EmployerId")]
        public virtual Employer Employer { get; set; }

        public virtual IdentityAuthStatus Status { get; set; }

        public virtual string RefusedReason { get; set; }
    }
}