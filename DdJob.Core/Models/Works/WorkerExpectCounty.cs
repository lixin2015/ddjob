﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Models.Address;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Works
{
    [Table("WorkerExpectCounty")]
    public class WorkerExpectCounty : CreationAuditedEntity<long>
    {
        public virtual long WorkerId { get; set; }
        [ForeignKey("WorkerId")]
        public virtual Worker Worker { get; set; }

        public virtual long CountyId { get; set; }
        [ForeignKey("CountyId")]
        public virtual County County { get; set; }
    }
}