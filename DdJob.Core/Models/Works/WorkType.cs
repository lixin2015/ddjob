﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DdJob.Core.Models.Works
{
    [Table("WorkType")]
    public class WorkType : FullAuditedEntity<long>
    {
        public virtual long Name { get; set; }

        public virtual string Description { get; set; }
    }
}