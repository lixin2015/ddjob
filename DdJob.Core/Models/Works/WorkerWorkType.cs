﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Users;

namespace DdJob.Core.Models.Works
{
    [Table("WorkerWorkType")]
    public class WorkerWorkType : CreationAuditedEntity<long>
    {
        public virtual long WorkerId { get; set; }
        [ForeignKey("WorkerId")]
        public virtual Worker Worker { get; set; }

        public virtual long WorkTypeId { get; set; }
        [ForeignKey("WorkTypeId")]
        public virtual WorkType WorkType { get; set; }
    }
}