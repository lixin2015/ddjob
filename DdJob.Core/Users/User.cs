﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Authorization.Users;
using Abp.Extensions;
using DdJob.Core.Enums;
using DdJob.Enums;

namespace DdJob.Core.Users
{
    /// <summary>
    /// 系统用户信息
    /// </summary>
    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123qwe";

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public virtual string NickName { get; set; }
        public virtual int LoginFailureCount { get; set; }
        public virtual DateTime? LastLoginFailureTime { get; set; }

        /// <summary>
        /// 用户类型
        /// </summary>
        public UserType UserType { get; set; }
        public long? WorkerId { get; set; }
        [ForeignKey("WorkerId")]
        public virtual Worker Worker { get; set; }

        public long? EmployerId { get; set; }
        [ForeignKey("EmployerId")]
        public virtual Employer Employer { get; set; }
    }
}