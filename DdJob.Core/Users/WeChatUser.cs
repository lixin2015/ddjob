﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DdJob.Core.Users
{
    /// <summary>
    /// 微信用户
    /// </summary>
    [Table("WeChatUser")]
    public class WeChatUser : FullAuditedEntity<long>
    {
        public virtual string OpenId { get; set; }

        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        public virtual string NickName { get; set; }

        public virtual string Avatar { get; set; }

        public virtual bool IsActive { get; set; }
    }
}