﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;
using DdJob.Core.Enums;
using DdJob.Core.Models;
using DdJob.Core.Models.Address;
using DdJob.Core.Models.Works;
using DdJob.Enums;

namespace DdJob.Core.Users
{

    [Table("Worker")]
    public class Worker : FullAuditedEntity<long>
    {

        public virtual string Name { get; set; }
        public virtual DateTime? Birthday { get; set; }
        public virtual bool? Sex { get; set; }
        public virtual string Sign { get; set; }
        public virtual int? WorkingYears { get; set; }
        public virtual string ContactNumber { get; set; }
        public virtual string Description { get; set; }
        public virtual WorkStatus Status { get; set; }
        public virtual IList<WorkType> WorkTypes { get; set; }

        public virtual long? ProvinceId { get; set; }
        [ForeignKey("ProvinceId")]
        public virtual Province Province { get; set; }
        public virtual long? CityId { get; set; }
        [ForeignKey("CityId")]
        public virtual City City { get; set; }
        public virtual IList<County> ExpectCounties { get; set; }

        public virtual bool HasClientAuthenticated { get; set; }
        /// <summary>
        /// 积分
        /// </summary>
        public virtual int Integral { get; set; }
    }
}
