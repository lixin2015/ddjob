﻿using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities.Auditing;

namespace DdJob.Core.Users
{

    [Table("Employer")]
    public class Employer : FullAuditedEntity<long>
    {
        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual bool HasClientAuthenticated { get; set; }
        
        public virtual bool HasEmployerAuthenticated { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual int CreditScore { get; set; }
    }
}
