﻿namespace DdJob
{
    public static class StringLengths
    {
        public const int Name = 100;
        public const int Title = 150;
        public const int Email = 250;
        public const int Description = 500;
        public const int Address = 250;
        public const int File = 250;
        public const int PostalCode = 20;
        public const int Phone = 50;
        public const int CreditCard = 20;
        public const int Cvv = 3;
        public const int Code = 5;
        public const int CardCode = 100;
        public const int Secret = 64;
        public const int Common = 200;
        public const int Long = 256;
        public const int Middle = 128;
        public const int Short = 64;
        public const int XShort = 32;
    }

    public static class DefaultLengths
    {
        public const int CardMaxGenrateLength = 100000;
    }

    public static class DefaultString
    {
        public const string SystemName = "光大微商城";
        public const string SystemEmail = "DdJob@test.com";
        public const string DefaultUserName = "DdJob";
        public const string DefaultSurname = "BM";
        public const string DefaultLongTime = "yyyy/MM/dd HH:mm:ss";
        public const string DefaultShortDate = "yyyy/MM/dd";
        public const string DefaultShortTime = "HH:mm:ss";
    }

    public static class CustomSettingKey
    {
        public const string DefultReceiverAddress = "DefultReceiverAddress";
        public const string WeChatPaymentUrl = "WeChatPaymentUrl";
        public const string SmsAccount = "SmsAccount";
        public const string SmsPassword = "SmsPassword";
        public const string SmsSign = "SmsSign";
        public const string Weburl = "http://web.cr6868.com/asmx/smsservice.aspx";
        //public const string Weburl = "http://114.80.154.72:801/asmx/smsservice.aspx";
    }

    public static class WeChatKey
    {
        //payment user id
        public const string userid = "user_id";

        public const string appid = "wechat_appid";
        public const string domain = "wechat_domain";
        public const string appsecret = "wechat_appsecret";
        public const string returnUrl = "wechat_returnUrl";
    }

    public static class SMSFixedTemplateName
    {
        public const string SendIdentifyCode = "客户验证码";
    }
}
