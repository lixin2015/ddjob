﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DdJob
{
    /// <summary>
    /// 从webconfig读取
    /// </summary>
    public static class DdJobAppConfiguration
    {
        #region 系统设置
        /// <summary>
        /// 是否是开发环境，是的话会多存数据到数据库或打印日志等。
        /// </summary>
        public static bool SYS_IS_DEV
        {
            get { return ConfigurationManager.AppSettings["SYS_IS_DEV"].ToLower() == "true"; }
        }
        #endregion
    }
}
